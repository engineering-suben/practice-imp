module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/server/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/server/controllers/userController.js":
/*!**************************************************!*\
  !*** ./src/server/controllers/userController.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _models_userModel__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../models/userModel */ "./src/server/models/userModel.js");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var body_parser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! body-parser */ "body-parser");
/* harmony import */ var body_parser__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(body_parser__WEBPACK_IMPORTED_MODULE_2__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }




var userRouter = express__WEBPACK_IMPORTED_MODULE_1___default.a.Router(); // userRouter.use(bodyParser.urlencoded({ extended: false }));

userRouter.use(body_parser__WEBPACK_IMPORTED_MODULE_2___default.a.json());
userRouter.post("/login", /*#__PURE__*/_asyncToGenerator(function* () {}));
userRouter.post("/signup", /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator(function* (req, res) {
    var findResult = yield Object(_models_userModel__WEBPACK_IMPORTED_MODULE_0__["find"])({
      email: req.body.email
    });
    console.log("findResult", findResult);

    if (findResult.length) {
      res.send({
        message: "User already exists."
      });
      return;
    }

    var entry = yield Object(_models_userModel__WEBPACK_IMPORTED_MODULE_0__["create"])(_objectSpread(_objectSpread({}, req.body), {}, {
      email: req.body.email,
      password: req.body.password
    }));
    res.send(entry);
  });

  return function (_x, _x2) {
    return _ref2.apply(this, arguments);
  };
}());
userRouter.post("/update", /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator(function* (req, res) {
    var result = yield Object(_models_userModel__WEBPACK_IMPORTED_MODULE_0__["updateByEmail"])(req.body.email, req.body.data);
    res.send(result);
  });

  return function (_x3, _x4) {
    return _ref3.apply(this, arguments);
  };
}());
userRouter.post("/delete", /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator(function* (req, res) {
    var result = yield Object(_models_userModel__WEBPACK_IMPORTED_MODULE_0__["deleteByEmail"])(req.body.email);
    res.send(result);
  });

  return function (_x5, _x6) {
    return _ref4.apply(this, arguments);
  };
}());
/* harmony default export */ __webpack_exports__["default"] = (userRouter);

/***/ }),

/***/ "./src/server/index.js":
/*!*****************************!*\
  !*** ./src/server/index.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var https__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! https */ "https");
/* harmony import */ var https__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(https__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! fs */ "fs");
/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(fs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! mongoose */ "mongoose");
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(mongoose__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var memcached__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! memcached */ "memcached");
/* harmony import */ var memcached__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(memcached__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var cookie_parser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! cookie-parser */ "cookie-parser");
/* harmony import */ var cookie_parser__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(cookie_parser__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _middlewares_handleRender__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./middlewares/handleRender */ "./src/server/middlewares/handleRender.js");
/* harmony import */ var _controllers_userController__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./controllers/userController */ "./src/server/controllers/userController.js");
/* harmony import */ var jsdom__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! jsdom */ "jsdom");
/* harmony import */ var jsdom__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(jsdom__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var html_pdf__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! html-pdf */ "html-pdf");
/* harmony import */ var html_pdf__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(html_pdf__WEBPACK_IMPORTED_MODULE_9__);




 // import bodyParser from "body-parser";





 // mongoose.connect(
// 	"mongodb://localhost:27017/test1",
// 	{
// 		useNewUrlParser: true,
// 		useUnifiedTopology: true,
// 	},
// 	(err) => {
// 		if (err) {
// 			console.error("Mongo DB Connection Error!");
// 			console.error(err);
// 		} else {
// 			console.log("Mongo DB Connected");
// 		}
// 	}
// );
//

var html = "<html><head></head><body></body></html>";
var options = {};
var {
  JSDOM
} = jsdom__WEBPACK_IMPORTED_MODULE_8___default.a;
global.dom = new JSDOM(html, options);
global.window = dom.window;
global.document = dom.window.document; //

var memcached = new memcached__WEBPACK_IMPORTED_MODULE_4___default.a("127.0.0.1:11211");
var app = express__WEBPACK_IMPORTED_MODULE_2___default()();
app.use(express__WEBPACK_IMPORTED_MODULE_2___default.a.static("public"));
app.use(cookie_parser__WEBPACK_IMPORTED_MODULE_5___default()());
var publicRouter = express__WEBPACK_IMPORTED_MODULE_2___default.a.Router();
var apiRouter = express__WEBPACK_IMPORTED_MODULE_2___default.a.Router();
app.use("/", publicRouter);
app.use("/home", publicRouter);
app.use("/api", apiRouter); // publicRouter.use(handleRender());

publicRouter.get("/", Object(_middlewares_handleRender__WEBPACK_IMPORTED_MODULE_6__["handleRender"])(), (req, res) => {
  memcached.set("sth", "asd", 300, function (err, data) {
    console.log("/ err, data", err, data);
  }); // res.cookie("access-token", process.env.PORT, {
  // 	expires: new Date(Date.now() + 12000),
  // });

  console.log("/");
  console.log("req.Referer", req.get("Referrer")); // console.log("req.cookies", req.headers.cookie);

  res.header("Access-Control-Allow-Credentials", true); // res.header("Access-Control-Allow-Origin", "https://ldev.limber.co.in:9000");

  res.cookie("access-token", process.env.PORT, {
    httpOnly: true
  });
  res.cookie("random", "random");
  res.send(res.locals.html);
});
publicRouter.get("/home", Object(_middlewares_handleRender__WEBPACK_IMPORTED_MODULE_6__["handleRender"])(), (req, res) => {
  memcached.get("sth", function (err, data) {
    console.log("/home err, data", err, data);
  });
  console.log("/home");
  console.log("req.Referer", req.get("Referrer"));
  console.log("req.Origin", req.get("Origin"));
  console.log("req.cookies", req.headers.cookie);
  console.log("req.cookies", req.cookies); // res.header(
  // 	"Content-Security-Policy",
  // 	"frame-ancestors 'self'; connect-src 'self';"
  // );
  // res.cookie("access-token", process.env.PORT);

  res.send(res.locals.html);
});
publicRouter.get("/products", Object(_middlewares_handleRender__WEBPACK_IMPORTED_MODULE_6__["handleRender"])(), (req, res) => {
  console.log("/products");
  console.log("req.Referer", req.get("Referrer"));
  console.log("req.cookies", req.headers.cookie); // res.header("X-Frame-Options", "deny");
  // res.header(
  // 	"Content-Security-Policy",
  // 	"frame-ancestors 'self' https://ldev.limber.co.in:9000 https://localhost:9000; connect-src 'self'"
  // );
  // res.header("Content-Security-Policy", "connect-src 'self';");
  // res.cookie("access-token", process.env.PORT);

  res.send(res.locals.html);
});
publicRouter.get("/pdf", Object(_middlewares_handleRender__WEBPACK_IMPORTED_MODULE_6__["handleRender"])(), (req, res) => {
  console.log("/pdf");
  var filename = new Date().getTime();
  var options = {
    format: "Letter"
  };
  html_pdf__WEBPACK_IMPORTED_MODULE_9___default.a.create(res.locals.html, options).toFile("./output/" + filename + ".pdf", function (err, res) {
    if (err) return console.log(err);
    console.log(res);
  });
  res.send(res.locals.html);
});
apiRouter.use((req, res, next) => {
  console.log("apiRouter", req.get("Origin")); // res.header("Access-Control-Allow-Origin", req.get("Origin"));

  next();
});
apiRouter.use("/users", _controllers_userController__WEBPACK_IMPORTED_MODULE_7__["default"]);
apiRouter.options("/user", (req, res) => {
  console.log("****HERE****"); // res.header("Access-Control-Allow-Credentials", true);

  res.header("Access-Control-Allow-Headers", "Content-Type");
  res.send();
});
apiRouter.get("/user", (req, res) => {
  // res.cookie("access-token", process.env.PORT, {
  // 	expires: new Date(Date.now() + 12000),
  // });
  console.log("/user");
  console.log("req.Referer", req.get("Referrer"));
  console.log("req.cookies", req.headers.cookie); // res.set("Cache-Control", "private, max-age=30");
  // res.set("Cache-Control", "private");

  res.header("Access-Control-Allow-Credentials", true);
  res.cookie("access-token", process.env.PORT, {
    expires: new Date(Date.now() + 2 * 60 * 1000),
    sameSite: "None",
    secure: true
  }).cookie("somestring", "somerandomstring", {
    expires: new Date(Date.now() + 2 * 60 * 1000),
    sameSite: "None",
    secure: true
  });
  res.send(["arsenal", "chelsea", "manu", 4]);
  console.log("sent");
});
apiRouter.post("/user", (req, res) => {
  // res.cookie("access-token", process.env.PORT, {
  // 	expires: new Date(Date.now() + 12000),
  // });
  console.log("/user");
  console.log("req.Referer", req.get("Referrer"));
  console.log("req.cookies", req.headers.cookie);
  res.header("Access-Control-Allow-Credentials", true);
  res.cookie("access-token", process.env.PORT);
  res.send(["arsenal", "chelsea"]);
});
app.get("*", (req, res, next) => {
  res.redirect("/");
}); // app.listen(process.env.PORT, () => {
// 	console.log("#############################");
// 	console.log("server running on port: ", process.env.PORT);
// 	console.log("#############################");
// });

https__WEBPACK_IMPORTED_MODULE_0___default.a.createServer({
  key: fs__WEBPACK_IMPORTED_MODULE_1___default.a.readFileSync("localhost-privkey.pem"),
  cert: fs__WEBPACK_IMPORTED_MODULE_1___default.a.readFileSync("localhost-cert.pem")
}, app).listen(process.env.PORT, () => {
  console.log("#############################");
  console.log("server running on port: ", process.env.PORT);
  console.log("#############################");
});

/***/ }),

/***/ "./src/server/middlewares/handleRender.js":
/*!************************************************!*\
  !*** ./src/server/middlewares/handleRender.js ***!
  \************************************************/
/*! exports provided: handleRender, renderFullPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleRender", function() { return handleRender; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "renderFullPage", function() { return renderFullPage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom_server__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom/server */ "react-dom/server");
/* harmony import */ var react_dom_server__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom_server__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! redux */ "redux");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _shared_configs_reducers__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/configs/reducers */ "./src/shared/configs/reducers.js");
/* harmony import */ var _shared_app__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../shared/app */ "./src/shared/app.js");






 // import { ChunkExtractor } from "@loadable/server";
// import path from "path";
// import render, { send } from "../processes/render/render";
// const statsFile = path.resolve(
// 	__dirname,
// 	"public/assets/web/loadable-stats.json"
// );
// const statsFile = path.resolve(__dirname, "./web/loadable-stats.json");
// console.log("statsFile", statsFile);

var handleRender = options => {
  return function (req, res, next) {
    // ****
    // const store = createStore(reducers);
    // const webExtractor = new ChunkExtractor({
    // 	statsFile,
    // 	entrypoints: ["index.js"],
    // });
    // const jsx = webExtractor.collectChunks(
    // 	<Provider store={store}>
    // 		<StaticRouter location={req.url}>
    // 			<App />
    // 		</StaticRouter>
    // 	</Provider>
    // );
    // const html = renderToString(jsx);
    // const preloadedState = store.getState();
    // res.locals.html = renderCodeSplit(html, webExtractor, preloadedState);
    // next();
    // ****
    // ****
    var store = Object(redux__WEBPACK_IMPORTED_MODULE_3__["createStore"])(_shared_configs_reducers__WEBPACK_IMPORTED_MODULE_5__["default"]);
    var html = Object(react_dom_server__WEBPACK_IMPORTED_MODULE_1__["renderToString"])( /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_redux__WEBPACK_IMPORTED_MODULE_4__["Provider"], {
      store: store
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["StaticRouter"], {
      location: req.url
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_shared_app__WEBPACK_IMPORTED_MODULE_6__["default"], null))));
    var preloadedState = store.getState();
    res.locals.html = renderFullPage(html, preloadedState);
    next(); // ****
    // ****
    // render.send({ store, location: req.url });
    // render.on("message", (m) => {
    // 	const html = m.html;
    // 	const preloadedState = store.getState();
    // 	res.locals.html = renderFullPage(html, preloadedState);
    // 	next();
    // });
    // ****
  };
};
var renderFullPage = (html, preloadedState) => {
  return "<!DOCTYPE html>\n      <head>\n      \t<!--<meta http-equiv=\"X-Frame-Options\" content=\"deny\">--><!-- This doesn't work -->\n        <title>React SSR</title>\n        <link rel=\"stylesheet\" href=\"/assets/index.css\">\n        <script defer src=\"/assets/index.js\"></script>\n      </head>\n      <body>\n        <div id=\"root\">".concat(html, "</div>\n        <script>\n        \twindow.__PRELOADED_STATE__ = ").concat(JSON.stringify(preloadedState), "\n        </script>\n      </body>\n    </html>");
}; // export const renderCodeSplit = (html, webExtractor, preloadedState) => {
// 	console.log("webExtractor.getLinkTags", webExtractor.getLinkTags());
// 	console.log("webExtractor.getScriptTags", webExtractor.getScriptTags());
// 	return `<!DOCTYPE html>
//       <head>
//         <title>React SSR</title>
//         ${webExtractor.getLinkTags()}
//         ${webExtractor.getScriptTags()}
//       </head>
//       <body>
//         <div id="root">${html}</div>
//         <script>
//         	window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState)}
//         </script>
//       </body>
//     </html>`;
// };

/***/ }),

/***/ "./src/server/models/userModel.js":
/*!****************************************!*\
  !*** ./src/server/models/userModel.js ***!
  \****************************************/
/*! exports provided: create, find, updateByEmail, deleteByEmail, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "create", function() { return create; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "find", function() { return find; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateByEmail", function() { return updateByEmail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteByEmail", function() { return deleteByEmail; });
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! mongoose */ "mongoose");
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mongoose__WEBPACK_IMPORTED_MODULE_0__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }


var UserSchema = mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.Schema({
  _id: {
    type: mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.Schema.Types.ObjectId,
    required: true,
    auto: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  }
});
var User = mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.model("Users", UserSchema, "Users");
var create = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator(function* (obj) {
    var email = obj.email;
    var password = obj.password;
    console.log("User", _objectSpread(_objectSpread({}, obj), {}, {
      email,
      password
    }));
    var user = new User(_objectSpread(_objectSpread({}, obj), {}, {
      email,
      password
    }));
    return yield user.save();
  });

  return function create(_x) {
    return _ref.apply(this, arguments);
  };
}();
var find = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator(function* (obj) {
    var email = obj.email;
    var result = yield User.find({
      email
    }).lean();
    return result;
  });

  return function find(_x2) {
    return _ref2.apply(this, arguments);
  };
}();
var updateByEmail = /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator(function* (email, obj) {
    var result = yield User.update({
      email
    }, obj);
    return result;
  });

  return function updateByEmail(_x3, _x4) {
    return _ref3.apply(this, arguments);
  };
}();
var deleteByEmail = /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator(function* (email) {
    var result = yield User.deleteOne({
      email
    });
    return result;
  });

  return function deleteByEmail(_x5) {
    return _ref4.apply(this, arguments);
  };
}();
/* harmony default export */ __webpack_exports__["default"] = (User);

/***/ }),

/***/ "./src/shared/app.js":
/*!***************************!*\
  !*** ./src/shared/app.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _configs_routes_routes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./configs/routes/routes */ "./src/shared/configs/routes/routes.js");



var App = () => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_configs_routes_routes__WEBPACK_IMPORTED_MODULE_1__["default"], null);
};

/* harmony default export */ __webpack_exports__["default"] = (App);

/***/ }),

/***/ "./src/shared/common/components/routes/PrivateRoute.js":
/*!*************************************************************!*\
  !*** ./src/shared/common/components/routes/PrivateRoute.js ***!
  \*************************************************************/
/*! exports provided: PrivateRoute */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrivateRoute", function() { return PrivateRoute; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils_authUtils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../utils/authUtils */ "./src/shared/common/utils/authUtils.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }




var PrivateRoute = (_ref) => {
  var {
    component: Component,
    redirectPath
  } = _ref,
      rest = _objectWithoutProperties(_ref, ["component", "redirectPath"]);

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], _extends({}, rest, {
    render: props => Object(_utils_authUtils__WEBPACK_IMPORTED_MODULE_2__["isAccessGranted"])() ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Component, props) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Redirect"], {
      to: redirectPath
    })
  }));
};

/***/ }),

/***/ "./src/shared/common/components/routes/PublicRoute.js":
/*!************************************************************!*\
  !*** ./src/shared/common/components/routes/PublicRoute.js ***!
  \************************************************************/
/*! exports provided: PublicRoute */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PublicRoute", function() { return PublicRoute; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils_authUtils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../utils/authUtils */ "./src/shared/common/utils/authUtils.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }




var PublicRoute = (_ref) => {
  var {
    component: Component,
    redirectPath
  } = _ref,
      rest = _objectWithoutProperties(_ref, ["component", "redirectPath"]);

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], _extends({}, rest, {
    render: props => /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Component, props)
  }));
};

/***/ }),

/***/ "./src/shared/common/utils/authUtils.js":
/*!**********************************************!*\
  !*** ./src/shared/common/utils/authUtils.js ***!
  \**********************************************/
/*! exports provided: isAccessGranted */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isAccessGranted", function() { return isAccessGranted; });
var isAccessGranted = () => {
  // check auth header in backend doing a hack now
  if (typeof localStorage === "undefined") {
    return true;
  }

  if (localStorage.getItem("access_token")) {
    return true;
  }

  return false;
};

/***/ }),

/***/ "./src/shared/configs/reducers.js":
/*!****************************************!*\
  !*** ./src/shared/configs/reducers.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ "redux");
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(redux__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _pages_home_ducks__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../pages/home/ducks */ "./src/shared/pages/home/ducks.js");
/* harmony import */ var _pages_products_ducks__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../pages/products/ducks */ "./src/shared/pages/products/ducks.js");



var reducer = Object(redux__WEBPACK_IMPORTED_MODULE_0__["combineReducers"])({
  // ...exampleReducer
  homeReducer: _pages_home_ducks__WEBPACK_IMPORTED_MODULE_1__["homeReducer"],
  productsReducer: _pages_products_ducks__WEBPACK_IMPORTED_MODULE_2__["productsReducer"]
});
/* harmony default export */ __webpack_exports__["default"] = (reducer);

/***/ }),

/***/ "./src/shared/configs/routes/routes.js":
/*!*********************************************!*\
  !*** ./src/shared/configs/routes/routes.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _common_components_routes_PrivateRoute__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../common/components/routes/PrivateRoute */ "./src/shared/common/components/routes/PrivateRoute.js");
/* harmony import */ var _common_components_routes_PublicRoute__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../common/components/routes/PublicRoute */ "./src/shared/common/components/routes/PublicRoute.js");
/* harmony import */ var _pages_home_home__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../pages/home/home */ "./src/shared/pages/home/home.js");
/* harmony import */ var _pages_products_products__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../pages/products/products */ "./src/shared/pages/products/products.js");
/* harmony import */ var _pages_pdf_pdf__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../pages/pdf/pdf */ "./src/shared/pages/pdf/pdf.js");








var Routes = props => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Switch"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_components_routes_PublicRoute__WEBPACK_IMPORTED_MODULE_3__["PublicRoute"], {
    path: "/pdf",
    component: _pages_pdf_pdf__WEBPACK_IMPORTED_MODULE_6__["default"],
    redirectPath: "/pdf"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Redirect"], {
    to: "/pdf"
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Routes);

/***/ }),

/***/ "./src/shared/pages/home/ducks.js":
/*!****************************************!*\
  !*** ./src/shared/pages/home/ducks.js ***!
  \****************************************/
/*! exports provided: stateSelector, getLoading, homeReducer, doSomething, homeSaga */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "stateSelector", function() { return stateSelector; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLoading", function() { return getLoading; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "homeReducer", function() { return homeReducer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "doSomething", function() { return doSomething; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "homeSaga", function() { return homeSaga; });
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @reduxjs/toolkit */ "@reduxjs/toolkit");
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__);


var DO_SOMETHING = "[home] do something";
var SOMETHING_DONE = "[home] something done";
var initialState = {
  loading: true
};
var stateSelector = state => state.homeReducer;
var getLoading = Object(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(stateSelector, state => {
  console.log(state);
  return state.loading;
});
var homeReducer = Object(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__["createReducer"])(initialState, {
  [SOMETHING_DONE]: (state, _ref) => {
    var {
      payload
    } = _ref;
    state.loading = false;
  }
});
var doSomething = Object(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__["createAction"])(DO_SOMETHING);

function* initHome(_ref2) {
  var {
    payload
  } = _ref2;
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])({
    type: SOMETHING_DONE,
    payload: {
      loading: false
    }
  });
}

function* homeSaga() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["all"])([yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["takeLatest"])(DO_SOMETHING, initHome)]);
}

/***/ }),

/***/ "./src/shared/pages/home/home.js":
/*!***************************************!*\
  !*** ./src/shared/pages/home/home.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ducks__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ducks */ "./src/shared/pages/home/ducks.js");



var aTag = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
  href: "https://localhost:3000/home"
}, "link");

var Home = (_ref) => {
  var {
    loading,
    doSomething
  } = _ref;
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {// fetch("https://ldev.limber.co.in:9000/api/user", {
    // 	method: "post",
    // 	// withCredentials: true,
    // 	credentials: "include",
    // 	data: {
    // 		asd: "asd",
    // 	},
    // 	headers: {
    // 		"Content-Type": "application/json",
    // 	},
    // })
    // 	.then((body) => {
    // 		console.log("body", body.headers.get("set-cookie"));
    // 		return body.json();
    // 	})
    // 	.then((res) => console.log("res", res));
    // fetch("https://ldev.limber.in:9001/api/user", {
    // 	credentials: "include",
    // })
    // 	.then((body) => {
    // 		console.log("body", body.headers.get("set-cookie"));
    // 		return body.json();
    // 	})
    // 	.then((res) => console.log("res", res));
    // setTimeout(() => {
    // 	doSomething();
    // }, 1000);
  }, []);

  var onClick = event => {
    console.log("onClick", event);
    event.preventDefault(); // fetch("https://localhost:3000/api/users")
    // 	.then((body) => body.json())
    // 	.then((res) => console.log("res", res));

    fetch("https://ldev.limber.co.in:9000/api/user", {
      credentials: "include",
      mode: "cors"
    }).then(body => {
      console.log("body", body.headers.get("set-cookie"));
      return body.json();
    }).then(res => console.log("res", res));
  };

  console.log("****Home****");
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "https://localhost:9000/home",
    onClick: onClick
  }, "link"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "https://ldev.limber.in:9001/api/user"
  }, "lnk"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("iframe", {
    src: "https://ldev.limber.in:9001/products",
    referrerPolicy: "strict-origin-when-cross-origin",
    sandbox: "allow-scripts allow-modals"
  }));
};

var enhance = Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(state => ({
  loading: Object(_ducks__WEBPACK_IMPORTED_MODULE_2__["getLoading"])(state)
}), {
  doSomething: _ducks__WEBPACK_IMPORTED_MODULE_2__["doSomething"]
});
/* harmony default export */ __webpack_exports__["default"] = (enhance(Home));

/***/ }),

/***/ "./src/shared/pages/pdf/pdf.js":
/*!*************************************!*\
  !*** ./src/shared/pages/pdf/pdf.js ***!
  \*************************************/
/*! exports provided: getBarChart, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getBarChart", function() { return getBarChart; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var d3__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! d3 */ "d3");
/* harmony import */ var d3__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(d3__WEBPACK_IMPORTED_MODULE_1__);
 // import { connect } from "react-redux";


var getBarChart = (width, height) => {
  var data = [{
    label: "A",
    value: 10
  }, {
    label: "B",
    value: 5
  }, {
    label: "C",
    value: 10
  }, {
    label: "D",
    value: 20
  }, {
    label: "E",
    value: 10
  }, {
    label: "F",
    value: 5
  }, {
    label: "G",
    value: 20
  }, {
    label: "H",
    value: 7
  }, {
    label: "I",
    value: 3
  }, {
    label: "J",
    value: 10
  }]; // const d3 = global.d3;
  // console.log("d3", global.d3);

  var margin = {
    top: 20,
    right: 20,
    bottom: 30,
    left: 40
  };
  var chartWidth = width - margin.left - margin.right;
  var chartHeight = height - margin.top - margin.bottom;
  var x = d3__WEBPACK_IMPORTED_MODULE_1__["scaleBand"]().rangeRound([0, chartWidth]).paddingInner(0.1);
  var y = d3__WEBPACK_IMPORTED_MODULE_1__["scaleLinear"]().rangeRound([chartHeight, 0]);
  var z = d3__WEBPACK_IMPORTED_MODULE_1__["scaleOrdinal"]().range(["#992600", "#004d00", "#003366"]);
  x.domain(data.map(function (d) {
    return d.label;
  }));
  y.domain([0, d3__WEBPACK_IMPORTED_MODULE_1__["max"](data, function (d) {
    return d.value;
  })]);
  var div = document.createElement("div");
  div.style.width = width + "px";
  div.style.height = height + "px";
  var svg = d3__WEBPACK_IMPORTED_MODULE_1__["select"](div).append("svg").attr("width", width).attr("height", height);
  var g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")").attr("width", chartWidth).attr("height", chartHeight);
  g.selectAll(".bar").data(data).enter().append("rect").attr("class", "bar").attr("x", function (d) {
    return x(d.label);
  }).attr("width", x.bandwidth()).attr("y", function (d) {
    return y(d.value);
  }).attr("height", function (d) {
    return chartHeight - y(d.value);
  }).attr("fill", function (d) {
    return z(d.label);
  });
  g.append("g").attr("transform", "translate(0," + chartHeight + ")").call(d3__WEBPACK_IMPORTED_MODULE_1__["axisBottom"](x));
  g.append("g").call(d3__WEBPACK_IMPORTED_MODULE_1__["axisLeft"](y));
  return div.innerHTML;
};

function createMarkup(data) {
  return {
    __html: data
  };
}

class Pdf extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(props) {
    super(props);
  }

  render() {
    var html = getBarChart(500, 500);
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      dangerouslySetInnerHTML: createMarkup(html)
    });
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Pdf);

/***/ }),

/***/ "./src/shared/pages/products/ducks.js":
/*!********************************************!*\
  !*** ./src/shared/pages/products/ducks.js ***!
  \********************************************/
/*! exports provided: stateSelector, getLoading, productsReducer, doSomething, productsSaga */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "stateSelector", function() { return stateSelector; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLoading", function() { return getLoading; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "productsReducer", function() { return productsReducer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "doSomething", function() { return doSomething; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "productsSaga", function() { return productsSaga; });
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @reduxjs/toolkit */ "@reduxjs/toolkit");
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__);


var DO_SOMETHING = "[products] do something";
var SOMETHING_DONE = "[products] something done";
var initialState = {
  loading: true
};
var stateSelector = state => state.productsReducer;
var getLoading = Object(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(stateSelector, state => {
  console.log(state);
  return state.loading;
});
var productsReducer = Object(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__["createReducer"])(initialState, {
  [SOMETHING_DONE]: (state, _ref) => {
    var {
      payload
    } = _ref;
    state.loading = false;
  }
});
var doSomething = Object(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__["createAction"])(DO_SOMETHING);

function* initProducts(_ref2) {
  var {
    payload
  } = _ref2;
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])({
    type: SOMETHING_DONE,
    payload: {
      loading: false
    }
  });
}

function* productsSaga() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["all"])([yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["takeLatest"])(DO_SOMETHING, initProducts)]);
}

/***/ }),

/***/ "./src/shared/pages/products/products.js":
/*!***********************************************!*\
  !*** ./src/shared/pages/products/products.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ducks__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ducks */ "./src/shared/pages/products/ducks.js");



var aTag = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
  href: "https://localhost:3000/home"
}, "link");

var Products = (_ref) => {
  var {
    loading,
    doSomething
  } = _ref;
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {// fetch("https://ldev.limber.co.in:9001/api/user", {
    // 	method: "post",
    // 	// withCredentials: true,
    // 	credentials: "include",
    // 	data: {
    // 		asd: "asd",
    // 	},
    // 	headers: {
    // 		"Content-Type": "application/json",
    // 	},
    // })
    // 	.then((body) => {
    // 		console.log("body", body.headers.get("set-cookie"));
    // 		return body.json();
    // 	})
    // 	.then((res) => console.log("res", res));
    // fetch("https://ldev.limber.in:9001/api/user", {
    // 	credentials: "include",
    // })
    // 	.then((body) => {
    // 		console.log("body", body.headers.get("set-cookie"));
    // 		return body.json();
    // 	})
    // 	.then((res) => console.log("res", res));
    // setTimeout(() => {
    // 	doSomething();
    // }, 1000);
  }, []);

  var onClick = event => {
    console.log("onClick", event);
    event.preventDefault(); // fetch("https://localhost:3000/api/users")
    // 	.then((body) => body.json())
    // 	.then((res) => console.log("res", res));

    fetch("https://ldev.limber.in:9001/api/user", {// credentials: "include",
    }).then(body => {
      console.log("body", body.headers.get("set-cookie"));
      return body.json();
    }).then(res => console.log("res", res));
    alert("Hello");
  };

  console.log("****Products****");
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "https://localhost:9000/home",
    onClick: onClick
  }, "link"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "https://ldev.limber.in:9001/api/user"
  }, "lnk"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null));
};

var enhance = Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(state => ({
  loading: Object(_ducks__WEBPACK_IMPORTED_MODULE_2__["getLoading"])(state)
}), {
  doSomething: _ducks__WEBPACK_IMPORTED_MODULE_2__["doSomething"]
});
/* harmony default export */ __webpack_exports__["default"] = (enhance(Products));

/***/ }),

/***/ "@reduxjs/toolkit":
/*!***********************************!*\
  !*** external "@reduxjs/toolkit" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@reduxjs/toolkit");

/***/ }),

/***/ "body-parser":
/*!******************************!*\
  !*** external "body-parser" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("body-parser");

/***/ }),

/***/ "cookie-parser":
/*!********************************!*\
  !*** external "cookie-parser" ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("cookie-parser");

/***/ }),

/***/ "d3":
/*!*********************!*\
  !*** external "d3" ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("d3");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),

/***/ "fs":
/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ }),

/***/ "html-pdf":
/*!***************************!*\
  !*** external "html-pdf" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("html-pdf");

/***/ }),

/***/ "https":
/*!************************!*\
  !*** external "https" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("https");

/***/ }),

/***/ "jsdom":
/*!************************!*\
  !*** external "jsdom" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("jsdom");

/***/ }),

/***/ "memcached":
/*!****************************!*\
  !*** external "memcached" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("memcached");

/***/ }),

/***/ "mongoose":
/*!***************************!*\
  !*** external "mongoose" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("mongoose");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-dom/server":
/*!***********************************!*\
  !*** external "react-dom/server" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-dom/server");

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ "react-router-dom":
/*!***********************************!*\
  !*** external "react-router-dom" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-router-dom");

/***/ }),

/***/ "redux":
/*!************************!*\
  !*** external "redux" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux");

/***/ }),

/***/ "redux-saga/effects":
/*!*************************************!*\
  !*** external "redux-saga/effects" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux-saga/effects");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NlcnZlci9jb250cm9sbGVycy91c2VyQ29udHJvbGxlci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2VydmVyL2luZGV4LmpzIiwid2VicGFjazovLy8uL3NyYy9zZXJ2ZXIvbWlkZGxld2FyZXMvaGFuZGxlUmVuZGVyLmpzIiwid2VicGFjazovLy8uL3NyYy9zZXJ2ZXIvbW9kZWxzL3VzZXJNb2RlbC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2hhcmVkL2FwcC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2hhcmVkL2NvbW1vbi9jb21wb25lbnRzL3JvdXRlcy9Qcml2YXRlUm91dGUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NoYXJlZC9jb21tb24vY29tcG9uZW50cy9yb3V0ZXMvUHVibGljUm91dGUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NoYXJlZC9jb21tb24vdXRpbHMvYXV0aFV0aWxzLmpzIiwid2VicGFjazovLy8uL3NyYy9zaGFyZWQvY29uZmlncy9yZWR1Y2Vycy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2hhcmVkL2NvbmZpZ3Mvcm91dGVzL3JvdXRlcy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2hhcmVkL3BhZ2VzL2hvbWUvZHVja3MuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NoYXJlZC9wYWdlcy9ob21lL2hvbWUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NoYXJlZC9wYWdlcy9wZGYvcGRmLmpzIiwid2VicGFjazovLy8uL3NyYy9zaGFyZWQvcGFnZXMvcHJvZHVjdHMvZHVja3MuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NoYXJlZC9wYWdlcy9wcm9kdWN0cy9wcm9kdWN0cy5qcyIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAcmVkdXhqcy90b29sa2l0XCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiYm9keS1wYXJzZXJcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJjb29raWUtcGFyc2VyXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiZDNcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJleHByZXNzXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiZnNcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJodG1sLXBkZlwiIiwid2VicGFjazovLy9leHRlcm5hbCBcImh0dHBzXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwianNkb21cIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJtZW1jYWNoZWRcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJtb25nb29zZVwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0XCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVhY3QtZG9tL3NlcnZlclwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0LXJlZHV4XCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVhY3Qtcm91dGVyLWRvbVwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlZHV4XCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVkdXgtc2FnYS9lZmZlY3RzXCIiXSwibmFtZXMiOlsidXNlclJvdXRlciIsImV4cHJlc3MiLCJSb3V0ZXIiLCJ1c2UiLCJib2R5UGFyc2VyIiwianNvbiIsInBvc3QiLCJyZXEiLCJyZXMiLCJmaW5kUmVzdWx0IiwiZmluZCIsImVtYWlsIiwiYm9keSIsImNvbnNvbGUiLCJsb2ciLCJsZW5ndGgiLCJzZW5kIiwibWVzc2FnZSIsImVudHJ5IiwiY3JlYXRlIiwicGFzc3dvcmQiLCJyZXN1bHQiLCJ1cGRhdGVCeUVtYWlsIiwiZGF0YSIsImRlbGV0ZUJ5RW1haWwiLCJodG1sIiwib3B0aW9ucyIsIkpTRE9NIiwianNkb20iLCJnbG9iYWwiLCJkb20iLCJ3aW5kb3ciLCJkb2N1bWVudCIsIm1lbWNhY2hlZCIsIk1lbWNhY2hlZCIsImFwcCIsInN0YXRpYyIsImNvb2tpZVBhcnNlciIsInB1YmxpY1JvdXRlciIsImFwaVJvdXRlciIsImdldCIsImhhbmRsZVJlbmRlciIsInNldCIsImVyciIsImhlYWRlciIsImNvb2tpZSIsInByb2Nlc3MiLCJlbnYiLCJQT1JUIiwiaHR0cE9ubHkiLCJsb2NhbHMiLCJoZWFkZXJzIiwiY29va2llcyIsImZpbGVuYW1lIiwiRGF0ZSIsImdldFRpbWUiLCJmb3JtYXQiLCJwZGYiLCJ0b0ZpbGUiLCJuZXh0IiwiZXhwaXJlcyIsIm5vdyIsInNhbWVTaXRlIiwic2VjdXJlIiwicmVkaXJlY3QiLCJodHRwcyIsImNyZWF0ZVNlcnZlciIsImtleSIsImZzIiwicmVhZEZpbGVTeW5jIiwiY2VydCIsImxpc3RlbiIsInN0b3JlIiwiY3JlYXRlU3RvcmUiLCJyZWR1Y2VycyIsInJlbmRlclRvU3RyaW5nIiwidXJsIiwicHJlbG9hZGVkU3RhdGUiLCJnZXRTdGF0ZSIsInJlbmRlckZ1bGxQYWdlIiwiSlNPTiIsInN0cmluZ2lmeSIsIlVzZXJTY2hlbWEiLCJtb25nb29zZSIsIlNjaGVtYSIsIl9pZCIsInR5cGUiLCJUeXBlcyIsIk9iamVjdElkIiwicmVxdWlyZWQiLCJhdXRvIiwiU3RyaW5nIiwidW5pcXVlIiwiVXNlciIsIm1vZGVsIiwib2JqIiwidXNlciIsInNhdmUiLCJsZWFuIiwidXBkYXRlIiwiZGVsZXRlT25lIiwiQXBwIiwiUHJpdmF0ZVJvdXRlIiwiY29tcG9uZW50IiwiQ29tcG9uZW50IiwicmVkaXJlY3RQYXRoIiwicmVzdCIsInByb3BzIiwiaXNBY2Nlc3NHcmFudGVkIiwiUHVibGljUm91dGUiLCJsb2NhbFN0b3JhZ2UiLCJnZXRJdGVtIiwicmVkdWNlciIsImNvbWJpbmVSZWR1Y2VycyIsImhvbWVSZWR1Y2VyIiwicHJvZHVjdHNSZWR1Y2VyIiwiUm91dGVzIiwiUGRmIiwiRE9fU09NRVRISU5HIiwiU09NRVRISU5HX0RPTkUiLCJpbml0aWFsU3RhdGUiLCJsb2FkaW5nIiwic3RhdGVTZWxlY3RvciIsInN0YXRlIiwiZ2V0TG9hZGluZyIsImNyZWF0ZVNlbGVjdG9yIiwiY3JlYXRlUmVkdWNlciIsInBheWxvYWQiLCJkb1NvbWV0aGluZyIsImNyZWF0ZUFjdGlvbiIsImluaXRIb21lIiwicHV0IiwiaG9tZVNhZ2EiLCJhbGwiLCJ0YWtlTGF0ZXN0IiwiYVRhZyIsIkhvbWUiLCJ1c2VFZmZlY3QiLCJvbkNsaWNrIiwiZXZlbnQiLCJwcmV2ZW50RGVmYXVsdCIsImZldGNoIiwiY3JlZGVudGlhbHMiLCJtb2RlIiwidGhlbiIsImVuaGFuY2UiLCJjb25uZWN0IiwiZ2V0QmFyQ2hhcnQiLCJ3aWR0aCIsImhlaWdodCIsImxhYmVsIiwidmFsdWUiLCJtYXJnaW4iLCJ0b3AiLCJyaWdodCIsImJvdHRvbSIsImxlZnQiLCJjaGFydFdpZHRoIiwiY2hhcnRIZWlnaHQiLCJ4IiwiZDMiLCJyYW5nZVJvdW5kIiwicGFkZGluZ0lubmVyIiwieSIsInoiLCJyYW5nZSIsImRvbWFpbiIsIm1hcCIsImQiLCJkaXYiLCJjcmVhdGVFbGVtZW50Iiwic3R5bGUiLCJzdmciLCJhcHBlbmQiLCJhdHRyIiwiZyIsInNlbGVjdEFsbCIsImVudGVyIiwiYmFuZHdpZHRoIiwiY2FsbCIsImlubmVySFRNTCIsImNyZWF0ZU1hcmt1cCIsIl9faHRtbCIsIlJlYWN0IiwiY29uc3RydWN0b3IiLCJyZW5kZXIiLCJpbml0UHJvZHVjdHMiLCJwcm9kdWN0c1NhZ2EiLCJQcm9kdWN0cyIsImFsZXJ0Il0sIm1hcHBpbmdzIjoiOztRQUFBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNsRkE7QUFNQTtBQUNBO0FBRUEsSUFBTUEsVUFBVSxHQUFHQyw4Q0FBTyxDQUFDQyxNQUFSLEVBQW5CLEMsQ0FDQTs7QUFDQUYsVUFBVSxDQUFDRyxHQUFYLENBQWVDLGtEQUFVLENBQUNDLElBQVgsRUFBZjtBQUVBTCxVQUFVLENBQUNNLElBQVgsQ0FBZ0IsUUFBaEIsaUNBQTBCLGFBQVksQ0FBRSxDQUF4QztBQUVBTixVQUFVLENBQUNNLElBQVgsQ0FBZ0IsU0FBaEI7QUFBQSxnQ0FBMkIsV0FBT0MsR0FBUCxFQUFZQyxHQUFaLEVBQW9CO0FBQzlDLFFBQU1DLFVBQVUsU0FBU0MsOERBQUksQ0FBQztBQUFFQyxXQUFLLEVBQUVKLEdBQUcsQ0FBQ0ssSUFBSixDQUFTRDtBQUFsQixLQUFELENBQTdCO0FBQ0FFLFdBQU8sQ0FBQ0MsR0FBUixDQUFZLFlBQVosRUFBMEJMLFVBQTFCOztBQUNBLFFBQUlBLFVBQVUsQ0FBQ00sTUFBZixFQUF1QjtBQUN0QlAsU0FBRyxDQUFDUSxJQUFKLENBQVM7QUFBRUMsZUFBTyxFQUFFO0FBQVgsT0FBVDtBQUNBO0FBQ0E7O0FBQ0QsUUFBTUMsS0FBSyxTQUFTQyxnRUFBTSxpQ0FDdEJaLEdBQUcsQ0FBQ0ssSUFEa0I7QUFFekJELFdBQUssRUFBRUosR0FBRyxDQUFDSyxJQUFKLENBQVNELEtBRlM7QUFHekJTLGNBQVEsRUFBRWIsR0FBRyxDQUFDSyxJQUFKLENBQVNRO0FBSE0sT0FBMUI7QUFLQVosT0FBRyxDQUFDUSxJQUFKLENBQVNFLEtBQVQ7QUFDQSxHQWJEOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZUFsQixVQUFVLENBQUNNLElBQVgsQ0FBZ0IsU0FBaEI7QUFBQSxnQ0FBMkIsV0FBT0MsR0FBUCxFQUFZQyxHQUFaLEVBQW9CO0FBQzlDLFFBQU1hLE1BQU0sU0FBU0MsdUVBQWEsQ0FBQ2YsR0FBRyxDQUFDSyxJQUFKLENBQVNELEtBQVYsRUFBaUJKLEdBQUcsQ0FBQ0ssSUFBSixDQUFTVyxJQUExQixDQUFsQztBQUNBZixPQUFHLENBQUNRLElBQUosQ0FBU0ssTUFBVDtBQUNBLEdBSEQ7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQXJCLFVBQVUsQ0FBQ00sSUFBWCxDQUFnQixTQUFoQjtBQUFBLGdDQUEyQixXQUFPQyxHQUFQLEVBQVlDLEdBQVosRUFBb0I7QUFDOUMsUUFBTWEsTUFBTSxTQUFTRyx1RUFBYSxDQUFDakIsR0FBRyxDQUFDSyxJQUFKLENBQVNELEtBQVYsQ0FBbEM7QUFDQUgsT0FBRyxDQUFDUSxJQUFKLENBQVNLLE1BQVQ7QUFDQSxHQUhEOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS2VyQix5RUFBZixFOzs7Ozs7Ozs7Ozs7QUN4Q0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtDQUVBOztBQUNBO0FBQ0E7QUFFQTtBQUVBO0NBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7O0FBQ0EsSUFBTXlCLElBQUksR0FBRyx5Q0FBYjtBQUVBLElBQU1DLE9BQU8sR0FBRyxFQUFoQjtBQUVBLElBQU07QUFBRUM7QUFBRixJQUFZQyw0Q0FBbEI7QUFDQUMsTUFBTSxDQUFDQyxHQUFQLEdBQWEsSUFBSUgsS0FBSixDQUFVRixJQUFWLEVBQWdCQyxPQUFoQixDQUFiO0FBQ0FHLE1BQU0sQ0FBQ0UsTUFBUCxHQUFnQkQsR0FBRyxDQUFDQyxNQUFwQjtBQUNBRixNQUFNLENBQUNHLFFBQVAsR0FBa0JGLEdBQUcsQ0FBQ0MsTUFBSixDQUFXQyxRQUE3QixDLENBQ0E7O0FBRUEsSUFBTUMsU0FBUyxHQUFHLElBQUlDLGdEQUFKLENBQWMsaUJBQWQsQ0FBbEI7QUFFQSxJQUFNQyxHQUFHLEdBQUdsQyw4Q0FBTyxFQUFuQjtBQUNBa0MsR0FBRyxDQUFDaEMsR0FBSixDQUFRRiw4Q0FBTyxDQUFDbUMsTUFBUixDQUFlLFFBQWYsQ0FBUjtBQUNBRCxHQUFHLENBQUNoQyxHQUFKLENBQVFrQyxvREFBWSxFQUFwQjtBQUVBLElBQU1DLFlBQVksR0FBR3JDLDhDQUFPLENBQUNDLE1BQVIsRUFBckI7QUFDQSxJQUFNcUMsU0FBUyxHQUFHdEMsOENBQU8sQ0FBQ0MsTUFBUixFQUFsQjtBQUVBaUMsR0FBRyxDQUFDaEMsR0FBSixDQUFRLEdBQVIsRUFBYW1DLFlBQWI7QUFDQUgsR0FBRyxDQUFDaEMsR0FBSixDQUFRLE9BQVIsRUFBaUJtQyxZQUFqQjtBQUNBSCxHQUFHLENBQUNoQyxHQUFKLENBQVEsTUFBUixFQUFnQm9DLFNBQWhCLEUsQ0FFQTs7QUFFQUQsWUFBWSxDQUFDRSxHQUFiLENBQWlCLEdBQWpCLEVBQXNCQyw4RUFBWSxFQUFsQyxFQUFzQyxDQUFDbEMsR0FBRCxFQUFNQyxHQUFOLEtBQWM7QUFDbkR5QixXQUFTLENBQUNTLEdBQVYsQ0FBYyxLQUFkLEVBQXFCLEtBQXJCLEVBQTRCLEdBQTVCLEVBQWlDLFVBQVVDLEdBQVYsRUFBZXBCLElBQWYsRUFBcUI7QUFDckRWLFdBQU8sQ0FBQ0MsR0FBUixDQUFZLGFBQVosRUFBMkI2QixHQUEzQixFQUFnQ3BCLElBQWhDO0FBQ0EsR0FGRCxFQURtRCxDQUtuRDtBQUNBO0FBQ0E7O0FBQ0FWLFNBQU8sQ0FBQ0MsR0FBUixDQUFZLEdBQVo7QUFDQUQsU0FBTyxDQUFDQyxHQUFSLENBQVksYUFBWixFQUEyQlAsR0FBRyxDQUFDaUMsR0FBSixDQUFRLFVBQVIsQ0FBM0IsRUFUbUQsQ0FVbkQ7O0FBQ0FoQyxLQUFHLENBQUNvQyxNQUFKLENBQVcsa0NBQVgsRUFBK0MsSUFBL0MsRUFYbUQsQ0FZbkQ7O0FBQ0FwQyxLQUFHLENBQUNxQyxNQUFKLENBQVcsY0FBWCxFQUEyQkMsT0FBTyxDQUFDQyxHQUFSLENBQVlDLElBQXZDLEVBQTZDO0FBQUVDLFlBQVEsRUFBRTtBQUFaLEdBQTdDO0FBQ0F6QyxLQUFHLENBQUNxQyxNQUFKLENBQVcsUUFBWCxFQUFxQixRQUFyQjtBQUNBckMsS0FBRyxDQUFDUSxJQUFKLENBQVNSLEdBQUcsQ0FBQzBDLE1BQUosQ0FBV3pCLElBQXBCO0FBQ0EsQ0FoQkQ7QUFrQkFhLFlBQVksQ0FBQ0UsR0FBYixDQUFpQixPQUFqQixFQUEwQkMsOEVBQVksRUFBdEMsRUFBMEMsQ0FBQ2xDLEdBQUQsRUFBTUMsR0FBTixLQUFjO0FBQ3ZEeUIsV0FBUyxDQUFDTyxHQUFWLENBQWMsS0FBZCxFQUFxQixVQUFVRyxHQUFWLEVBQWVwQixJQUFmLEVBQXFCO0FBQ3pDVixXQUFPLENBQUNDLEdBQVIsQ0FBWSxpQkFBWixFQUErQjZCLEdBQS9CLEVBQW9DcEIsSUFBcEM7QUFDQSxHQUZEO0FBSUFWLFNBQU8sQ0FBQ0MsR0FBUixDQUFZLE9BQVo7QUFDQUQsU0FBTyxDQUFDQyxHQUFSLENBQVksYUFBWixFQUEyQlAsR0FBRyxDQUFDaUMsR0FBSixDQUFRLFVBQVIsQ0FBM0I7QUFDQTNCLFNBQU8sQ0FBQ0MsR0FBUixDQUFZLFlBQVosRUFBMEJQLEdBQUcsQ0FBQ2lDLEdBQUosQ0FBUSxRQUFSLENBQTFCO0FBQ0EzQixTQUFPLENBQUNDLEdBQVIsQ0FBWSxhQUFaLEVBQTJCUCxHQUFHLENBQUM0QyxPQUFKLENBQVlOLE1BQXZDO0FBQ0FoQyxTQUFPLENBQUNDLEdBQVIsQ0FBWSxhQUFaLEVBQTJCUCxHQUFHLENBQUM2QyxPQUEvQixFQVR1RCxDQVV2RDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBNUMsS0FBRyxDQUFDUSxJQUFKLENBQVNSLEdBQUcsQ0FBQzBDLE1BQUosQ0FBV3pCLElBQXBCO0FBQ0EsQ0FoQkQ7QUFrQkFhLFlBQVksQ0FBQ0UsR0FBYixDQUFpQixXQUFqQixFQUE4QkMsOEVBQVksRUFBMUMsRUFBOEMsQ0FBQ2xDLEdBQUQsRUFBTUMsR0FBTixLQUFjO0FBQzNESyxTQUFPLENBQUNDLEdBQVIsQ0FBWSxXQUFaO0FBQ0FELFNBQU8sQ0FBQ0MsR0FBUixDQUFZLGFBQVosRUFBMkJQLEdBQUcsQ0FBQ2lDLEdBQUosQ0FBUSxVQUFSLENBQTNCO0FBQ0EzQixTQUFPLENBQUNDLEdBQVIsQ0FBWSxhQUFaLEVBQTJCUCxHQUFHLENBQUM0QyxPQUFKLENBQVlOLE1BQXZDLEVBSDJELENBSTNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBckMsS0FBRyxDQUFDUSxJQUFKLENBQVNSLEdBQUcsQ0FBQzBDLE1BQUosQ0FBV3pCLElBQXBCO0FBQ0EsQ0FaRDtBQWNBYSxZQUFZLENBQUNFLEdBQWIsQ0FBaUIsTUFBakIsRUFBeUJDLDhFQUFZLEVBQXJDLEVBQXlDLENBQUNsQyxHQUFELEVBQU1DLEdBQU4sS0FBYztBQUN0REssU0FBTyxDQUFDQyxHQUFSLENBQVksTUFBWjtBQUVBLE1BQU11QyxRQUFRLEdBQUcsSUFBSUMsSUFBSixHQUFXQyxPQUFYLEVBQWpCO0FBQ0EsTUFBTTdCLE9BQU8sR0FBRztBQUFFOEIsVUFBTSxFQUFFO0FBQVYsR0FBaEI7QUFDQUMsaURBQUcsQ0FDRHRDLE1BREYsQ0FDU1gsR0FBRyxDQUFDMEMsTUFBSixDQUFXekIsSUFEcEIsRUFDMEJDLE9BRDFCLEVBRUVnQyxNQUZGLENBRVMsY0FBY0wsUUFBZCxHQUF5QixNQUZsQyxFQUUwQyxVQUFVVixHQUFWLEVBQWVuQyxHQUFmLEVBQW9CO0FBQzVELFFBQUltQyxHQUFKLEVBQVMsT0FBTzlCLE9BQU8sQ0FBQ0MsR0FBUixDQUFZNkIsR0FBWixDQUFQO0FBQ1Q5QixXQUFPLENBQUNDLEdBQVIsQ0FBWU4sR0FBWjtBQUNBLEdBTEY7QUFPQUEsS0FBRyxDQUFDUSxJQUFKLENBQVNSLEdBQUcsQ0FBQzBDLE1BQUosQ0FBV3pCLElBQXBCO0FBQ0EsQ0FiRDtBQWVBYyxTQUFTLENBQUNwQyxHQUFWLENBQWMsQ0FBQ0ksR0FBRCxFQUFNQyxHQUFOLEVBQVdtRCxJQUFYLEtBQW9CO0FBQ2pDOUMsU0FBTyxDQUFDQyxHQUFSLENBQVksV0FBWixFQUF5QlAsR0FBRyxDQUFDaUMsR0FBSixDQUFRLFFBQVIsQ0FBekIsRUFEaUMsQ0FFakM7O0FBQ0FtQixNQUFJO0FBQ0osQ0FKRDtBQU1BcEIsU0FBUyxDQUFDcEMsR0FBVixDQUFjLFFBQWQsRUFBd0JILG1FQUF4QjtBQUVBdUMsU0FBUyxDQUFDYixPQUFWLENBQWtCLE9BQWxCLEVBQTJCLENBQUNuQixHQUFELEVBQU1DLEdBQU4sS0FBYztBQUN4Q0ssU0FBTyxDQUFDQyxHQUFSLENBQVksY0FBWixFQUR3QyxDQUV4Qzs7QUFDQU4sS0FBRyxDQUFDb0MsTUFBSixDQUFXLDhCQUFYLEVBQTJDLGNBQTNDO0FBQ0FwQyxLQUFHLENBQUNRLElBQUo7QUFDQSxDQUxEO0FBT0F1QixTQUFTLENBQUNDLEdBQVYsQ0FBYyxPQUFkLEVBQXVCLENBQUNqQyxHQUFELEVBQU1DLEdBQU4sS0FBYztBQUNwQztBQUNBO0FBQ0E7QUFDQUssU0FBTyxDQUFDQyxHQUFSLENBQVksT0FBWjtBQUNBRCxTQUFPLENBQUNDLEdBQVIsQ0FBWSxhQUFaLEVBQTJCUCxHQUFHLENBQUNpQyxHQUFKLENBQVEsVUFBUixDQUEzQjtBQUNBM0IsU0FBTyxDQUFDQyxHQUFSLENBQVksYUFBWixFQUEyQlAsR0FBRyxDQUFDNEMsT0FBSixDQUFZTixNQUF2QyxFQU5vQyxDQU9wQztBQUNBOztBQUNBckMsS0FBRyxDQUFDb0MsTUFBSixDQUFXLGtDQUFYLEVBQStDLElBQS9DO0FBQ0FwQyxLQUFHLENBQ0RxQyxNQURGLENBQ1MsY0FEVCxFQUN5QkMsT0FBTyxDQUFDQyxHQUFSLENBQVlDLElBRHJDLEVBQzJDO0FBQ3pDWSxXQUFPLEVBQUUsSUFBSU4sSUFBSixDQUFTQSxJQUFJLENBQUNPLEdBQUwsS0FBYSxJQUFJLEVBQUosR0FBUyxJQUEvQixDQURnQztBQUV6Q0MsWUFBUSxFQUFFLE1BRitCO0FBR3pDQyxVQUFNLEVBQUU7QUFIaUMsR0FEM0MsRUFNRWxCLE1BTkYsQ0FNUyxZQU5ULEVBTXVCLGtCQU52QixFQU0yQztBQUN6Q2UsV0FBTyxFQUFFLElBQUlOLElBQUosQ0FBU0EsSUFBSSxDQUFDTyxHQUFMLEtBQWEsSUFBSSxFQUFKLEdBQVMsSUFBL0IsQ0FEZ0M7QUFFekNDLFlBQVEsRUFBRSxNQUYrQjtBQUd6Q0MsVUFBTSxFQUFFO0FBSGlDLEdBTjNDO0FBV0F2RCxLQUFHLENBQUNRLElBQUosQ0FBUyxDQUFDLFNBQUQsRUFBWSxTQUFaLEVBQXVCLE1BQXZCLEVBQStCLENBQS9CLENBQVQ7QUFDQUgsU0FBTyxDQUFDQyxHQUFSLENBQVksTUFBWjtBQUNBLENBdkJEO0FBeUJBeUIsU0FBUyxDQUFDakMsSUFBVixDQUFlLE9BQWYsRUFBd0IsQ0FBQ0MsR0FBRCxFQUFNQyxHQUFOLEtBQWM7QUFDckM7QUFDQTtBQUNBO0FBQ0FLLFNBQU8sQ0FBQ0MsR0FBUixDQUFZLE9BQVo7QUFDQUQsU0FBTyxDQUFDQyxHQUFSLENBQVksYUFBWixFQUEyQlAsR0FBRyxDQUFDaUMsR0FBSixDQUFRLFVBQVIsQ0FBM0I7QUFDQTNCLFNBQU8sQ0FBQ0MsR0FBUixDQUFZLGFBQVosRUFBMkJQLEdBQUcsQ0FBQzRDLE9BQUosQ0FBWU4sTUFBdkM7QUFDQXJDLEtBQUcsQ0FBQ29DLE1BQUosQ0FBVyxrQ0FBWCxFQUErQyxJQUEvQztBQUNBcEMsS0FBRyxDQUFDcUMsTUFBSixDQUFXLGNBQVgsRUFBMkJDLE9BQU8sQ0FBQ0MsR0FBUixDQUFZQyxJQUF2QztBQUNBeEMsS0FBRyxDQUFDUSxJQUFKLENBQVMsQ0FBQyxTQUFELEVBQVksU0FBWixDQUFUO0FBQ0EsQ0FWRDtBQVlBbUIsR0FBRyxDQUFDSyxHQUFKLENBQVEsR0FBUixFQUFhLENBQUNqQyxHQUFELEVBQU1DLEdBQU4sRUFBV21ELElBQVgsS0FBb0I7QUFDaENuRCxLQUFHLENBQUN3RCxRQUFKLENBQWEsR0FBYjtBQUNBLENBRkQsRSxDQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUFDLDRDQUFLLENBQ0hDLFlBREYsQ0FFRTtBQUNDQyxLQUFHLEVBQUVDLHlDQUFFLENBQUNDLFlBQUgsQ0FBZ0IsdUJBQWhCLENBRE47QUFFQ0MsTUFBSSxFQUFFRix5Q0FBRSxDQUFDQyxZQUFILENBQWdCLG9CQUFoQjtBQUZQLENBRkYsRUFNRWxDLEdBTkYsRUFRRW9DLE1BUkYsQ0FRU3pCLE9BQU8sQ0FBQ0MsR0FBUixDQUFZQyxJQVJyQixFQVEyQixNQUFNO0FBQy9CbkMsU0FBTyxDQUFDQyxHQUFSLENBQVksK0JBQVo7QUFDQUQsU0FBTyxDQUFDQyxHQUFSLENBQVksMEJBQVosRUFBd0NnQyxPQUFPLENBQUNDLEdBQVIsQ0FBWUMsSUFBcEQ7QUFDQW5DLFNBQU8sQ0FBQ0MsR0FBUixDQUFZLCtCQUFaO0FBQ0EsQ0FaRixFOzs7Ozs7Ozs7Ozs7QUN2TEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0NBR0E7QUFDQTtBQUVBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBOztBQUVPLElBQU0yQixZQUFZLEdBQUlmLE9BQUQsSUFBYTtBQUN4QyxTQUFPLFVBQVVuQixHQUFWLEVBQWVDLEdBQWYsRUFBb0JtRCxJQUFwQixFQUEwQjtBQUNoQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFFBQU1hLEtBQUssR0FBR0MseURBQVcsQ0FBQ0MsZ0VBQUQsQ0FBekI7QUFDQSxRQUFNakQsSUFBSSxHQUFHa0QsdUVBQWMsZUFDMUIsMkRBQUMsb0RBQUQ7QUFBVSxXQUFLLEVBQUVIO0FBQWpCLG9CQUNDLDJEQUFDLDZEQUFEO0FBQWMsY0FBUSxFQUFFakUsR0FBRyxDQUFDcUU7QUFBNUIsb0JBQ0MsMkRBQUMsbURBQUQsT0FERCxDQURELENBRDBCLENBQTNCO0FBT0EsUUFBTUMsY0FBYyxHQUFHTCxLQUFLLENBQUNNLFFBQU4sRUFBdkI7QUFDQXRFLE9BQUcsQ0FBQzBDLE1BQUosQ0FBV3pCLElBQVgsR0FBa0JzRCxjQUFjLENBQUN0RCxJQUFELEVBQU9vRCxjQUFQLENBQWhDO0FBQ0FsQixRQUFJLEdBOUI0QixDQStCaEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQXpDRDtBQTBDQSxDQTNDTTtBQTZDQSxJQUFNb0IsY0FBYyxHQUFHLENBQUN0RCxJQUFELEVBQU9vRCxjQUFQLEtBQTBCO0FBQ3ZELG1XQVF3QnBELElBUnhCLDhFQVV1Q3VELElBQUksQ0FBQ0MsU0FBTCxDQUFlSixjQUFmLENBVnZDO0FBY0EsQ0FmTSxDLENBaUJQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNwR0E7QUFFQSxJQUFNSyxVQUFVLEdBQUdDLCtDQUFRLENBQUNDLE1BQVQsQ0FBZ0I7QUFDbENDLEtBQUcsRUFBRTtBQUFFQyxRQUFJLEVBQUVILCtDQUFRLENBQUNDLE1BQVQsQ0FBZ0JHLEtBQWhCLENBQXNCQyxRQUE5QjtBQUF3Q0MsWUFBUSxFQUFFLElBQWxEO0FBQXdEQyxRQUFJLEVBQUU7QUFBOUQsR0FENkI7QUFFbEMvRSxPQUFLLEVBQUU7QUFDTjJFLFFBQUksRUFBRUssTUFEQTtBQUVORixZQUFRLEVBQUUsSUFGSjtBQUdORyxVQUFNLEVBQUU7QUFIRixHQUYyQjtBQU9sQ3hFLFVBQVEsRUFBRTtBQUFFa0UsUUFBSSxFQUFFSyxNQUFSO0FBQWdCRixZQUFRLEVBQUU7QUFBMUI7QUFQd0IsQ0FBaEIsQ0FBbkI7QUFVQSxJQUFNSSxJQUFJLEdBQUdWLCtDQUFRLENBQUNXLEtBQVQsQ0FBZSxPQUFmLEVBQXdCWixVQUF4QixFQUFvQyxPQUFwQyxDQUFiO0FBRU8sSUFBTS9ELE1BQU07QUFBQSwrQkFBRyxXQUFnQjRFLEdBQWhCLEVBQXFCO0FBQzFDLFFBQU1wRixLQUFLLEdBQUdvRixHQUFHLENBQUNwRixLQUFsQjtBQUNBLFFBQU1TLFFBQVEsR0FBRzJFLEdBQUcsQ0FBQzNFLFFBQXJCO0FBQ0FQLFdBQU8sQ0FBQ0MsR0FBUixDQUFZLE1BQVosa0NBQXlCaUYsR0FBekI7QUFBOEJwRixXQUE5QjtBQUFxQ1M7QUFBckM7QUFDQSxRQUFNNEUsSUFBSSxHQUFHLElBQUlILElBQUosaUNBQWNFLEdBQWQ7QUFBbUJwRixXQUFuQjtBQUEwQlM7QUFBMUIsT0FBYjtBQUNBLGlCQUFhNEUsSUFBSSxDQUFDQyxJQUFMLEVBQWI7QUFDQSxHQU5rQjs7QUFBQSxrQkFBTjlFLE1BQU07QUFBQTtBQUFBO0FBQUEsR0FBWjtBQVFBLElBQU1ULElBQUk7QUFBQSxnQ0FBRyxXQUFnQnFGLEdBQWhCLEVBQXFCO0FBQ3hDLFFBQU1wRixLQUFLLEdBQUdvRixHQUFHLENBQUNwRixLQUFsQjtBQUNBLFFBQU1VLE1BQU0sU0FBU3dFLElBQUksQ0FBQ25GLElBQUwsQ0FBVTtBQUFFQztBQUFGLEtBQVYsRUFBcUJ1RixJQUFyQixFQUFyQjtBQUNBLFdBQU83RSxNQUFQO0FBQ0EsR0FKZ0I7O0FBQUEsa0JBQUpYLElBQUk7QUFBQTtBQUFBO0FBQUEsR0FBVjtBQU1BLElBQU1ZLGFBQWE7QUFBQSxnQ0FBRyxXQUFnQlgsS0FBaEIsRUFBdUJvRixHQUF2QixFQUE0QjtBQUN4RCxRQUFNMUUsTUFBTSxTQUFTd0UsSUFBSSxDQUFDTSxNQUFMLENBQVk7QUFBRXhGO0FBQUYsS0FBWixFQUF1Qm9GLEdBQXZCLENBQXJCO0FBQ0EsV0FBTzFFLE1BQVA7QUFDQSxHQUh5Qjs7QUFBQSxrQkFBYkMsYUFBYTtBQUFBO0FBQUE7QUFBQSxHQUFuQjtBQUtBLElBQU1FLGFBQWE7QUFBQSxnQ0FBRyxXQUFnQmIsS0FBaEIsRUFBdUI7QUFDbkQsUUFBTVUsTUFBTSxTQUFTd0UsSUFBSSxDQUFDTyxTQUFMLENBQWU7QUFBRXpGO0FBQUYsS0FBZixDQUFyQjtBQUNBLFdBQU9VLE1BQVA7QUFDQSxHQUh5Qjs7QUFBQSxrQkFBYkcsYUFBYTtBQUFBO0FBQUE7QUFBQSxHQUFuQjtBQUtRcUUsbUVBQWYsRTs7Ozs7Ozs7Ozs7O0FDdENBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTs7QUFFQSxJQUFNUSxHQUFHLEdBQUcsTUFBTTtBQUNqQixzQkFBTywyREFBQyw4REFBRCxPQUFQO0FBQ0EsQ0FGRDs7QUFJZUEsa0VBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1JBO0FBQ0E7QUFFQTtBQUVPLElBQU1DLFlBQVksR0FBRyxVQUl0QjtBQUFBLE1BSnVCO0FBQzVCQyxhQUFTLEVBQUVDLFNBRGlCO0FBRTVCQztBQUY0QixHQUl2QjtBQUFBLE1BREZDLElBQ0U7O0FBQ0wsc0JBQ0MsMkRBQUMsc0RBQUQsZUFDS0EsSUFETDtBQUVDLFVBQU0sRUFBR0MsS0FBRCxJQUNQQyx3RUFBZSxrQkFDZCwyREFBQyxTQUFELEVBQWVELEtBQWYsQ0FEYyxnQkFHZCwyREFBQyx5REFBRDtBQUFVLFFBQUUsRUFBRUY7QUFBZDtBQU5ILEtBREQ7QUFZQSxDQWpCTSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDTFA7QUFDQTtBQUVBO0FBRU8sSUFBTUksV0FBVyxHQUFHLFVBSXJCO0FBQUEsTUFKc0I7QUFDM0JOLGFBQVMsRUFBRUMsU0FEZ0I7QUFFM0JDO0FBRjJCLEdBSXRCO0FBQUEsTUFERkMsSUFDRTs7QUFDTCxzQkFBTywyREFBQyxzREFBRCxlQUFXQSxJQUFYO0FBQWlCLFVBQU0sRUFBR0MsS0FBRCxpQkFBVywyREFBQyxTQUFELEVBQWVBLEtBQWY7QUFBcEMsS0FBUDtBQUNBLENBTk0sQzs7Ozs7Ozs7Ozs7O0FDTFA7QUFBQTtBQUFPLElBQU1DLGVBQWUsR0FBRyxNQUFNO0FBQ3BDO0FBQ0EsTUFBSSxPQUFPRSxZQUFQLEtBQXdCLFdBQTVCLEVBQXlDO0FBQ3hDLFdBQU8sSUFBUDtBQUNBOztBQUVELE1BQUlBLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixjQUFyQixDQUFKLEVBQTBDO0FBQ3pDLFdBQU8sSUFBUDtBQUNBOztBQUNELFNBQU8sS0FBUDtBQUNBLENBVk0sQzs7Ozs7Ozs7Ozs7O0FDQVA7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUVBLElBQU1DLE9BQU8sR0FBR0MsNkRBQWUsQ0FBQztBQUMvQjtBQUNBQyw0RUFGK0I7QUFHL0JDLHdGQUFlQTtBQUhnQixDQUFELENBQS9CO0FBTWVILHNFQUFmLEU7Ozs7Ozs7Ozs7OztBQ1hBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsSUFBTUksTUFBTSxHQUFJVCxLQUFELElBQVc7QUFDekIsc0JBQ0MsMkRBQUMsdURBQUQscUJBT0MsMkRBQUMsaUZBQUQ7QUFBYSxRQUFJLEVBQUMsTUFBbEI7QUFBeUIsYUFBUyxFQUFFVSxzREFBcEM7QUFBeUMsZ0JBQVksRUFBQztBQUF0RCxJQVBELGVBUUMsMkRBQUMseURBQUQ7QUFBVSxNQUFFLEVBQUM7QUFBYixJQVJELENBREQ7QUFZQSxDQWJEOztBQWVlRCxxRUFBZixFOzs7Ozs7Ozs7Ozs7QUN6QkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUEsSUFBTUUsWUFBWSxHQUFHLHFCQUFyQjtBQUNBLElBQU1DLGNBQWMsR0FBRyx1QkFBdkI7QUFFQSxJQUFNQyxZQUFZLEdBQUc7QUFDcEJDLFNBQU8sRUFBRTtBQURXLENBQXJCO0FBSU8sSUFBTUMsYUFBYSxHQUFJQyxLQUFELElBQVdBLEtBQUssQ0FBQ1QsV0FBdkM7QUFFQSxJQUFNVSxVQUFVLEdBQUdDLHVFQUFjLENBQUNILGFBQUQsRUFBaUJDLEtBQUQsSUFBVztBQUNsRTlHLFNBQU8sQ0FBQ0MsR0FBUixDQUFZNkcsS0FBWjtBQUNBLFNBQU9BLEtBQUssQ0FBQ0YsT0FBYjtBQUNBLENBSHVDLENBQWpDO0FBS0EsSUFBTVAsV0FBVyxHQUFHWSxzRUFBYSxDQUFDTixZQUFELEVBQWU7QUFDdEQsR0FBQ0QsY0FBRCxHQUFrQixDQUFDSSxLQUFELFdBQXdCO0FBQUEsUUFBaEI7QUFBRUk7QUFBRixLQUFnQjtBQUN6Q0osU0FBSyxDQUFDRixPQUFOLEdBQWdCLEtBQWhCO0FBQ0E7QUFIcUQsQ0FBZixDQUFqQztBQU1BLElBQU1PLFdBQVcsR0FBR0MscUVBQVksQ0FBQ1gsWUFBRCxDQUFoQzs7QUFFUCxVQUFVWSxRQUFWLFFBQWdDO0FBQUEsTUFBYjtBQUFFSDtBQUFGLEdBQWE7QUFDL0IsUUFBTUksOERBQUcsQ0FBQztBQUFFN0MsUUFBSSxFQUFFaUMsY0FBUjtBQUF3QlEsV0FBTyxFQUFFO0FBQUVOLGFBQU8sRUFBRTtBQUFYO0FBQWpDLEdBQUQsQ0FBVDtBQUNBOztBQUVNLFVBQVVXLFFBQVYsR0FBcUI7QUFDM0IsUUFBTUMsOERBQUcsQ0FBQyxDQUFDLE1BQU1DLHFFQUFVLENBQUNoQixZQUFELEVBQWVZLFFBQWYsQ0FBakIsQ0FBRCxDQUFUO0FBQ0EsQzs7Ozs7Ozs7Ozs7O0FDL0JEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQTtBQUVBLElBQU1LLElBQUksZ0JBQUc7QUFBRyxNQUFJLEVBQUM7QUFBUixVQUFiOztBQUVBLElBQU1DLElBQUksR0FBRyxVQUE4QjtBQUFBLE1BQTdCO0FBQUVmLFdBQUY7QUFBV087QUFBWCxHQUE2QjtBQUMxQ1MseURBQVMsQ0FBQyxNQUFNLENBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0E1QlEsRUE0Qk4sRUE1Qk0sQ0FBVDs7QUE4QkEsTUFBTUMsT0FBTyxHQUFJQyxLQUFELElBQVc7QUFDMUI5SCxXQUFPLENBQUNDLEdBQVIsQ0FBWSxTQUFaLEVBQXVCNkgsS0FBdkI7QUFDQUEsU0FBSyxDQUFDQyxjQUFOLEdBRjBCLENBSTFCO0FBQ0E7QUFDQTs7QUFFQUMsU0FBSyxDQUFDLHlDQUFELEVBQTRDO0FBQ2hEQyxpQkFBVyxFQUFFLFNBRG1DO0FBRWhEQyxVQUFJLEVBQUU7QUFGMEMsS0FBNUMsQ0FBTCxDQUlFQyxJQUpGLENBSVFwSSxJQUFELElBQVU7QUFDZkMsYUFBTyxDQUFDQyxHQUFSLENBQVksTUFBWixFQUFvQkYsSUFBSSxDQUFDdUMsT0FBTCxDQUFhWCxHQUFiLENBQWlCLFlBQWpCLENBQXBCO0FBQ0EsYUFBTzVCLElBQUksQ0FBQ1AsSUFBTCxFQUFQO0FBQ0EsS0FQRixFQVFFMkksSUFSRixDQVFReEksR0FBRCxJQUFTSyxPQUFPLENBQUNDLEdBQVIsQ0FBWSxLQUFaLEVBQW1CTixHQUFuQixDQVJoQjtBQVNBLEdBakJEOztBQW1CQUssU0FBTyxDQUFDQyxHQUFSLENBQVksY0FBWjtBQUVBLHNCQUNDLHFGQUNDO0FBQUcsUUFBSSxFQUFDLDZCQUFSO0FBQXNDLFdBQU8sRUFBRTRIO0FBQS9DLFlBREQsZUFJQyxzRUFKRCxlQUtDO0FBQUcsUUFBSSxFQUFDO0FBQVIsV0FMRCxlQU1DLHNFQU5ELGVBT0M7QUFDQyxPQUFHLEVBQUMsc0NBREw7QUFFQyxrQkFBYyxFQUFDLGlDQUZoQjtBQUdDLFdBQU8sRUFBQztBQUhULElBUEQsQ0FERDtBQWdCQSxDQXBFRDs7QUFzRUEsSUFBTU8sT0FBTyxHQUFHQywyREFBTyxDQUNyQnZCLEtBQUQsS0FBWTtBQUNYRixTQUFPLEVBQUVHLHlEQUFVLENBQUNELEtBQUQ7QUFEUixDQUFaLENBRHNCLEVBSXRCO0FBQ0NLLGlFQUFXQTtBQURaLENBSnNCLENBQXZCO0FBU2VpQixzRUFBTyxDQUFDVCxJQUFELENBQXRCLEU7Ozs7Ozs7Ozs7OztBQ3RGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Q0FDQTs7QUFDQTtBQUVPLElBQU1XLFdBQVcsR0FBRyxDQUFDQyxLQUFELEVBQVFDLE1BQVIsS0FBbUI7QUFDN0MsTUFBTTlILElBQUksR0FBRyxDQUNaO0FBQUUrSCxTQUFLLEVBQUUsR0FBVDtBQUFjQyxTQUFLLEVBQUU7QUFBckIsR0FEWSxFQUVaO0FBQUVELFNBQUssRUFBRSxHQUFUO0FBQWNDLFNBQUssRUFBRTtBQUFyQixHQUZZLEVBR1o7QUFBRUQsU0FBSyxFQUFFLEdBQVQ7QUFBY0MsU0FBSyxFQUFFO0FBQXJCLEdBSFksRUFJWjtBQUFFRCxTQUFLLEVBQUUsR0FBVDtBQUFjQyxTQUFLLEVBQUU7QUFBckIsR0FKWSxFQUtaO0FBQUVELFNBQUssRUFBRSxHQUFUO0FBQWNDLFNBQUssRUFBRTtBQUFyQixHQUxZLEVBTVo7QUFBRUQsU0FBSyxFQUFFLEdBQVQ7QUFBY0MsU0FBSyxFQUFFO0FBQXJCLEdBTlksRUFPWjtBQUFFRCxTQUFLLEVBQUUsR0FBVDtBQUFjQyxTQUFLLEVBQUU7QUFBckIsR0FQWSxFQVFaO0FBQUVELFNBQUssRUFBRSxHQUFUO0FBQWNDLFNBQUssRUFBRTtBQUFyQixHQVJZLEVBU1o7QUFBRUQsU0FBSyxFQUFFLEdBQVQ7QUFBY0MsU0FBSyxFQUFFO0FBQXJCLEdBVFksRUFVWjtBQUFFRCxTQUFLLEVBQUUsR0FBVDtBQUFjQyxTQUFLLEVBQUU7QUFBckIsR0FWWSxDQUFiLENBRDZDLENBYzdDO0FBQ0E7O0FBQ0EsTUFBTUMsTUFBTSxHQUFHO0FBQUVDLE9BQUcsRUFBRSxFQUFQO0FBQVdDLFNBQUssRUFBRSxFQUFsQjtBQUFzQkMsVUFBTSxFQUFFLEVBQTlCO0FBQWtDQyxRQUFJLEVBQUU7QUFBeEMsR0FBZjtBQUNBLE1BQU1DLFVBQVUsR0FBR1QsS0FBSyxHQUFHSSxNQUFNLENBQUNJLElBQWYsR0FBc0JKLE1BQU0sQ0FBQ0UsS0FBaEQ7QUFDQSxNQUFNSSxXQUFXLEdBQUdULE1BQU0sR0FBR0csTUFBTSxDQUFDQyxHQUFoQixHQUFzQkQsTUFBTSxDQUFDRyxNQUFqRDtBQUVBLE1BQU1JLENBQUMsR0FBR0MsNENBQUEsR0FBZUMsVUFBZixDQUEwQixDQUFDLENBQUQsRUFBSUosVUFBSixDQUExQixFQUEyQ0ssWUFBM0MsQ0FBd0QsR0FBeEQsQ0FBVjtBQUNBLE1BQU1DLENBQUMsR0FBR0gsOENBQUEsR0FBaUJDLFVBQWpCLENBQTRCLENBQUNILFdBQUQsRUFBYyxDQUFkLENBQTVCLENBQVY7QUFDQSxNQUFNTSxDQUFDLEdBQUdKLCtDQUFBLEdBQWtCSyxLQUFsQixDQUF3QixDQUFDLFNBQUQsRUFBWSxTQUFaLEVBQXVCLFNBQXZCLENBQXhCLENBQVY7QUFDQU4sR0FBQyxDQUFDTyxNQUFGLENBQ0MvSSxJQUFJLENBQUNnSixHQUFMLENBQVMsVUFBVUMsQ0FBVixFQUFhO0FBQ3JCLFdBQU9BLENBQUMsQ0FBQ2xCLEtBQVQ7QUFDQSxHQUZELENBREQ7QUFLQWEsR0FBQyxDQUFDRyxNQUFGLENBQVMsQ0FDUixDQURRLEVBRVJOLHNDQUFBLENBQU96SSxJQUFQLEVBQWEsVUFBVWlKLENBQVYsRUFBYTtBQUN6QixXQUFPQSxDQUFDLENBQUNqQixLQUFUO0FBQ0EsR0FGRCxDQUZRLENBQVQ7QUFPQSxNQUFNa0IsR0FBRyxHQUFHekksUUFBUSxDQUFDMEksYUFBVCxDQUF1QixLQUF2QixDQUFaO0FBQ0FELEtBQUcsQ0FBQ0UsS0FBSixDQUFVdkIsS0FBVixHQUFrQkEsS0FBSyxHQUFHLElBQTFCO0FBQ0FxQixLQUFHLENBQUNFLEtBQUosQ0FBVXRCLE1BQVYsR0FBbUJBLE1BQU0sR0FBRyxJQUE1QjtBQUVBLE1BQU11QixHQUFHLEdBQUdaLHlDQUFBLENBQ0hTLEdBREcsRUFFVkksTUFGVSxDQUVILEtBRkcsRUFHVkMsSUFIVSxDQUdMLE9BSEssRUFHSTFCLEtBSEosRUFJVjBCLElBSlUsQ0FJTCxRQUpLLEVBSUt6QixNQUpMLENBQVo7QUFLQSxNQUFNMEIsQ0FBQyxHQUFHSCxHQUFHLENBQ1hDLE1BRFEsQ0FDRCxHQURDLEVBRVJDLElBRlEsQ0FFSCxXQUZHLEVBRVUsZUFBZXRCLE1BQU0sQ0FBQ0ksSUFBdEIsR0FBNkIsR0FBN0IsR0FBbUNKLE1BQU0sQ0FBQ0MsR0FBMUMsR0FBZ0QsR0FGMUQsRUFHUnFCLElBSFEsQ0FHSCxPQUhHLEVBR01qQixVQUhOLEVBSVJpQixJQUpRLENBSUgsUUFKRyxFQUlPaEIsV0FKUCxDQUFWO0FBS0FpQixHQUFDLENBQUNDLFNBQUYsQ0FBWSxNQUFaLEVBQ0V6SixJQURGLENBQ09BLElBRFAsRUFFRTBKLEtBRkYsR0FHRUosTUFIRixDQUdTLE1BSFQsRUFJRUMsSUFKRixDQUlPLE9BSlAsRUFJZ0IsS0FKaEIsRUFLRUEsSUFMRixDQUtPLEdBTFAsRUFLWSxVQUFVTixDQUFWLEVBQWE7QUFDdkIsV0FBT1QsQ0FBQyxDQUFDUyxDQUFDLENBQUNsQixLQUFILENBQVI7QUFDQSxHQVBGLEVBUUV3QixJQVJGLENBUU8sT0FSUCxFQVFnQmYsQ0FBQyxDQUFDbUIsU0FBRixFQVJoQixFQVNFSixJQVRGLENBU08sR0FUUCxFQVNZLFVBQVVOLENBQVYsRUFBYTtBQUN2QixXQUFPTCxDQUFDLENBQUNLLENBQUMsQ0FBQ2pCLEtBQUgsQ0FBUjtBQUNBLEdBWEYsRUFZRXVCLElBWkYsQ0FZTyxRQVpQLEVBWWlCLFVBQVVOLENBQVYsRUFBYTtBQUM1QixXQUFPVixXQUFXLEdBQUdLLENBQUMsQ0FBQ0ssQ0FBQyxDQUFDakIsS0FBSCxDQUF0QjtBQUNBLEdBZEYsRUFlRXVCLElBZkYsQ0FlTyxNQWZQLEVBZWUsVUFBVU4sQ0FBVixFQUFhO0FBQzFCLFdBQU9KLENBQUMsQ0FBQ0ksQ0FBQyxDQUFDbEIsS0FBSCxDQUFSO0FBQ0EsR0FqQkY7QUFtQkF5QixHQUFDLENBQUNGLE1BQUYsQ0FBUyxHQUFULEVBQ0VDLElBREYsQ0FDTyxXQURQLEVBQ29CLGlCQUFpQmhCLFdBQWpCLEdBQStCLEdBRG5ELEVBRUVxQixJQUZGLENBRU9uQiw2Q0FBQSxDQUFjRCxDQUFkLENBRlA7QUFJQWdCLEdBQUMsQ0FBQ0YsTUFBRixDQUFTLEdBQVQsRUFBY00sSUFBZCxDQUFtQm5CLDJDQUFBLENBQVlHLENBQVosQ0FBbkI7QUFFQSxTQUFPTSxHQUFHLENBQUNXLFNBQVg7QUFDQSxDQTNFTTs7QUE2RVAsU0FBU0MsWUFBVCxDQUFzQjlKLElBQXRCLEVBQTRCO0FBQzNCLFNBQU87QUFBRStKLFVBQU0sRUFBRS9KO0FBQVYsR0FBUDtBQUNBOztBQUVELE1BQU04RixHQUFOLFNBQWtCa0UsNENBQUssQ0FBQy9FLFNBQXhCLENBQWtDO0FBQ2pDZ0YsYUFBVyxDQUFDN0UsS0FBRCxFQUFRO0FBQ2xCLFVBQU1BLEtBQU47QUFDQTs7QUFFRDhFLFFBQU0sR0FBRztBQUNSLFFBQU1oSyxJQUFJLEdBQUcwSCxXQUFXLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FBeEI7QUFDQSx3QkFBTztBQUFLLDZCQUF1QixFQUFFa0MsWUFBWSxDQUFDNUosSUFBRDtBQUExQyxNQUFQO0FBQ0E7O0FBUmdDOztBQVduQjRGLGtFQUFmLEU7Ozs7Ozs7Ozs7OztBQ2hHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQSxJQUFNQyxZQUFZLEdBQUcseUJBQXJCO0FBQ0EsSUFBTUMsY0FBYyxHQUFHLDJCQUF2QjtBQUVBLElBQU1DLFlBQVksR0FBRztBQUNwQkMsU0FBTyxFQUFFO0FBRFcsQ0FBckI7QUFJTyxJQUFNQyxhQUFhLEdBQUlDLEtBQUQsSUFBV0EsS0FBSyxDQUFDUixlQUF2QztBQUVBLElBQU1TLFVBQVUsR0FBR0MsdUVBQWMsQ0FBQ0gsYUFBRCxFQUFpQkMsS0FBRCxJQUFXO0FBQ2xFOUcsU0FBTyxDQUFDQyxHQUFSLENBQVk2RyxLQUFaO0FBQ0EsU0FBT0EsS0FBSyxDQUFDRixPQUFiO0FBQ0EsQ0FIdUMsQ0FBakM7QUFLQSxJQUFNTixlQUFlLEdBQUdXLHNFQUFhLENBQUNOLFlBQUQsRUFBZTtBQUMxRCxHQUFDRCxjQUFELEdBQWtCLENBQUNJLEtBQUQsV0FBd0I7QUFBQSxRQUFoQjtBQUFFSTtBQUFGLEtBQWdCO0FBQ3pDSixTQUFLLENBQUNGLE9BQU4sR0FBZ0IsS0FBaEI7QUFDQTtBQUh5RCxDQUFmLENBQXJDO0FBTUEsSUFBTU8sV0FBVyxHQUFHQyxxRUFBWSxDQUFDWCxZQUFELENBQWhDOztBQUVQLFVBQVVvRSxZQUFWLFFBQW9DO0FBQUEsTUFBYjtBQUFFM0Q7QUFBRixHQUFhO0FBQ25DLFFBQU1JLDhEQUFHLENBQUM7QUFBRTdDLFFBQUksRUFBRWlDLGNBQVI7QUFBd0JRLFdBQU8sRUFBRTtBQUFFTixhQUFPLEVBQUU7QUFBWDtBQUFqQyxHQUFELENBQVQ7QUFDQTs7QUFFTSxVQUFVa0UsWUFBVixHQUF5QjtBQUMvQixRQUFNdEQsOERBQUcsQ0FBQyxDQUFDLE1BQU1DLHFFQUFVLENBQUNoQixZQUFELEVBQWVvRSxZQUFmLENBQWpCLENBQUQsQ0FBVDtBQUNBLEM7Ozs7Ozs7Ozs7OztBQy9CRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFFQSxJQUFNbkQsSUFBSSxnQkFBRztBQUFHLE1BQUksRUFBQztBQUFSLFVBQWI7O0FBRUEsSUFBTXFELFFBQVEsR0FBRyxVQUE4QjtBQUFBLE1BQTdCO0FBQUVuRSxXQUFGO0FBQVdPO0FBQVgsR0FBNkI7QUFDOUNTLHlEQUFTLENBQUMsTUFBTSxDQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBNUJRLEVBNEJOLEVBNUJNLENBQVQ7O0FBOEJBLE1BQU1DLE9BQU8sR0FBSUMsS0FBRCxJQUFXO0FBQzFCOUgsV0FBTyxDQUFDQyxHQUFSLENBQVksU0FBWixFQUF1QjZILEtBQXZCO0FBQ0FBLFNBQUssQ0FBQ0MsY0FBTixHQUYwQixDQUkxQjtBQUNBO0FBQ0E7O0FBRUFDLFNBQUssQ0FBQyxzQ0FBRCxFQUF5QyxDQUM3QztBQUQ2QyxLQUF6QyxDQUFMLENBR0VHLElBSEYsQ0FHUXBJLElBQUQsSUFBVTtBQUNmQyxhQUFPLENBQUNDLEdBQVIsQ0FBWSxNQUFaLEVBQW9CRixJQUFJLENBQUN1QyxPQUFMLENBQWFYLEdBQWIsQ0FBaUIsWUFBakIsQ0FBcEI7QUFDQSxhQUFPNUIsSUFBSSxDQUFDUCxJQUFMLEVBQVA7QUFDQSxLQU5GLEVBT0UySSxJQVBGLENBT1F4SSxHQUFELElBQVNLLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLEtBQVosRUFBbUJOLEdBQW5CLENBUGhCO0FBU0FxTCxTQUFLLENBQUMsT0FBRCxDQUFMO0FBQ0EsR0FsQkQ7O0FBb0JBaEwsU0FBTyxDQUFDQyxHQUFSLENBQVksa0JBQVo7QUFFQSxzQkFDQyxxRkFDQztBQUFHLFFBQUksRUFBQyw2QkFBUjtBQUFzQyxXQUFPLEVBQUU0SDtBQUEvQyxZQURELGVBSUMsc0VBSkQsZUFLQztBQUFHLFFBQUksRUFBQztBQUFSLFdBTEQsZUFNQyxzRUFORCxDQUREO0FBV0EsQ0FoRUQ7O0FBa0VBLElBQU1PLE9BQU8sR0FBR0MsMkRBQU8sQ0FDckJ2QixLQUFELEtBQVk7QUFDWEYsU0FBTyxFQUFFRyx5REFBVSxDQUFDRCxLQUFEO0FBRFIsQ0FBWixDQURzQixFQUl0QjtBQUNDSyxpRUFBV0E7QUFEWixDQUpzQixDQUF2QjtBQVNlaUIsc0VBQU8sQ0FBQzJDLFFBQUQsQ0FBdEIsRTs7Ozs7Ozs7Ozs7QUNsRkEsNkM7Ozs7Ozs7Ozs7O0FDQUEsd0M7Ozs7Ozs7Ozs7O0FDQUEsMEM7Ozs7Ozs7Ozs7O0FDQUEsK0I7Ozs7Ozs7Ozs7O0FDQUEsb0M7Ozs7Ozs7Ozs7O0FDQUEsK0I7Ozs7Ozs7Ozs7O0FDQUEscUM7Ozs7Ozs7Ozs7O0FDQUEsa0M7Ozs7Ozs7Ozs7O0FDQUEsa0M7Ozs7Ozs7Ozs7O0FDQUEsc0M7Ozs7Ozs7Ozs7O0FDQUEscUM7Ozs7Ozs7Ozs7O0FDQUEsa0M7Ozs7Ozs7Ozs7O0FDQUEsNkM7Ozs7Ozs7Ozs7O0FDQUEsd0M7Ozs7Ozs7Ozs7O0FDQUEsNkM7Ozs7Ozs7Ozs7O0FDQUEsa0M7Ozs7Ozs7Ozs7O0FDQUEsK0MiLCJmaWxlIjoiZGlzdC9zZXJ2ZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9zZXJ2ZXIvaW5kZXguanNcIik7XG4iLCJpbXBvcnQge1xuXHRjcmVhdGUsXG5cdGZpbmQsXG5cdHVwZGF0ZUJ5RW1haWwsXG5cdGRlbGV0ZUJ5RW1haWwsXG59IGZyb20gXCIuLi9tb2RlbHMvdXNlck1vZGVsXCI7XG5pbXBvcnQgZXhwcmVzcyBmcm9tIFwiZXhwcmVzc1wiO1xuaW1wb3J0IGJvZHlQYXJzZXIgZnJvbSBcImJvZHktcGFyc2VyXCI7XG5cbmNvbnN0IHVzZXJSb3V0ZXIgPSBleHByZXNzLlJvdXRlcigpO1xuLy8gdXNlclJvdXRlci51c2UoYm9keVBhcnNlci51cmxlbmNvZGVkKHsgZXh0ZW5kZWQ6IGZhbHNlIH0pKTtcbnVzZXJSb3V0ZXIudXNlKGJvZHlQYXJzZXIuanNvbigpKTtcblxudXNlclJvdXRlci5wb3N0KFwiL2xvZ2luXCIsIGFzeW5jICgpID0+IHt9KTtcblxudXNlclJvdXRlci5wb3N0KFwiL3NpZ251cFwiLCBhc3luYyAocmVxLCByZXMpID0+IHtcblx0Y29uc3QgZmluZFJlc3VsdCA9IGF3YWl0IGZpbmQoeyBlbWFpbDogcmVxLmJvZHkuZW1haWwgfSk7XG5cdGNvbnNvbGUubG9nKFwiZmluZFJlc3VsdFwiLCBmaW5kUmVzdWx0KTtcblx0aWYgKGZpbmRSZXN1bHQubGVuZ3RoKSB7XG5cdFx0cmVzLnNlbmQoeyBtZXNzYWdlOiBcIlVzZXIgYWxyZWFkeSBleGlzdHMuXCIgfSk7XG5cdFx0cmV0dXJuO1xuXHR9XG5cdGNvbnN0IGVudHJ5ID0gYXdhaXQgY3JlYXRlKHtcblx0XHQuLi5yZXEuYm9keSxcblx0XHRlbWFpbDogcmVxLmJvZHkuZW1haWwsXG5cdFx0cGFzc3dvcmQ6IHJlcS5ib2R5LnBhc3N3b3JkLFxuXHR9KTtcblx0cmVzLnNlbmQoZW50cnkpO1xufSk7XG5cbnVzZXJSb3V0ZXIucG9zdChcIi91cGRhdGVcIiwgYXN5bmMgKHJlcSwgcmVzKSA9PiB7XG5cdGNvbnN0IHJlc3VsdCA9IGF3YWl0IHVwZGF0ZUJ5RW1haWwocmVxLmJvZHkuZW1haWwsIHJlcS5ib2R5LmRhdGEpO1xuXHRyZXMuc2VuZChyZXN1bHQpO1xufSk7XG5cbnVzZXJSb3V0ZXIucG9zdChcIi9kZWxldGVcIiwgYXN5bmMgKHJlcSwgcmVzKSA9PiB7XG5cdGNvbnN0IHJlc3VsdCA9IGF3YWl0IGRlbGV0ZUJ5RW1haWwocmVxLmJvZHkuZW1haWwpO1xuXHRyZXMuc2VuZChyZXN1bHQpO1xufSk7XG5cbmV4cG9ydCBkZWZhdWx0IHVzZXJSb3V0ZXI7XG4iLCJpbXBvcnQgaHR0cHMgZnJvbSBcImh0dHBzXCI7XG5pbXBvcnQgZnMgZnJvbSBcImZzXCI7XG5pbXBvcnQgZXhwcmVzcyBmcm9tIFwiZXhwcmVzc1wiO1xuaW1wb3J0IG1vbmdvb3NlIGZyb20gXCJtb25nb29zZVwiO1xuaW1wb3J0IE1lbWNhY2hlZCBmcm9tIFwibWVtY2FjaGVkXCI7XG4vLyBpbXBvcnQgYm9keVBhcnNlciBmcm9tIFwiYm9keS1wYXJzZXJcIjtcbmltcG9ydCBjb29raWVQYXJzZXIgZnJvbSBcImNvb2tpZS1wYXJzZXJcIjtcbmltcG9ydCB7IGhhbmRsZVJlbmRlciB9IGZyb20gXCIuL21pZGRsZXdhcmVzL2hhbmRsZVJlbmRlclwiO1xuXG5pbXBvcnQgdXNlclJvdXRlciBmcm9tIFwiLi9jb250cm9sbGVycy91c2VyQ29udHJvbGxlclwiO1xuXG5pbXBvcnQganNkb20gZnJvbSBcImpzZG9tXCI7XG5pbXBvcnQgcGRmIGZyb20gXCJodG1sLXBkZlwiO1xuXG4vLyBtb25nb29zZS5jb25uZWN0KFxuLy8gXHRcIm1vbmdvZGI6Ly9sb2NhbGhvc3Q6MjcwMTcvdGVzdDFcIixcbi8vIFx0e1xuLy8gXHRcdHVzZU5ld1VybFBhcnNlcjogdHJ1ZSxcbi8vIFx0XHR1c2VVbmlmaWVkVG9wb2xvZ3k6IHRydWUsXG4vLyBcdH0sXG4vLyBcdChlcnIpID0+IHtcbi8vIFx0XHRpZiAoZXJyKSB7XG4vLyBcdFx0XHRjb25zb2xlLmVycm9yKFwiTW9uZ28gREIgQ29ubmVjdGlvbiBFcnJvciFcIik7XG4vLyBcdFx0XHRjb25zb2xlLmVycm9yKGVycik7XG4vLyBcdFx0fSBlbHNlIHtcbi8vIFx0XHRcdGNvbnNvbGUubG9nKFwiTW9uZ28gREIgQ29ubmVjdGVkXCIpO1xuLy8gXHRcdH1cbi8vIFx0fVxuLy8gKTtcblxuLy9cbmNvbnN0IGh0bWwgPSBcIjxodG1sPjxoZWFkPjwvaGVhZD48Ym9keT48L2JvZHk+PC9odG1sPlwiO1xuXG5jb25zdCBvcHRpb25zID0ge307XG5cbmNvbnN0IHsgSlNET00gfSA9IGpzZG9tO1xuZ2xvYmFsLmRvbSA9IG5ldyBKU0RPTShodG1sLCBvcHRpb25zKTtcbmdsb2JhbC53aW5kb3cgPSBkb20ud2luZG93O1xuZ2xvYmFsLmRvY3VtZW50ID0gZG9tLndpbmRvdy5kb2N1bWVudDtcbi8vXG5cbmNvbnN0IG1lbWNhY2hlZCA9IG5ldyBNZW1jYWNoZWQoXCIxMjcuMC4wLjE6MTEyMTFcIik7XG5cbmNvbnN0IGFwcCA9IGV4cHJlc3MoKTtcbmFwcC51c2UoZXhwcmVzcy5zdGF0aWMoXCJwdWJsaWNcIikpO1xuYXBwLnVzZShjb29raWVQYXJzZXIoKSk7XG5cbmNvbnN0IHB1YmxpY1JvdXRlciA9IGV4cHJlc3MuUm91dGVyKCk7XG5jb25zdCBhcGlSb3V0ZXIgPSBleHByZXNzLlJvdXRlcigpO1xuXG5hcHAudXNlKFwiL1wiLCBwdWJsaWNSb3V0ZXIpO1xuYXBwLnVzZShcIi9ob21lXCIsIHB1YmxpY1JvdXRlcik7XG5hcHAudXNlKFwiL2FwaVwiLCBhcGlSb3V0ZXIpO1xuXG4vLyBwdWJsaWNSb3V0ZXIudXNlKGhhbmRsZVJlbmRlcigpKTtcblxucHVibGljUm91dGVyLmdldChcIi9cIiwgaGFuZGxlUmVuZGVyKCksIChyZXEsIHJlcykgPT4ge1xuXHRtZW1jYWNoZWQuc2V0KFwic3RoXCIsIFwiYXNkXCIsIDMwMCwgZnVuY3Rpb24gKGVyciwgZGF0YSkge1xuXHRcdGNvbnNvbGUubG9nKFwiLyBlcnIsIGRhdGFcIiwgZXJyLCBkYXRhKTtcblx0fSk7XG5cblx0Ly8gcmVzLmNvb2tpZShcImFjY2Vzcy10b2tlblwiLCBwcm9jZXNzLmVudi5QT1JULCB7XG5cdC8vIFx0ZXhwaXJlczogbmV3IERhdGUoRGF0ZS5ub3coKSArIDEyMDAwKSxcblx0Ly8gfSk7XG5cdGNvbnNvbGUubG9nKFwiL1wiKTtcblx0Y29uc29sZS5sb2coXCJyZXEuUmVmZXJlclwiLCByZXEuZ2V0KFwiUmVmZXJyZXJcIikpO1xuXHQvLyBjb25zb2xlLmxvZyhcInJlcS5jb29raWVzXCIsIHJlcS5oZWFkZXJzLmNvb2tpZSk7XG5cdHJlcy5oZWFkZXIoXCJBY2Nlc3MtQ29udHJvbC1BbGxvdy1DcmVkZW50aWFsc1wiLCB0cnVlKTtcblx0Ly8gcmVzLmhlYWRlcihcIkFjY2Vzcy1Db250cm9sLUFsbG93LU9yaWdpblwiLCBcImh0dHBzOi8vbGRldi5saW1iZXIuY28uaW46OTAwMFwiKTtcblx0cmVzLmNvb2tpZShcImFjY2Vzcy10b2tlblwiLCBwcm9jZXNzLmVudi5QT1JULCB7IGh0dHBPbmx5OiB0cnVlIH0pO1xuXHRyZXMuY29va2llKFwicmFuZG9tXCIsIFwicmFuZG9tXCIpO1xuXHRyZXMuc2VuZChyZXMubG9jYWxzLmh0bWwpO1xufSk7XG5cbnB1YmxpY1JvdXRlci5nZXQoXCIvaG9tZVwiLCBoYW5kbGVSZW5kZXIoKSwgKHJlcSwgcmVzKSA9PiB7XG5cdG1lbWNhY2hlZC5nZXQoXCJzdGhcIiwgZnVuY3Rpb24gKGVyciwgZGF0YSkge1xuXHRcdGNvbnNvbGUubG9nKFwiL2hvbWUgZXJyLCBkYXRhXCIsIGVyciwgZGF0YSk7XG5cdH0pO1xuXG5cdGNvbnNvbGUubG9nKFwiL2hvbWVcIik7XG5cdGNvbnNvbGUubG9nKFwicmVxLlJlZmVyZXJcIiwgcmVxLmdldChcIlJlZmVycmVyXCIpKTtcblx0Y29uc29sZS5sb2coXCJyZXEuT3JpZ2luXCIsIHJlcS5nZXQoXCJPcmlnaW5cIikpO1xuXHRjb25zb2xlLmxvZyhcInJlcS5jb29raWVzXCIsIHJlcS5oZWFkZXJzLmNvb2tpZSk7XG5cdGNvbnNvbGUubG9nKFwicmVxLmNvb2tpZXNcIiwgcmVxLmNvb2tpZXMpO1xuXHQvLyByZXMuaGVhZGVyKFxuXHQvLyBcdFwiQ29udGVudC1TZWN1cml0eS1Qb2xpY3lcIixcblx0Ly8gXHRcImZyYW1lLWFuY2VzdG9ycyAnc2VsZic7IGNvbm5lY3Qtc3JjICdzZWxmJztcIlxuXHQvLyApO1xuXHQvLyByZXMuY29va2llKFwiYWNjZXNzLXRva2VuXCIsIHByb2Nlc3MuZW52LlBPUlQpO1xuXHRyZXMuc2VuZChyZXMubG9jYWxzLmh0bWwpO1xufSk7XG5cbnB1YmxpY1JvdXRlci5nZXQoXCIvcHJvZHVjdHNcIiwgaGFuZGxlUmVuZGVyKCksIChyZXEsIHJlcykgPT4ge1xuXHRjb25zb2xlLmxvZyhcIi9wcm9kdWN0c1wiKTtcblx0Y29uc29sZS5sb2coXCJyZXEuUmVmZXJlclwiLCByZXEuZ2V0KFwiUmVmZXJyZXJcIikpO1xuXHRjb25zb2xlLmxvZyhcInJlcS5jb29raWVzXCIsIHJlcS5oZWFkZXJzLmNvb2tpZSk7XG5cdC8vIHJlcy5oZWFkZXIoXCJYLUZyYW1lLU9wdGlvbnNcIiwgXCJkZW55XCIpO1xuXHQvLyByZXMuaGVhZGVyKFxuXHQvLyBcdFwiQ29udGVudC1TZWN1cml0eS1Qb2xpY3lcIixcblx0Ly8gXHRcImZyYW1lLWFuY2VzdG9ycyAnc2VsZicgaHR0cHM6Ly9sZGV2LmxpbWJlci5jby5pbjo5MDAwIGh0dHBzOi8vbG9jYWxob3N0OjkwMDA7IGNvbm5lY3Qtc3JjICdzZWxmJ1wiXG5cdC8vICk7XG5cdC8vIHJlcy5oZWFkZXIoXCJDb250ZW50LVNlY3VyaXR5LVBvbGljeVwiLCBcImNvbm5lY3Qtc3JjICdzZWxmJztcIik7XG5cdC8vIHJlcy5jb29raWUoXCJhY2Nlc3MtdG9rZW5cIiwgcHJvY2Vzcy5lbnYuUE9SVCk7XG5cdHJlcy5zZW5kKHJlcy5sb2NhbHMuaHRtbCk7XG59KTtcblxucHVibGljUm91dGVyLmdldChcIi9wZGZcIiwgaGFuZGxlUmVuZGVyKCksIChyZXEsIHJlcykgPT4ge1xuXHRjb25zb2xlLmxvZyhcIi9wZGZcIik7XG5cblx0Y29uc3QgZmlsZW5hbWUgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcblx0Y29uc3Qgb3B0aW9ucyA9IHsgZm9ybWF0OiBcIkxldHRlclwiIH07XG5cdHBkZlxuXHRcdC5jcmVhdGUocmVzLmxvY2Fscy5odG1sLCBvcHRpb25zKVxuXHRcdC50b0ZpbGUoXCIuL291dHB1dC9cIiArIGZpbGVuYW1lICsgXCIucGRmXCIsIGZ1bmN0aW9uIChlcnIsIHJlcykge1xuXHRcdFx0aWYgKGVycikgcmV0dXJuIGNvbnNvbGUubG9nKGVycik7XG5cdFx0XHRjb25zb2xlLmxvZyhyZXMpO1xuXHRcdH0pO1xuXG5cdHJlcy5zZW5kKHJlcy5sb2NhbHMuaHRtbCk7XG59KTtcblxuYXBpUm91dGVyLnVzZSgocmVxLCByZXMsIG5leHQpID0+IHtcblx0Y29uc29sZS5sb2coXCJhcGlSb3V0ZXJcIiwgcmVxLmdldChcIk9yaWdpblwiKSk7XG5cdC8vIHJlcy5oZWFkZXIoXCJBY2Nlc3MtQ29udHJvbC1BbGxvdy1PcmlnaW5cIiwgcmVxLmdldChcIk9yaWdpblwiKSk7XG5cdG5leHQoKTtcbn0pO1xuXG5hcGlSb3V0ZXIudXNlKFwiL3VzZXJzXCIsIHVzZXJSb3V0ZXIpO1xuXG5hcGlSb3V0ZXIub3B0aW9ucyhcIi91c2VyXCIsIChyZXEsIHJlcykgPT4ge1xuXHRjb25zb2xlLmxvZyhcIioqKipIRVJFKioqKlwiKTtcblx0Ly8gcmVzLmhlYWRlcihcIkFjY2Vzcy1Db250cm9sLUFsbG93LUNyZWRlbnRpYWxzXCIsIHRydWUpO1xuXHRyZXMuaGVhZGVyKFwiQWNjZXNzLUNvbnRyb2wtQWxsb3ctSGVhZGVyc1wiLCBcIkNvbnRlbnQtVHlwZVwiKTtcblx0cmVzLnNlbmQoKTtcbn0pO1xuXG5hcGlSb3V0ZXIuZ2V0KFwiL3VzZXJcIiwgKHJlcSwgcmVzKSA9PiB7XG5cdC8vIHJlcy5jb29raWUoXCJhY2Nlc3MtdG9rZW5cIiwgcHJvY2Vzcy5lbnYuUE9SVCwge1xuXHQvLyBcdGV4cGlyZXM6IG5ldyBEYXRlKERhdGUubm93KCkgKyAxMjAwMCksXG5cdC8vIH0pO1xuXHRjb25zb2xlLmxvZyhcIi91c2VyXCIpO1xuXHRjb25zb2xlLmxvZyhcInJlcS5SZWZlcmVyXCIsIHJlcS5nZXQoXCJSZWZlcnJlclwiKSk7XG5cdGNvbnNvbGUubG9nKFwicmVxLmNvb2tpZXNcIiwgcmVxLmhlYWRlcnMuY29va2llKTtcblx0Ly8gcmVzLnNldChcIkNhY2hlLUNvbnRyb2xcIiwgXCJwcml2YXRlLCBtYXgtYWdlPTMwXCIpO1xuXHQvLyByZXMuc2V0KFwiQ2FjaGUtQ29udHJvbFwiLCBcInByaXZhdGVcIik7XG5cdHJlcy5oZWFkZXIoXCJBY2Nlc3MtQ29udHJvbC1BbGxvdy1DcmVkZW50aWFsc1wiLCB0cnVlKTtcblx0cmVzXG5cdFx0LmNvb2tpZShcImFjY2Vzcy10b2tlblwiLCBwcm9jZXNzLmVudi5QT1JULCB7XG5cdFx0XHRleHBpcmVzOiBuZXcgRGF0ZShEYXRlLm5vdygpICsgMiAqIDYwICogMTAwMCksXG5cdFx0XHRzYW1lU2l0ZTogXCJOb25lXCIsXG5cdFx0XHRzZWN1cmU6IHRydWUsXG5cdFx0fSlcblx0XHQuY29va2llKFwic29tZXN0cmluZ1wiLCBcInNvbWVyYW5kb21zdHJpbmdcIiwge1xuXHRcdFx0ZXhwaXJlczogbmV3IERhdGUoRGF0ZS5ub3coKSArIDIgKiA2MCAqIDEwMDApLFxuXHRcdFx0c2FtZVNpdGU6IFwiTm9uZVwiLFxuXHRcdFx0c2VjdXJlOiB0cnVlLFxuXHRcdH0pO1xuXHRyZXMuc2VuZChbXCJhcnNlbmFsXCIsIFwiY2hlbHNlYVwiLCBcIm1hbnVcIiwgNF0pO1xuXHRjb25zb2xlLmxvZyhcInNlbnRcIik7XG59KTtcblxuYXBpUm91dGVyLnBvc3QoXCIvdXNlclwiLCAocmVxLCByZXMpID0+IHtcblx0Ly8gcmVzLmNvb2tpZShcImFjY2Vzcy10b2tlblwiLCBwcm9jZXNzLmVudi5QT1JULCB7XG5cdC8vIFx0ZXhwaXJlczogbmV3IERhdGUoRGF0ZS5ub3coKSArIDEyMDAwKSxcblx0Ly8gfSk7XG5cdGNvbnNvbGUubG9nKFwiL3VzZXJcIik7XG5cdGNvbnNvbGUubG9nKFwicmVxLlJlZmVyZXJcIiwgcmVxLmdldChcIlJlZmVycmVyXCIpKTtcblx0Y29uc29sZS5sb2coXCJyZXEuY29va2llc1wiLCByZXEuaGVhZGVycy5jb29raWUpO1xuXHRyZXMuaGVhZGVyKFwiQWNjZXNzLUNvbnRyb2wtQWxsb3ctQ3JlZGVudGlhbHNcIiwgdHJ1ZSk7XG5cdHJlcy5jb29raWUoXCJhY2Nlc3MtdG9rZW5cIiwgcHJvY2Vzcy5lbnYuUE9SVCk7XG5cdHJlcy5zZW5kKFtcImFyc2VuYWxcIiwgXCJjaGVsc2VhXCJdKTtcbn0pO1xuXG5hcHAuZ2V0KFwiKlwiLCAocmVxLCByZXMsIG5leHQpID0+IHtcblx0cmVzLnJlZGlyZWN0KFwiL1wiKTtcbn0pO1xuXG4vLyBhcHAubGlzdGVuKHByb2Nlc3MuZW52LlBPUlQsICgpID0+IHtcbi8vIFx0Y29uc29sZS5sb2coXCIjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjI1wiKTtcbi8vIFx0Y29uc29sZS5sb2coXCJzZXJ2ZXIgcnVubmluZyBvbiBwb3J0OiBcIiwgcHJvY2Vzcy5lbnYuUE9SVCk7XG4vLyBcdGNvbnNvbGUubG9nKFwiIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyNcIik7XG4vLyB9KTtcblxuaHR0cHNcblx0LmNyZWF0ZVNlcnZlcihcblx0XHR7XG5cdFx0XHRrZXk6IGZzLnJlYWRGaWxlU3luYyhcImxvY2FsaG9zdC1wcml2a2V5LnBlbVwiKSxcblx0XHRcdGNlcnQ6IGZzLnJlYWRGaWxlU3luYyhcImxvY2FsaG9zdC1jZXJ0LnBlbVwiKSxcblx0XHR9LFxuXHRcdGFwcFxuXHQpXG5cdC5saXN0ZW4ocHJvY2Vzcy5lbnYuUE9SVCwgKCkgPT4ge1xuXHRcdGNvbnNvbGUubG9nKFwiIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyNcIik7XG5cdFx0Y29uc29sZS5sb2coXCJzZXJ2ZXIgcnVubmluZyBvbiBwb3J0OiBcIiwgcHJvY2Vzcy5lbnYuUE9SVCk7XG5cdFx0Y29uc29sZS5sb2coXCIjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjI1wiKTtcblx0fSk7XG4iLCJpbXBvcnQgUmVhY3QgZnJvbSBcInJlYWN0XCI7XG5pbXBvcnQgeyByZW5kZXJUb1N0cmluZywgcmVuZGVyVG9Ob2RlU3RyZWFtIH0gZnJvbSBcInJlYWN0LWRvbS9zZXJ2ZXJcIjtcbmltcG9ydCB7IFN0YXRpY1JvdXRlciB9IGZyb20gXCJyZWFjdC1yb3V0ZXItZG9tXCI7XG5pbXBvcnQgeyBjcmVhdGVTdG9yZSB9IGZyb20gXCJyZWR1eFwiO1xuaW1wb3J0IHsgUHJvdmlkZXIgfSBmcm9tIFwicmVhY3QtcmVkdXhcIjtcblxuaW1wb3J0IHJlZHVjZXJzIGZyb20gXCIuLi8uLi9zaGFyZWQvY29uZmlncy9yZWR1Y2Vyc1wiO1xuaW1wb3J0IEFwcCBmcm9tIFwiLi4vLi4vc2hhcmVkL2FwcFwiO1xuXG4vLyBpbXBvcnQgeyBDaHVua0V4dHJhY3RvciB9IGZyb20gXCJAbG9hZGFibGUvc2VydmVyXCI7XG4vLyBpbXBvcnQgcGF0aCBmcm9tIFwicGF0aFwiO1xuXG4vLyBpbXBvcnQgcmVuZGVyLCB7IHNlbmQgfSBmcm9tIFwiLi4vcHJvY2Vzc2VzL3JlbmRlci9yZW5kZXJcIjtcblxuLy8gY29uc3Qgc3RhdHNGaWxlID0gcGF0aC5yZXNvbHZlKFxuLy8gXHRfX2Rpcm5hbWUsXG4vLyBcdFwicHVibGljL2Fzc2V0cy93ZWIvbG9hZGFibGUtc3RhdHMuanNvblwiXG4vLyApO1xuXG4vLyBjb25zdCBzdGF0c0ZpbGUgPSBwYXRoLnJlc29sdmUoX19kaXJuYW1lLCBcIi4vd2ViL2xvYWRhYmxlLXN0YXRzLmpzb25cIik7XG4vLyBjb25zb2xlLmxvZyhcInN0YXRzRmlsZVwiLCBzdGF0c0ZpbGUpO1xuXG5leHBvcnQgY29uc3QgaGFuZGxlUmVuZGVyID0gKG9wdGlvbnMpID0+IHtcblx0cmV0dXJuIGZ1bmN0aW9uIChyZXEsIHJlcywgbmV4dCkge1xuXHRcdC8vICoqKipcblx0XHQvLyBjb25zdCBzdG9yZSA9IGNyZWF0ZVN0b3JlKHJlZHVjZXJzKTtcblx0XHQvLyBjb25zdCB3ZWJFeHRyYWN0b3IgPSBuZXcgQ2h1bmtFeHRyYWN0b3Ioe1xuXHRcdC8vIFx0c3RhdHNGaWxlLFxuXHRcdC8vIFx0ZW50cnlwb2ludHM6IFtcImluZGV4LmpzXCJdLFxuXHRcdC8vIH0pO1xuXHRcdC8vIGNvbnN0IGpzeCA9IHdlYkV4dHJhY3Rvci5jb2xsZWN0Q2h1bmtzKFxuXHRcdC8vIFx0PFByb3ZpZGVyIHN0b3JlPXtzdG9yZX0+XG5cdFx0Ly8gXHRcdDxTdGF0aWNSb3V0ZXIgbG9jYXRpb249e3JlcS51cmx9PlxuXHRcdC8vIFx0XHRcdDxBcHAgLz5cblx0XHQvLyBcdFx0PC9TdGF0aWNSb3V0ZXI+XG5cdFx0Ly8gXHQ8L1Byb3ZpZGVyPlxuXHRcdC8vICk7XG5cdFx0Ly8gY29uc3QgaHRtbCA9IHJlbmRlclRvU3RyaW5nKGpzeCk7XG5cdFx0Ly8gY29uc3QgcHJlbG9hZGVkU3RhdGUgPSBzdG9yZS5nZXRTdGF0ZSgpO1xuXHRcdC8vIHJlcy5sb2NhbHMuaHRtbCA9IHJlbmRlckNvZGVTcGxpdChodG1sLCB3ZWJFeHRyYWN0b3IsIHByZWxvYWRlZFN0YXRlKTtcblx0XHQvLyBuZXh0KCk7XG5cdFx0Ly8gKioqKlxuXHRcdC8vICoqKipcblx0XHRjb25zdCBzdG9yZSA9IGNyZWF0ZVN0b3JlKHJlZHVjZXJzKTtcblx0XHRjb25zdCBodG1sID0gcmVuZGVyVG9TdHJpbmcoXG5cdFx0XHQ8UHJvdmlkZXIgc3RvcmU9e3N0b3JlfT5cblx0XHRcdFx0PFN0YXRpY1JvdXRlciBsb2NhdGlvbj17cmVxLnVybH0+XG5cdFx0XHRcdFx0PEFwcCAvPlxuXHRcdFx0XHQ8L1N0YXRpY1JvdXRlcj5cblx0XHRcdDwvUHJvdmlkZXI+XG5cdFx0KTtcblx0XHRjb25zdCBwcmVsb2FkZWRTdGF0ZSA9IHN0b3JlLmdldFN0YXRlKCk7XG5cdFx0cmVzLmxvY2Fscy5odG1sID0gcmVuZGVyRnVsbFBhZ2UoaHRtbCwgcHJlbG9hZGVkU3RhdGUpO1xuXHRcdG5leHQoKTtcblx0XHQvLyAqKioqXG5cdFx0Ly8gKioqKlxuXHRcdC8vIHJlbmRlci5zZW5kKHsgc3RvcmUsIGxvY2F0aW9uOiByZXEudXJsIH0pO1xuXHRcdC8vIHJlbmRlci5vbihcIm1lc3NhZ2VcIiwgKG0pID0+IHtcblx0XHQvLyBcdGNvbnN0IGh0bWwgPSBtLmh0bWw7XG5cdFx0Ly8gXHRjb25zdCBwcmVsb2FkZWRTdGF0ZSA9IHN0b3JlLmdldFN0YXRlKCk7XG5cdFx0Ly8gXHRyZXMubG9jYWxzLmh0bWwgPSByZW5kZXJGdWxsUGFnZShodG1sLCBwcmVsb2FkZWRTdGF0ZSk7XG5cdFx0Ly8gXHRuZXh0KCk7XG5cdFx0Ly8gfSk7XG5cdFx0Ly8gKioqKlxuXHR9O1xufTtcblxuZXhwb3J0IGNvbnN0IHJlbmRlckZ1bGxQYWdlID0gKGh0bWwsIHByZWxvYWRlZFN0YXRlKSA9PiB7XG5cdHJldHVybiBgPCFET0NUWVBFIGh0bWw+XG4gICAgICA8aGVhZD5cbiAgICAgIFx0PCEtLTxtZXRhIGh0dHAtZXF1aXY9XCJYLUZyYW1lLU9wdGlvbnNcIiBjb250ZW50PVwiZGVueVwiPi0tPjwhLS0gVGhpcyBkb2Vzbid0IHdvcmsgLS0+XG4gICAgICAgIDx0aXRsZT5SZWFjdCBTU1I8L3RpdGxlPlxuICAgICAgICA8bGluayByZWw9XCJzdHlsZXNoZWV0XCIgaHJlZj1cIi9hc3NldHMvaW5kZXguY3NzXCI+XG4gICAgICAgIDxzY3JpcHQgZGVmZXIgc3JjPVwiL2Fzc2V0cy9pbmRleC5qc1wiPjwvc2NyaXB0PlxuICAgICAgPC9oZWFkPlxuICAgICAgPGJvZHk+XG4gICAgICAgIDxkaXYgaWQ9XCJyb290XCI+JHtodG1sfTwvZGl2PlxuICAgICAgICA8c2NyaXB0PlxuICAgICAgICBcdHdpbmRvdy5fX1BSRUxPQURFRF9TVEFURV9fID0gJHtKU09OLnN0cmluZ2lmeShwcmVsb2FkZWRTdGF0ZSl9XG4gICAgICAgIDwvc2NyaXB0PlxuICAgICAgPC9ib2R5PlxuICAgIDwvaHRtbD5gO1xufTtcblxuLy8gZXhwb3J0IGNvbnN0IHJlbmRlckNvZGVTcGxpdCA9IChodG1sLCB3ZWJFeHRyYWN0b3IsIHByZWxvYWRlZFN0YXRlKSA9PiB7XG4vLyBcdGNvbnNvbGUubG9nKFwid2ViRXh0cmFjdG9yLmdldExpbmtUYWdzXCIsIHdlYkV4dHJhY3Rvci5nZXRMaW5rVGFncygpKTtcbi8vIFx0Y29uc29sZS5sb2coXCJ3ZWJFeHRyYWN0b3IuZ2V0U2NyaXB0VGFnc1wiLCB3ZWJFeHRyYWN0b3IuZ2V0U2NyaXB0VGFncygpKTtcbi8vIFx0cmV0dXJuIGA8IURPQ1RZUEUgaHRtbD5cbi8vICAgICAgIDxoZWFkPlxuLy8gICAgICAgICA8dGl0bGU+UmVhY3QgU1NSPC90aXRsZT5cbi8vICAgICAgICAgJHt3ZWJFeHRyYWN0b3IuZ2V0TGlua1RhZ3MoKX1cbi8vICAgICAgICAgJHt3ZWJFeHRyYWN0b3IuZ2V0U2NyaXB0VGFncygpfVxuLy8gICAgICAgPC9oZWFkPlxuLy8gICAgICAgPGJvZHk+XG4vLyAgICAgICAgIDxkaXYgaWQ9XCJyb290XCI+JHtodG1sfTwvZGl2PlxuLy8gICAgICAgICA8c2NyaXB0PlxuLy8gICAgICAgICBcdHdpbmRvdy5fX1BSRUxPQURFRF9TVEFURV9fID0gJHtKU09OLnN0cmluZ2lmeShwcmVsb2FkZWRTdGF0ZSl9XG4vLyAgICAgICAgIDwvc2NyaXB0PlxuLy8gICAgICAgPC9ib2R5PlxuLy8gICAgIDwvaHRtbD5gO1xuLy8gfTtcbiIsImltcG9ydCBtb25nb29zZSBmcm9tIFwibW9uZ29vc2VcIjtcblxuY29uc3QgVXNlclNjaGVtYSA9IG1vbmdvb3NlLlNjaGVtYSh7XG5cdF9pZDogeyB0eXBlOiBtb25nb29zZS5TY2hlbWEuVHlwZXMuT2JqZWN0SWQsIHJlcXVpcmVkOiB0cnVlLCBhdXRvOiB0cnVlIH0sXG5cdGVtYWlsOiB7XG5cdFx0dHlwZTogU3RyaW5nLFxuXHRcdHJlcXVpcmVkOiB0cnVlLFxuXHRcdHVuaXF1ZTogdHJ1ZSxcblx0fSxcblx0cGFzc3dvcmQ6IHsgdHlwZTogU3RyaW5nLCByZXF1aXJlZDogdHJ1ZSB9LFxufSk7XG5cbmNvbnN0IFVzZXIgPSBtb25nb29zZS5tb2RlbChcIlVzZXJzXCIsIFVzZXJTY2hlbWEsIFwiVXNlcnNcIik7XG5cbmV4cG9ydCBjb25zdCBjcmVhdGUgPSBhc3luYyBmdW5jdGlvbiAob2JqKSB7XG5cdGNvbnN0IGVtYWlsID0gb2JqLmVtYWlsO1xuXHRjb25zdCBwYXNzd29yZCA9IG9iai5wYXNzd29yZDtcblx0Y29uc29sZS5sb2coXCJVc2VyXCIsIHsgLi4ub2JqLCBlbWFpbCwgcGFzc3dvcmQgfSk7XG5cdGNvbnN0IHVzZXIgPSBuZXcgVXNlcih7IC4uLm9iaiwgZW1haWwsIHBhc3N3b3JkIH0pO1xuXHRyZXR1cm4gYXdhaXQgdXNlci5zYXZlKCk7XG59O1xuXG5leHBvcnQgY29uc3QgZmluZCA9IGFzeW5jIGZ1bmN0aW9uIChvYmopIHtcblx0Y29uc3QgZW1haWwgPSBvYmouZW1haWw7XG5cdGNvbnN0IHJlc3VsdCA9IGF3YWl0IFVzZXIuZmluZCh7IGVtYWlsIH0pLmxlYW4oKTtcblx0cmV0dXJuIHJlc3VsdDtcbn07XG5cbmV4cG9ydCBjb25zdCB1cGRhdGVCeUVtYWlsID0gYXN5bmMgZnVuY3Rpb24gKGVtYWlsLCBvYmopIHtcblx0Y29uc3QgcmVzdWx0ID0gYXdhaXQgVXNlci51cGRhdGUoeyBlbWFpbCB9LCBvYmopO1xuXHRyZXR1cm4gcmVzdWx0O1xufTtcblxuZXhwb3J0IGNvbnN0IGRlbGV0ZUJ5RW1haWwgPSBhc3luYyBmdW5jdGlvbiAoZW1haWwpIHtcblx0Y29uc3QgcmVzdWx0ID0gYXdhaXQgVXNlci5kZWxldGVPbmUoeyBlbWFpbCB9KTtcblx0cmV0dXJuIHJlc3VsdDtcbn07XG5cbmV4cG9ydCBkZWZhdWx0IFVzZXI7XG4iLCJpbXBvcnQgUmVhY3QgZnJvbSBcInJlYWN0XCI7XG5cbmltcG9ydCBSb3V0ZXMgZnJvbSBcIi4vY29uZmlncy9yb3V0ZXMvcm91dGVzXCI7XG5cbmNvbnN0IEFwcCA9ICgpID0+IHtcblx0cmV0dXJuIDxSb3V0ZXM+PC9Sb3V0ZXM+O1xufTtcblxuZXhwb3J0IGRlZmF1bHQgQXBwO1xuIiwiaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IHsgUm91dGUsIFJlZGlyZWN0IH0gZnJvbSBcInJlYWN0LXJvdXRlci1kb21cIjtcblxuaW1wb3J0IHsgaXNBY2Nlc3NHcmFudGVkIH0gZnJvbSBcIi4uLy4uL3V0aWxzL2F1dGhVdGlsc1wiO1xuXG5leHBvcnQgY29uc3QgUHJpdmF0ZVJvdXRlID0gKHtcblx0Y29tcG9uZW50OiBDb21wb25lbnQsXG5cdHJlZGlyZWN0UGF0aCxcblx0Li4ucmVzdFxufSkgPT4ge1xuXHRyZXR1cm4gKFxuXHRcdDxSb3V0ZVxuXHRcdFx0ey4uLnJlc3R9XG5cdFx0XHRyZW5kZXI9eyhwcm9wcykgPT5cblx0XHRcdFx0aXNBY2Nlc3NHcmFudGVkKCkgPyAoXG5cdFx0XHRcdFx0PENvbXBvbmVudCB7Li4ucHJvcHN9IC8+XG5cdFx0XHRcdCkgOiAoXG5cdFx0XHRcdFx0PFJlZGlyZWN0IHRvPXtyZWRpcmVjdFBhdGh9IC8+XG5cdFx0XHRcdClcblx0XHRcdH1cblx0XHQvPlxuXHQpO1xufTtcbiIsImltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCB7IFJvdXRlLCBSZWRpcmVjdCB9IGZyb20gXCJyZWFjdC1yb3V0ZXItZG9tXCI7XG5cbmltcG9ydCB7IGlzQWNjZXNzR3JhbnRlZCB9IGZyb20gXCIuLi8uLi91dGlscy9hdXRoVXRpbHNcIjtcblxuZXhwb3J0IGNvbnN0IFB1YmxpY1JvdXRlID0gKHtcblx0Y29tcG9uZW50OiBDb21wb25lbnQsXG5cdHJlZGlyZWN0UGF0aCxcblx0Li4ucmVzdFxufSkgPT4ge1xuXHRyZXR1cm4gPFJvdXRlIHsuLi5yZXN0fSByZW5kZXI9eyhwcm9wcykgPT4gPENvbXBvbmVudCB7Li4ucHJvcHN9IC8+fSAvPjtcbn07XG4iLCJleHBvcnQgY29uc3QgaXNBY2Nlc3NHcmFudGVkID0gKCkgPT4ge1xuXHQvLyBjaGVjayBhdXRoIGhlYWRlciBpbiBiYWNrZW5kIGRvaW5nIGEgaGFjayBub3dcblx0aWYgKHR5cGVvZiBsb2NhbFN0b3JhZ2UgPT09IFwidW5kZWZpbmVkXCIpIHtcblx0XHRyZXR1cm4gdHJ1ZTtcblx0fVxuXG5cdGlmIChsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImFjY2Vzc190b2tlblwiKSkge1xuXHRcdHJldHVybiB0cnVlO1xuXHR9XG5cdHJldHVybiBmYWxzZTtcbn07XG4iLCJpbXBvcnQgeyBjb21iaW5lUmVkdWNlcnMgfSBmcm9tIFwicmVkdXhcIjtcblxuaW1wb3J0IHsgaG9tZVJlZHVjZXIgfSBmcm9tIFwiLi4vcGFnZXMvaG9tZS9kdWNrc1wiO1xuaW1wb3J0IHsgcHJvZHVjdHNSZWR1Y2VyIH0gZnJvbSBcIi4uL3BhZ2VzL3Byb2R1Y3RzL2R1Y2tzXCI7XG5cbmNvbnN0IHJlZHVjZXIgPSBjb21iaW5lUmVkdWNlcnMoe1xuXHQvLyAuLi5leGFtcGxlUmVkdWNlclxuXHRob21lUmVkdWNlcixcblx0cHJvZHVjdHNSZWR1Y2VyLFxufSk7XG5cbmV4cG9ydCBkZWZhdWx0IHJlZHVjZXI7XG4iLCJpbXBvcnQgUmVhY3QgZnJvbSBcInJlYWN0XCI7XG5pbXBvcnQgeyBTd2l0Y2gsIFJlZGlyZWN0IH0gZnJvbSBcInJlYWN0LXJvdXRlci1kb21cIjtcblxuaW1wb3J0IHsgUHJpdmF0ZVJvdXRlIH0gZnJvbSBcIi4uLy4uL2NvbW1vbi9jb21wb25lbnRzL3JvdXRlcy9Qcml2YXRlUm91dGVcIjtcbmltcG9ydCB7IFB1YmxpY1JvdXRlIH0gZnJvbSBcIi4uLy4uL2NvbW1vbi9jb21wb25lbnRzL3JvdXRlcy9QdWJsaWNSb3V0ZVwiO1xuXG5pbXBvcnQgSG9tZSBmcm9tIFwiLi4vLi4vcGFnZXMvaG9tZS9ob21lXCI7XG5pbXBvcnQgUHJvZHVjdHMgZnJvbSBcIi4uLy4uL3BhZ2VzL3Byb2R1Y3RzL3Byb2R1Y3RzXCI7XG5pbXBvcnQgUGRmIGZyb20gXCIuLi8uLi9wYWdlcy9wZGYvcGRmXCI7XG5cbmNvbnN0IFJvdXRlcyA9IChwcm9wcykgPT4ge1xuXHRyZXR1cm4gKFxuXHRcdDxTd2l0Y2g+XG5cdFx0XHR7Lyo8UHVibGljUm91dGUgcGF0aD1cIi9ob21lXCIgY29tcG9uZW50PXtIb21lfSByZWRpcmVjdFBhdGg9XCIvaG9tZVwiIC8+XG5cdFx0XHQ8UHVibGljUm91dGVcblx0XHRcdFx0cGF0aD1cIi9wcm9kdWN0c1wiXG5cdFx0XHRcdGNvbXBvbmVudD17UHJvZHVjdHN9XG5cdFx0XHRcdHJlZGlyZWN0UGF0aD1cIi9wcm9kdWN0c1wiXG5cdFx0XHQvPiovfVxuXHRcdFx0PFB1YmxpY1JvdXRlIHBhdGg9XCIvcGRmXCIgY29tcG9uZW50PXtQZGZ9IHJlZGlyZWN0UGF0aD1cIi9wZGZcIiAvPlxuXHRcdFx0PFJlZGlyZWN0IHRvPVwiL3BkZlwiIC8+XG5cdFx0PC9Td2l0Y2g+XG5cdCk7XG59O1xuXG5leHBvcnQgZGVmYXVsdCBSb3V0ZXM7XG4iLCJpbXBvcnQgeyBjcmVhdGVBY3Rpb24sIGNyZWF0ZVJlZHVjZXIsIGNyZWF0ZVNlbGVjdG9yIH0gZnJvbSBcIkByZWR1eGpzL3Rvb2xraXRcIjtcbmltcG9ydCB7IHB1dCwgYWxsLCB0YWtlTGF0ZXN0IH0gZnJvbSBcInJlZHV4LXNhZ2EvZWZmZWN0c1wiO1xuXG5jb25zdCBET19TT01FVEhJTkcgPSBcIltob21lXSBkbyBzb21ldGhpbmdcIjtcbmNvbnN0IFNPTUVUSElOR19ET05FID0gXCJbaG9tZV0gc29tZXRoaW5nIGRvbmVcIjtcblxuY29uc3QgaW5pdGlhbFN0YXRlID0ge1xuXHRsb2FkaW5nOiB0cnVlLFxufTtcblxuZXhwb3J0IGNvbnN0IHN0YXRlU2VsZWN0b3IgPSAoc3RhdGUpID0+IHN0YXRlLmhvbWVSZWR1Y2VyO1xuXG5leHBvcnQgY29uc3QgZ2V0TG9hZGluZyA9IGNyZWF0ZVNlbGVjdG9yKHN0YXRlU2VsZWN0b3IsIChzdGF0ZSkgPT4ge1xuXHRjb25zb2xlLmxvZyhzdGF0ZSk7XG5cdHJldHVybiBzdGF0ZS5sb2FkaW5nO1xufSk7XG5cbmV4cG9ydCBjb25zdCBob21lUmVkdWNlciA9IGNyZWF0ZVJlZHVjZXIoaW5pdGlhbFN0YXRlLCB7XG5cdFtTT01FVEhJTkdfRE9ORV06IChzdGF0ZSwgeyBwYXlsb2FkIH0pID0+IHtcblx0XHRzdGF0ZS5sb2FkaW5nID0gZmFsc2U7XG5cdH0sXG59KTtcblxuZXhwb3J0IGNvbnN0IGRvU29tZXRoaW5nID0gY3JlYXRlQWN0aW9uKERPX1NPTUVUSElORyk7XG5cbmZ1bmN0aW9uKiBpbml0SG9tZSh7IHBheWxvYWQgfSkge1xuXHR5aWVsZCBwdXQoeyB0eXBlOiBTT01FVEhJTkdfRE9ORSwgcGF5bG9hZDogeyBsb2FkaW5nOiBmYWxzZSB9IH0pO1xufVxuXG5leHBvcnQgZnVuY3Rpb24qIGhvbWVTYWdhKCkge1xuXHR5aWVsZCBhbGwoW3lpZWxkIHRha2VMYXRlc3QoRE9fU09NRVRISU5HLCBpbml0SG9tZSldKTtcbn1cbiIsImltcG9ydCBSZWFjdCwgeyB1c2VFZmZlY3QgfSBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tIFwicmVhY3QtcmVkdXhcIjtcblxuaW1wb3J0IHsgZ2V0TG9hZGluZywgZG9Tb21ldGhpbmcgfSBmcm9tIFwiLi9kdWNrc1wiO1xuXG5jb25zdCBhVGFnID0gPGEgaHJlZj1cImh0dHBzOi8vbG9jYWxob3N0OjMwMDAvaG9tZVwiPmxpbms8L2E+O1xuXG5jb25zdCBIb21lID0gKHsgbG9hZGluZywgZG9Tb21ldGhpbmcgfSkgPT4ge1xuXHR1c2VFZmZlY3QoKCkgPT4ge1xuXHRcdC8vIGZldGNoKFwiaHR0cHM6Ly9sZGV2LmxpbWJlci5jby5pbjo5MDAwL2FwaS91c2VyXCIsIHtcblx0XHQvLyBcdG1ldGhvZDogXCJwb3N0XCIsXG5cdFx0Ly8gXHQvLyB3aXRoQ3JlZGVudGlhbHM6IHRydWUsXG5cdFx0Ly8gXHRjcmVkZW50aWFsczogXCJpbmNsdWRlXCIsXG5cdFx0Ly8gXHRkYXRhOiB7XG5cdFx0Ly8gXHRcdGFzZDogXCJhc2RcIixcblx0XHQvLyBcdH0sXG5cdFx0Ly8gXHRoZWFkZXJzOiB7XG5cdFx0Ly8gXHRcdFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiLFxuXHRcdC8vIFx0fSxcblx0XHQvLyB9KVxuXHRcdC8vIFx0LnRoZW4oKGJvZHkpID0+IHtcblx0XHQvLyBcdFx0Y29uc29sZS5sb2coXCJib2R5XCIsIGJvZHkuaGVhZGVycy5nZXQoXCJzZXQtY29va2llXCIpKTtcblx0XHQvLyBcdFx0cmV0dXJuIGJvZHkuanNvbigpO1xuXHRcdC8vIFx0fSlcblx0XHQvLyBcdC50aGVuKChyZXMpID0+IGNvbnNvbGUubG9nKFwicmVzXCIsIHJlcykpO1xuXHRcdC8vIGZldGNoKFwiaHR0cHM6Ly9sZGV2LmxpbWJlci5pbjo5MDAxL2FwaS91c2VyXCIsIHtcblx0XHQvLyBcdGNyZWRlbnRpYWxzOiBcImluY2x1ZGVcIixcblx0XHQvLyB9KVxuXHRcdC8vIFx0LnRoZW4oKGJvZHkpID0+IHtcblx0XHQvLyBcdFx0Y29uc29sZS5sb2coXCJib2R5XCIsIGJvZHkuaGVhZGVycy5nZXQoXCJzZXQtY29va2llXCIpKTtcblx0XHQvLyBcdFx0cmV0dXJuIGJvZHkuanNvbigpO1xuXHRcdC8vIFx0fSlcblx0XHQvLyBcdC50aGVuKChyZXMpID0+IGNvbnNvbGUubG9nKFwicmVzXCIsIHJlcykpO1xuXHRcdC8vIHNldFRpbWVvdXQoKCkgPT4ge1xuXHRcdC8vIFx0ZG9Tb21ldGhpbmcoKTtcblx0XHQvLyB9LCAxMDAwKTtcblx0fSwgW10pO1xuXG5cdGNvbnN0IG9uQ2xpY2sgPSAoZXZlbnQpID0+IHtcblx0XHRjb25zb2xlLmxvZyhcIm9uQ2xpY2tcIiwgZXZlbnQpO1xuXHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cblx0XHQvLyBmZXRjaChcImh0dHBzOi8vbG9jYWxob3N0OjMwMDAvYXBpL3VzZXJzXCIpXG5cdFx0Ly8gXHQudGhlbigoYm9keSkgPT4gYm9keS5qc29uKCkpXG5cdFx0Ly8gXHQudGhlbigocmVzKSA9PiBjb25zb2xlLmxvZyhcInJlc1wiLCByZXMpKTtcblxuXHRcdGZldGNoKFwiaHR0cHM6Ly9sZGV2LmxpbWJlci5jby5pbjo5MDAwL2FwaS91c2VyXCIsIHtcblx0XHRcdGNyZWRlbnRpYWxzOiBcImluY2x1ZGVcIixcblx0XHRcdG1vZGU6IFwiY29yc1wiLFxuXHRcdH0pXG5cdFx0XHQudGhlbigoYm9keSkgPT4ge1xuXHRcdFx0XHRjb25zb2xlLmxvZyhcImJvZHlcIiwgYm9keS5oZWFkZXJzLmdldChcInNldC1jb29raWVcIikpO1xuXHRcdFx0XHRyZXR1cm4gYm9keS5qc29uKCk7XG5cdFx0XHR9KVxuXHRcdFx0LnRoZW4oKHJlcykgPT4gY29uc29sZS5sb2coXCJyZXNcIiwgcmVzKSk7XG5cdH07XG5cblx0Y29uc29sZS5sb2coXCIqKioqSG9tZSoqKipcIik7XG5cblx0cmV0dXJuIChcblx0XHQ8ZGl2PlxuXHRcdFx0PGEgaHJlZj1cImh0dHBzOi8vbG9jYWxob3N0OjkwMDAvaG9tZVwiIG9uQ2xpY2s9e29uQ2xpY2t9PlxuXHRcdFx0XHRsaW5rXG5cdFx0XHQ8L2E+XG5cdFx0XHQ8YnIgLz5cblx0XHRcdDxhIGhyZWY9XCJodHRwczovL2xkZXYubGltYmVyLmluOjkwMDEvYXBpL3VzZXJcIj5sbms8L2E+XG5cdFx0XHQ8YnIgLz5cblx0XHRcdDxpZnJhbWVcblx0XHRcdFx0c3JjPVwiaHR0cHM6Ly9sZGV2LmxpbWJlci5pbjo5MDAxL3Byb2R1Y3RzXCJcblx0XHRcdFx0cmVmZXJyZXJQb2xpY3k9XCJzdHJpY3Qtb3JpZ2luLXdoZW4tY3Jvc3Mtb3JpZ2luXCJcblx0XHRcdFx0c2FuZGJveD1cImFsbG93LXNjcmlwdHMgYWxsb3ctbW9kYWxzXCJcblx0XHRcdC8+XG5cdFx0XHR7Lyo8aWZyYW1lIHNyYz1cImh0dHBzOi8vZGV2ZWxvcGVyLm1vemlsbGEub3JnL2VuLVVTL2RvY3MvV2ViL0hUTUwvRWxlbWVudC9pZnJhbWVcIiAvPiovfVxuXHRcdDwvZGl2PlxuXHQpO1xufTtcblxuY29uc3QgZW5oYW5jZSA9IGNvbm5lY3QoXG5cdChzdGF0ZSkgPT4gKHtcblx0XHRsb2FkaW5nOiBnZXRMb2FkaW5nKHN0YXRlKSxcblx0fSksXG5cdHtcblx0XHRkb1NvbWV0aGluZyxcblx0fVxuKTtcblxuZXhwb3J0IGRlZmF1bHQgZW5oYW5jZShIb21lKTtcbiIsImltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcbi8vIGltcG9ydCB7IGNvbm5lY3QgfSBmcm9tIFwicmVhY3QtcmVkdXhcIjtcbmltcG9ydCAqIGFzIGQzIGZyb20gXCJkM1wiO1xuXG5leHBvcnQgY29uc3QgZ2V0QmFyQ2hhcnQgPSAod2lkdGgsIGhlaWdodCkgPT4ge1xuXHRjb25zdCBkYXRhID0gW1xuXHRcdHsgbGFiZWw6IFwiQVwiLCB2YWx1ZTogMTAgfSxcblx0XHR7IGxhYmVsOiBcIkJcIiwgdmFsdWU6IDUgfSxcblx0XHR7IGxhYmVsOiBcIkNcIiwgdmFsdWU6IDEwIH0sXG5cdFx0eyBsYWJlbDogXCJEXCIsIHZhbHVlOiAyMCB9LFxuXHRcdHsgbGFiZWw6IFwiRVwiLCB2YWx1ZTogMTAgfSxcblx0XHR7IGxhYmVsOiBcIkZcIiwgdmFsdWU6IDUgfSxcblx0XHR7IGxhYmVsOiBcIkdcIiwgdmFsdWU6IDIwIH0sXG5cdFx0eyBsYWJlbDogXCJIXCIsIHZhbHVlOiA3IH0sXG5cdFx0eyBsYWJlbDogXCJJXCIsIHZhbHVlOiAzIH0sXG5cdFx0eyBsYWJlbDogXCJKXCIsIHZhbHVlOiAxMCB9LFxuXHRdO1xuXG5cdC8vIGNvbnN0IGQzID0gZ2xvYmFsLmQzO1xuXHQvLyBjb25zb2xlLmxvZyhcImQzXCIsIGdsb2JhbC5kMyk7XG5cdGNvbnN0IG1hcmdpbiA9IHsgdG9wOiAyMCwgcmlnaHQ6IDIwLCBib3R0b206IDMwLCBsZWZ0OiA0MCB9O1xuXHRjb25zdCBjaGFydFdpZHRoID0gd2lkdGggLSBtYXJnaW4ubGVmdCAtIG1hcmdpbi5yaWdodDtcblx0Y29uc3QgY2hhcnRIZWlnaHQgPSBoZWlnaHQgLSBtYXJnaW4udG9wIC0gbWFyZ2luLmJvdHRvbTtcblxuXHRjb25zdCB4ID0gZDMuc2NhbGVCYW5kKCkucmFuZ2VSb3VuZChbMCwgY2hhcnRXaWR0aF0pLnBhZGRpbmdJbm5lcigwLjEpO1xuXHRjb25zdCB5ID0gZDMuc2NhbGVMaW5lYXIoKS5yYW5nZVJvdW5kKFtjaGFydEhlaWdodCwgMF0pO1xuXHRjb25zdCB6ID0gZDMuc2NhbGVPcmRpbmFsKCkucmFuZ2UoW1wiIzk5MjYwMFwiLCBcIiMwMDRkMDBcIiwgXCIjMDAzMzY2XCJdKTtcblx0eC5kb21haW4oXG5cdFx0ZGF0YS5tYXAoZnVuY3Rpb24gKGQpIHtcblx0XHRcdHJldHVybiBkLmxhYmVsO1xuXHRcdH0pXG5cdCk7XG5cdHkuZG9tYWluKFtcblx0XHQwLFxuXHRcdGQzLm1heChkYXRhLCBmdW5jdGlvbiAoZCkge1xuXHRcdFx0cmV0dXJuIGQudmFsdWU7XG5cdFx0fSksXG5cdF0pO1xuXG5cdGNvbnN0IGRpdiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7XG5cdGRpdi5zdHlsZS53aWR0aCA9IHdpZHRoICsgXCJweFwiO1xuXHRkaXYuc3R5bGUuaGVpZ2h0ID0gaGVpZ2h0ICsgXCJweFwiO1xuXG5cdGNvbnN0IHN2ZyA9IGQzXG5cdFx0LnNlbGVjdChkaXYpXG5cdFx0LmFwcGVuZChcInN2Z1wiKVxuXHRcdC5hdHRyKFwid2lkdGhcIiwgd2lkdGgpXG5cdFx0LmF0dHIoXCJoZWlnaHRcIiwgaGVpZ2h0KTtcblx0Y29uc3QgZyA9IHN2Z1xuXHRcdC5hcHBlbmQoXCJnXCIpXG5cdFx0LmF0dHIoXCJ0cmFuc2Zvcm1cIiwgXCJ0cmFuc2xhdGUoXCIgKyBtYXJnaW4ubGVmdCArIFwiLFwiICsgbWFyZ2luLnRvcCArIFwiKVwiKVxuXHRcdC5hdHRyKFwid2lkdGhcIiwgY2hhcnRXaWR0aClcblx0XHQuYXR0cihcImhlaWdodFwiLCBjaGFydEhlaWdodCk7XG5cdGcuc2VsZWN0QWxsKFwiLmJhclwiKVxuXHRcdC5kYXRhKGRhdGEpXG5cdFx0LmVudGVyKClcblx0XHQuYXBwZW5kKFwicmVjdFwiKVxuXHRcdC5hdHRyKFwiY2xhc3NcIiwgXCJiYXJcIilcblx0XHQuYXR0cihcInhcIiwgZnVuY3Rpb24gKGQpIHtcblx0XHRcdHJldHVybiB4KGQubGFiZWwpO1xuXHRcdH0pXG5cdFx0LmF0dHIoXCJ3aWR0aFwiLCB4LmJhbmR3aWR0aCgpKVxuXHRcdC5hdHRyKFwieVwiLCBmdW5jdGlvbiAoZCkge1xuXHRcdFx0cmV0dXJuIHkoZC52YWx1ZSk7XG5cdFx0fSlcblx0XHQuYXR0cihcImhlaWdodFwiLCBmdW5jdGlvbiAoZCkge1xuXHRcdFx0cmV0dXJuIGNoYXJ0SGVpZ2h0IC0geShkLnZhbHVlKTtcblx0XHR9KVxuXHRcdC5hdHRyKFwiZmlsbFwiLCBmdW5jdGlvbiAoZCkge1xuXHRcdFx0cmV0dXJuIHooZC5sYWJlbCk7XG5cdFx0fSk7XG5cblx0Zy5hcHBlbmQoXCJnXCIpXG5cdFx0LmF0dHIoXCJ0cmFuc2Zvcm1cIiwgXCJ0cmFuc2xhdGUoMCxcIiArIGNoYXJ0SGVpZ2h0ICsgXCIpXCIpXG5cdFx0LmNhbGwoZDMuYXhpc0JvdHRvbSh4KSk7XG5cblx0Zy5hcHBlbmQoXCJnXCIpLmNhbGwoZDMuYXhpc0xlZnQoeSkpO1xuXG5cdHJldHVybiBkaXYuaW5uZXJIVE1MO1xufTtcblxuZnVuY3Rpb24gY3JlYXRlTWFya3VwKGRhdGEpIHtcblx0cmV0dXJuIHsgX19odG1sOiBkYXRhIH07XG59XG5cbmNsYXNzIFBkZiBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG5cdGNvbnN0cnVjdG9yKHByb3BzKSB7XG5cdFx0c3VwZXIocHJvcHMpO1xuXHR9XG5cblx0cmVuZGVyKCkge1xuXHRcdGNvbnN0IGh0bWwgPSBnZXRCYXJDaGFydCg1MDAsIDUwMCk7XG5cdFx0cmV0dXJuIDxkaXYgZGFuZ2Vyb3VzbHlTZXRJbm5lckhUTUw9e2NyZWF0ZU1hcmt1cChodG1sKX0+PC9kaXY+O1xuXHR9XG59XG5cbmV4cG9ydCBkZWZhdWx0IFBkZjtcbiIsImltcG9ydCB7IGNyZWF0ZUFjdGlvbiwgY3JlYXRlUmVkdWNlciwgY3JlYXRlU2VsZWN0b3IgfSBmcm9tIFwiQHJlZHV4anMvdG9vbGtpdFwiO1xuaW1wb3J0IHsgcHV0LCBhbGwsIHRha2VMYXRlc3QgfSBmcm9tIFwicmVkdXgtc2FnYS9lZmZlY3RzXCI7XG5cbmNvbnN0IERPX1NPTUVUSElORyA9IFwiW3Byb2R1Y3RzXSBkbyBzb21ldGhpbmdcIjtcbmNvbnN0IFNPTUVUSElOR19ET05FID0gXCJbcHJvZHVjdHNdIHNvbWV0aGluZyBkb25lXCI7XG5cbmNvbnN0IGluaXRpYWxTdGF0ZSA9IHtcblx0bG9hZGluZzogdHJ1ZSxcbn07XG5cbmV4cG9ydCBjb25zdCBzdGF0ZVNlbGVjdG9yID0gKHN0YXRlKSA9PiBzdGF0ZS5wcm9kdWN0c1JlZHVjZXI7XG5cbmV4cG9ydCBjb25zdCBnZXRMb2FkaW5nID0gY3JlYXRlU2VsZWN0b3Ioc3RhdGVTZWxlY3RvciwgKHN0YXRlKSA9PiB7XG5cdGNvbnNvbGUubG9nKHN0YXRlKTtcblx0cmV0dXJuIHN0YXRlLmxvYWRpbmc7XG59KTtcblxuZXhwb3J0IGNvbnN0IHByb2R1Y3RzUmVkdWNlciA9IGNyZWF0ZVJlZHVjZXIoaW5pdGlhbFN0YXRlLCB7XG5cdFtTT01FVEhJTkdfRE9ORV06IChzdGF0ZSwgeyBwYXlsb2FkIH0pID0+IHtcblx0XHRzdGF0ZS5sb2FkaW5nID0gZmFsc2U7XG5cdH0sXG59KTtcblxuZXhwb3J0IGNvbnN0IGRvU29tZXRoaW5nID0gY3JlYXRlQWN0aW9uKERPX1NPTUVUSElORyk7XG5cbmZ1bmN0aW9uKiBpbml0UHJvZHVjdHMoeyBwYXlsb2FkIH0pIHtcblx0eWllbGQgcHV0KHsgdHlwZTogU09NRVRISU5HX0RPTkUsIHBheWxvYWQ6IHsgbG9hZGluZzogZmFsc2UgfSB9KTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uKiBwcm9kdWN0c1NhZ2EoKSB7XG5cdHlpZWxkIGFsbChbeWllbGQgdGFrZUxhdGVzdChET19TT01FVEhJTkcsIGluaXRQcm9kdWN0cyldKTtcbn1cbiIsImltcG9ydCBSZWFjdCwgeyB1c2VFZmZlY3QgfSBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tIFwicmVhY3QtcmVkdXhcIjtcblxuaW1wb3J0IHsgZ2V0TG9hZGluZywgZG9Tb21ldGhpbmcgfSBmcm9tIFwiLi9kdWNrc1wiO1xuXG5jb25zdCBhVGFnID0gPGEgaHJlZj1cImh0dHBzOi8vbG9jYWxob3N0OjMwMDAvaG9tZVwiPmxpbms8L2E+O1xuXG5jb25zdCBQcm9kdWN0cyA9ICh7IGxvYWRpbmcsIGRvU29tZXRoaW5nIH0pID0+IHtcblx0dXNlRWZmZWN0KCgpID0+IHtcblx0XHQvLyBmZXRjaChcImh0dHBzOi8vbGRldi5saW1iZXIuY28uaW46OTAwMS9hcGkvdXNlclwiLCB7XG5cdFx0Ly8gXHRtZXRob2Q6IFwicG9zdFwiLFxuXHRcdC8vIFx0Ly8gd2l0aENyZWRlbnRpYWxzOiB0cnVlLFxuXHRcdC8vIFx0Y3JlZGVudGlhbHM6IFwiaW5jbHVkZVwiLFxuXHRcdC8vIFx0ZGF0YToge1xuXHRcdC8vIFx0XHRhc2Q6IFwiYXNkXCIsXG5cdFx0Ly8gXHR9LFxuXHRcdC8vIFx0aGVhZGVyczoge1xuXHRcdC8vIFx0XHRcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIixcblx0XHQvLyBcdH0sXG5cdFx0Ly8gfSlcblx0XHQvLyBcdC50aGVuKChib2R5KSA9PiB7XG5cdFx0Ly8gXHRcdGNvbnNvbGUubG9nKFwiYm9keVwiLCBib2R5LmhlYWRlcnMuZ2V0KFwic2V0LWNvb2tpZVwiKSk7XG5cdFx0Ly8gXHRcdHJldHVybiBib2R5Lmpzb24oKTtcblx0XHQvLyBcdH0pXG5cdFx0Ly8gXHQudGhlbigocmVzKSA9PiBjb25zb2xlLmxvZyhcInJlc1wiLCByZXMpKTtcblx0XHQvLyBmZXRjaChcImh0dHBzOi8vbGRldi5saW1iZXIuaW46OTAwMS9hcGkvdXNlclwiLCB7XG5cdFx0Ly8gXHRjcmVkZW50aWFsczogXCJpbmNsdWRlXCIsXG5cdFx0Ly8gfSlcblx0XHQvLyBcdC50aGVuKChib2R5KSA9PiB7XG5cdFx0Ly8gXHRcdGNvbnNvbGUubG9nKFwiYm9keVwiLCBib2R5LmhlYWRlcnMuZ2V0KFwic2V0LWNvb2tpZVwiKSk7XG5cdFx0Ly8gXHRcdHJldHVybiBib2R5Lmpzb24oKTtcblx0XHQvLyBcdH0pXG5cdFx0Ly8gXHQudGhlbigocmVzKSA9PiBjb25zb2xlLmxvZyhcInJlc1wiLCByZXMpKTtcblx0XHQvLyBzZXRUaW1lb3V0KCgpID0+IHtcblx0XHQvLyBcdGRvU29tZXRoaW5nKCk7XG5cdFx0Ly8gfSwgMTAwMCk7XG5cdH0sIFtdKTtcblxuXHRjb25zdCBvbkNsaWNrID0gKGV2ZW50KSA9PiB7XG5cdFx0Y29uc29sZS5sb2coXCJvbkNsaWNrXCIsIGV2ZW50KTtcblx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG5cdFx0Ly8gZmV0Y2goXCJodHRwczovL2xvY2FsaG9zdDozMDAwL2FwaS91c2Vyc1wiKVxuXHRcdC8vIFx0LnRoZW4oKGJvZHkpID0+IGJvZHkuanNvbigpKVxuXHRcdC8vIFx0LnRoZW4oKHJlcykgPT4gY29uc29sZS5sb2coXCJyZXNcIiwgcmVzKSk7XG5cblx0XHRmZXRjaChcImh0dHBzOi8vbGRldi5saW1iZXIuaW46OTAwMS9hcGkvdXNlclwiLCB7XG5cdFx0XHQvLyBjcmVkZW50aWFsczogXCJpbmNsdWRlXCIsXG5cdFx0fSlcblx0XHRcdC50aGVuKChib2R5KSA9PiB7XG5cdFx0XHRcdGNvbnNvbGUubG9nKFwiYm9keVwiLCBib2R5LmhlYWRlcnMuZ2V0KFwic2V0LWNvb2tpZVwiKSk7XG5cdFx0XHRcdHJldHVybiBib2R5Lmpzb24oKTtcblx0XHRcdH0pXG5cdFx0XHQudGhlbigocmVzKSA9PiBjb25zb2xlLmxvZyhcInJlc1wiLCByZXMpKTtcblxuXHRcdGFsZXJ0KFwiSGVsbG9cIik7XG5cdH07XG5cblx0Y29uc29sZS5sb2coXCIqKioqUHJvZHVjdHMqKioqXCIpO1xuXG5cdHJldHVybiAoXG5cdFx0PGRpdj5cblx0XHRcdDxhIGhyZWY9XCJodHRwczovL2xvY2FsaG9zdDo5MDAwL2hvbWVcIiBvbkNsaWNrPXtvbkNsaWNrfT5cblx0XHRcdFx0bGlua1xuXHRcdFx0PC9hPlxuXHRcdFx0PGJyIC8+XG5cdFx0XHQ8YSBocmVmPVwiaHR0cHM6Ly9sZGV2LmxpbWJlci5pbjo5MDAxL2FwaS91c2VyXCI+bG5rPC9hPlxuXHRcdFx0PGJyIC8+XG5cdFx0XHR7Lyo8aWZyYW1lIHNyYz1cImh0dHBzOi8vZGV2ZWxvcGVyLm1vemlsbGEub3JnL2VuLVVTL2RvY3MvV2ViL0hUTUwvRWxlbWVudC9pZnJhbWVcIiAvPiovfVxuXHRcdDwvZGl2PlxuXHQpO1xufTtcblxuY29uc3QgZW5oYW5jZSA9IGNvbm5lY3QoXG5cdChzdGF0ZSkgPT4gKHtcblx0XHRsb2FkaW5nOiBnZXRMb2FkaW5nKHN0YXRlKSxcblx0fSksXG5cdHtcblx0XHRkb1NvbWV0aGluZyxcblx0fVxuKTtcblxuZXhwb3J0IGRlZmF1bHQgZW5oYW5jZShQcm9kdWN0cyk7XG4iLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAcmVkdXhqcy90b29sa2l0XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImJvZHktcGFyc2VyXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImNvb2tpZS1wYXJzZXJcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZDNcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZXhwcmVzc1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJmc1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJodG1sLXBkZlwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJodHRwc1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJqc2RvbVwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJtZW1jYWNoZWRcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwibW9uZ29vc2VcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3RcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QtZG9tL3NlcnZlclwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC1yZWR1eFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC1yb3V0ZXItZG9tXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlZHV4XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlZHV4LXNhZ2EvZWZmZWN0c1wiKTsiXSwic291cmNlUm9vdCI6IiJ9