import { combineReducers } from "redux";

import { homeReducer } from "../pages/home/ducks";
import { productsReducer } from "../pages/products/ducks";

const reducer = combineReducers({
	// ...exampleReducer
	homeReducer,
	productsReducer,
});

export default reducer;
