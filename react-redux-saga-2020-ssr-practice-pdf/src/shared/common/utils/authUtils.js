export const isAccessGranted = () => {
	// check auth header in backend doing a hack now
	if (typeof localStorage === "undefined") {
		return true;
	}

	if (localStorage.getItem("access_token")) {
		return true;
	}
	return false;
};
