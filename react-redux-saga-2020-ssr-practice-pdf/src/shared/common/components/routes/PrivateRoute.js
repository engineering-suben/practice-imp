import React from "react";
import { Route, Redirect } from "react-router-dom";

import { isAccessGranted } from "../../utils/authUtils";

export const PrivateRoute = ({
	component: Component,
	redirectPath,
	...rest
}) => {
	return (
		<Route
			{...rest}
			render={(props) =>
				isAccessGranted() ? (
					<Component {...props} />
				) : (
					<Redirect to={redirectPath} />
				)
			}
		/>
	);
};
