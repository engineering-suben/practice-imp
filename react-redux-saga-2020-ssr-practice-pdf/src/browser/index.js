import React from "react";
import { hydrate } from "react-dom";
import { /*BrowserRouter,*/ Router } from "react-router-dom";
import { Provider } from "react-redux";

import { loadableReady } from "@loadable/component";

import App from "../shared/app";
import { store, customHistory } from "../shared/configs/config";

import "../shared/index.css";
import "../shared/index.scss";

if ("serviceWorker" in navigator) {
	window.addEventListener("load", function (event) {
		return;
		navigator.serviceWorker
			.register("/sw.js", { scope: "/" })
			.then(function (reg) {
				console.log("Registration succeded. Scope is ", reg.scope);
				console.log("Registration: ", reg);
				if (reg.installing) {
					console.log("Service worker installing: ", reg.installing);
				}
				if (reg.waiting) {
					console.log("Service worker waiting: ", reg.waiting);
				}
				if (reg.active) {
					console.log("Service worker active: ", reg.active);
				}
			})
			.catch(function (error) {
				console.error("Registration failed: ", error);
			});
	});
}

// hydrate(
// 	<Provider store={store}>
// 		<Router history={customHistory}>
// 			{/*<BrowserRouter>*/}
// 			<App />
// 			{/*</BrowserRouter>*/}
// 		</Router>
// 	</Provider>,
// 	document.getElementById("root")
// );

loadableReady(() => {
	const root = document.getElementById("root");
	hydrate(
		<Provider store={store}>
			<Router history={customHistory}>
				{/*<BrowserRouter>*/}
				<App />
				{/*</BrowserRouter>*/}
			</Router>
		</Provider>,
		root
	);
});
