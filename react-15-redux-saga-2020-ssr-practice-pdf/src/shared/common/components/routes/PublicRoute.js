import React from "react";
import { Route, Redirect } from "react-router-dom";

import { isAccessGranted } from "../../utils/authUtils";

export const PublicRoute = ({
	component: Component,
	redirectPath,
	...rest
}) => {
	return <Route {...rest} render={(props) => <Component {...props} />} />;
};
