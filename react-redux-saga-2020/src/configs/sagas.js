import { all } from "redux-saga/effects";

import { homeSaga } from "../pages/home/ducks";
import { catalogueSaga } from "../pages/catalogue/ducks";

export default function* () {
	yield all([
		// exampleSaga(),
		homeSaga(),
		catalogueSaga(),
	]);
}
