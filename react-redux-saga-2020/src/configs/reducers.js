import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

import { createReducer } from "@reduxjs/toolkit";

import { homeReducer } from "../pages/home/ducks";
import { catalogueReducer } from "../pages/catalogue/ducks";

const createRootReducer = (customHistory) =>
	combineReducers({
		// ...exampleReducer
		router: connectRouter(customHistory),
		app: combineReducers({ homeReducer, catalogueReducer }),
	});

// export const reducer = combineReducers({
// 	// ...exampleReducer
// 	homeReducer,
// 	catalogueReducer,
// });

// export default reducer;

export default createRootReducer;

// export const rootReducer = createReducer(initialState, {
// 	[LOGOUT]: (state, { payload }) => {
// 		// state = undefined;
// 		return undefined;
// 	},
// 	default: (state, {payload})return state;
// });

// export const rootReducer = (customHistory) => {
// 	console.log("customHistory", customHistory);
// 	// console.log("action", action);
// 	return (state, action) => {
// 		console.log("state", state);
// 		console.log("action", action);
// 		// return {
// 		// 	...state,
// 		// 	router: connectRouter(customHistory),
// 		// 	app: combineReducers({
// 		// 		// ...exampleReducer
// 		// 		homeReducer,
// 		// 		catalogueReducer,
// 		// 	}),
// 		// };
// 		// return combineReducers({
// 		// 	// ...exampleReducer
// 		// 	router: connectRouter(customHistory),
// 		// 	app: combineReducers({ homeReducer, catalogueReducer }),
// 		// });
// 		return combineReducers({
// 			// ...exampleReducer
// 			router: connectRouter(customHistory),
// 			homeReducer,
// 			catalogueReducer,
// 		});
// 	};
// };
