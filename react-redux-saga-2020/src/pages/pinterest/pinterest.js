import React, { useEffect, useState, useRef } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import DatePicker from "../../common/components/datepicker/datepicker";
import Carousel from "../../common/components/carousel/carousel";

import "./pinterest.scss";

const Header = React.lazy(() =>
	import("../../common/components/header/header")
);
// import Header from "../../common/components/header/header";

// const useCustomHook = (val) => {
// 	const [value, setValue] = useState(val);

// 	useEffect(() => {}, [val]);

// 	return [value, setValue];
// };

const Pinterest = ({}) => {
	const d = useRef(null);
	const loader = useRef(null);

	useEffect(() => {}, []);

	const onBtnClick = (event) => {
		console.log(loader);

		// loader.current.addClass()
	};

	const date = new Date();
	// date.setMonth(1);
	// date.setDate(29);
	date.setDate(31);

	return (
		<div>
			<Header />
			Products
			<Link to="/home">Home</Link>
			<div>Arsenal</div>
			<div ref={loader} className="loader"></div>
			<div className="loader-ms">
				<div className="loader-ms-1"></div>
				<div className="loader-ms-2"></div>
			</div>
			<div className="loader-lms">
				<div className="loader-lms-1"></div>
				<div className="loader-lms-c loader-lms-2"></div>
				<div className="loader-lms-c loader-lms-3"></div>
				<div className="loader-lms-c loader-lms-4"></div>
			</div>
			<div className="sloader">
				<div className="sloader-child"></div>
			</div>
			<div className="sloader"></div>
			{/*<div className="container-1">
				<div className="container-1-child"></div>
				<div className="container-1-child"></div>
				<div className="container-1-child"></div>
			</div>*/}
			<div className="container-2">
				{/*<span className="container-2-child">span</span>*/}
				{/*<span className="container-2-child">span</span>*/}
				{/*<button className="container-2-child">button</button>*/}
				{/*<input className="container-2-child" type="text" />*/}
				{/*<select className="container-2-child">
					<option>1</option>
					<option>2</option>
				</select>*/}
				{/*<textarea className="container-2-child" style={{ resize: "none" }}>
					Arsenal
				</textarea>*/}
				{/*<img
					className="container-2-child"
					src="https://www.w3schools.com/w3images/rocks.jpg"
				/>*/}
				{/*<a className="container-2-child" href="">
					anchor tag
				</a>*/}
				{/*<em className="container-2-child">em tag</em>*/}
				{/*<br />*/}
				{/*<i className="container-2-child">i tag</i>*/}
				{/*<label className="container-2-child">label tag</label>*/}
				{/*<strong className="container-2-child">strong tag</strong>*/}
				{/*<sup className="container-2-child">sup tag</sup>*/}
				{/*<span className="container-2-child">A</span>*/}
				{/*<sub className="container-2-child">sub tag</sub>*/}
				{/*<span className="container-2-child">A</span>*/}
				{/*<tt className="container-2-child">tt tag</tt>*/}
				{/*<var className="container-2-child">var tag</var>*/}
				{/*<q className="container-2-child">q tag</q>*/}
				{/*<small className="container-2-child">small tag</small>*/}
				<bdo className="container-2-child" dir="ltr">
					bdo tag
				</bdo>
				{/*<big className="container-2-child">
					big tag - not supported in html5
				</big>*/}
				{/*<acronym className="container-2-child" title="acronym">
					acronym tag - not supported in html5
				</acronym>*/}
				<code className="container-2-child">code tag</code>
				<samp className="container-2-child">samp tag</samp>
				<time className="container-2-child">time tag</time>
			</div>
			<br />
			<div className="container-3">
				<div className="container-3-child"></div>
				<div className="container-3-child"></div>
				<div className="container-3-child"></div>
			</div>
			<button id="btn" onClick={onBtnClick}>
				loader
			</button>
			<DatePicker date={date} />
			<Carousel data={[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]} />
			{/*<div className="loader">
				<div className="loader-child">C</div>
			</div>*/}
			<picture>
				<source
					media="(max-width: 799px)"
					srcset="https://www.w3schools.com/w3images/rocks.jpg"
				/>
				<source
					media="(min-width: 800px)"
					srcset="https://www.w3schools.com/w3images/wedding.jpg"
				/>
				<img
					src="https://www.w3schools.com/w3images/wedding.jpg"
					alt="Chris standing up holding his daughter Elva"
				/>
			</picture>
			<div className="row">
				<div className="column">
					<img
						src="https://www.w3schools.com/w3images/wedding.jpg"
						style={{ width: "100%" }}
					/>
					<img
						src="https://www.w3schools.com/w3images/rocks.jpg"
						style={{ width: "100%" }}
					/>
					<img
						src="https://www.w3schools.com/w3images/falls2.jpg"
						style={{ width: "100%" }}
					/>
					<img
						src="https://www.w3schools.com/w3images/paris.jpg"
						style={{ width: "100%" }}
					/>
					<img
						src="https://www.w3schools.com/w3images/nature.jpg"
						style={{ width: "100%" }}
					/>
					<img
						src="https://www.w3schools.com/w3images/mist.jpg"
						style={{ width: "100%" }}
					/>
					<img
						src="https://www.w3schools.com/w3images/paris.jpg"
						style={{ width: "100%" }}
					/>
				</div>

				<div className="column">
					<img
						src="https://www.w3schools.com/w3images/underwater.jpg"
						style={{ width: "100%" }}
					/>
					<img
						src="https://www.w3schools.com/w3images/ocean.jpg"
						style={{ width: "100%" }}
					/>
					<img
						src="https://www.w3schools.com/w3images/wedding.jpg"
						style={{ width: "100%" }}
					/>
					<img
						src="https://www.w3schools.com/w3images/mountainskies.jpg"
						style={{ width: "100%" }}
					/>
					<img
						src="https://www.w3schools.com/w3images/rocks.jpg"
						style={{ width: "100%" }}
					/>
					<img
						src="https://www.w3schools.com/w3images/underwater.jpg"
						style={{ width: "100%" }}
					/>
				</div>

				<div className="column">
					<img
						src="https://www.w3schools.com/w3images/wedding.jpg"
						style={{ width: "100%" }}
					/>
					<img
						src="https://www.w3schools.com/w3images/rocks.jpg"
						style={{ width: "100%" }}
					/>
					<img
						src="https://www.w3schools.com/w3images/falls2.jpg"
						style={{ width: "100%" }}
					/>
					<img
						src="https://www.w3schools.com/w3images/paris.jpg"
						style={{ width: "100%" }}
					/>
					<img
						src="https://www.w3schools.com/w3images/nature.jpg"
						style={{ width: "100%" }}
					/>
					<img
						src="https://www.w3schools.com/w3images/mist.jpg"
						style={{ width: "100%" }}
					/>
					<img
						src="https://www.w3schools.com/w3images/paris.jpg"
						style={{ width: "100%" }}
					/>
				</div>

				<div className="column">
					<img
						src="https://www.w3schools.com/w3images/underwater.jpg"
						style={{ width: "100%" }}
					/>
					<img
						src="https://www.w3schools.com/w3images/ocean.jpg"
						style={{ width: "100%" }}
					/>
					<img
						src="https://www.w3schools.com/w3images/wedding.jpg"
						style={{ width: "100%" }}
					/>
					<img
						src="https://www.w3schools.com/w3images/mountainskies.jpg"
						style={{ width: "100%" }}
					/>
					<img
						src="https://www.w3schools.com/w3images/rocks.jpg"
						style={{ width: "100%" }}
					/>
					<img
						src="https://www.w3schools.com/w3images/underwater.jpg"
						style={{ width: "100%" }}
					/>
				</div>
			</div>
		</div>
	);
};

export default Pinterest;

// onMouseDown={onPointerDown}
