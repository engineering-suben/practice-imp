import React, {
	useEffect,
	useLayoutEffect,
	useState,
	useRef,
	Suspense,
} from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { cloneDeep } from "lodash";

import { deepClone } from "../../common/utils/utils.js";
import { getLoading, doSomething, logout } from "./ducks";

import Catalogue from "../catalogue/catalogue";

const Header = React.lazy(() =>
	import("../../common/components/header/header")
);
// import Header from "../../common/components/header/header";

const obj = {
	str: "asd",
	num: 9,
	arr: [
		0,
		1,
		2,
		"asd",
		{ a: "a" },
		[3, 4, 5, {}, document.getElementsByTagName("body")[0]],
	],
	obj: { a: "a", b: [1, 2, 3, {}], c: { c: "c", d: [] } },
};
window.obj = obj;

const Home = (props) => {
	const { loading, doSomething, logout } = props;
	// console.log("props", props);
	const catalogueRef = useRef(null);
	const worker = useRef(null);
	const count = useRef(0);

	const [str, setStr] = useState("");

	useEffect(() => {
		const cloned0 = deepClone(obj);
		console.log("cloned0", cloned0);
		window.cloned0 = cloned0;

		const cloned1 = cloneDeep(obj);
		console.log("cloned1", cloned1);
		window.cloned1 = cloned1;

		// console.log("componentDidMount");
		setTimeout(() => {
			// console.log("componentDidMount, setTimeout");
			// doSomething();
			// worker.current = new Worker("worker.js");
			// worker.current.onmessage = function (event) {
			// 	console.log("main", event);
			// 	console.log("main event.data", event.data);
			// };
		}, 1000);
	}, []);

	useLayoutEffect(() => {
		// console.log("effect: loading effect");
		// console.log("props", props);
		return () => {
			// console.log("cleanup effect: loading cleanup");
			// console.log("props", props);
		};
	}, [loading]);

	const onClick = (event) => {
		// console.log("catalogueRef", catalogueRef.current);
		return;
		worker.current.postMessage({ count: Math.random() * 10000000000 });
	};

	const onCancel = (event) => {
		logout(undefined);
		return;
		worker.current.terminate();
	};

	const onInpChange = (event) => {
		console.log("event.value", event.currentTarget.value);
		setStr(event.currentTarget.value);
	};

	// console.log("new props: props", props);
	// console.log("rendering...", count.current);
	count.current++;
	return (
		<div data-str={str}>
			<Header />
			Home
			<Link to="/products">Products</Link>
			<button type="button" onClick={onClick}>
				button
			</button>
			<button type="button" onClick={onCancel}>
				terminate
			</button>
			{/*<iframe src="https://www.youtube.com/embed/6uMhFsKP2AQ" />*/}
			{/*<iframe src="https://localhost:3000" />*/}
			{/*<Catalogue wrappedComponentRef={catalogueRef} test={{ a: "a" }} />*/}
			{/* <Catalogue test={{ a: "a" }} /> */}
			<form>
				<input value={str} onChange={onInpChange} />
			</form>
		</div>
	);
};

Home.propTypes = {
	test: PropTypes.string,
};

const enhance = connect(
	(state) => ({
		loading: getLoading(state),
	}),
	{
		doSomething,
		logout,
	}
);
console.log("56565656565", doSomething(5));
export default enhance(Home);
