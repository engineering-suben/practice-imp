import React, { useEffect, useState, useRef } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

const Header = React.lazy(() =>
	import("../../common/components/header/header")
);
// import Header from "../../common/components/header/header";

// const useCustomHook = (val) => {
// 	const [value, setValue] = useState(val);

// 	useEffect(() => {}, [val]);

// 	return [value, setValue];
// };

const Products = ({}) => {
	const d = useRef(null);

	useEffect(() => {
		// setTimeout(() => {}, 1000);

		d.current.addEventListener("pointerdown", function (event) {
			console.log("event", event);
			console.log("event.touches", event.touches);
		});
	}, []);

	const onPointerDown = (event) => {
		const e = event.nativeEvent;
		console.log("e", e);
	};

	return (
		<div>
			<Header />
			Products
			<Link to="/home">Home</Link>
			<div
				style={{ width: "500px", height: "500px", backgroundColor: "red" }}
				ref={d}
			></div>
		</div>
	);
};

export default Products;

// onMouseDown={onPointerDown}
