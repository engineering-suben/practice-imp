import { createAction, createReducer, createSelector } from "@reduxjs/toolkit";
import { put, all, takeLatest } from "redux-saga/effects";
import { push } from "connected-react-router";

// import { customHistory as history } from "../../configs/config";

const DO_SOMETHING = "[catalogue] do something";
const SOMETHING_DONE = "[catalogue] something done";

const initialState = {
	loading: true,
};

export const stateSelector = (state) => state.catalogueReducer;

export const getLoading = createSelector(stateSelector, (state) => {
	console.log("--state--", state);
	return state.loading;
});

export const catalogueReducer = createReducer(initialState, {
	[SOMETHING_DONE]: (state, { payload }) => {
		console.log("--payload--", payload);
		// console.log("state", state);
		state.loading = payload.loading;
		// return { ...state, loading: payload.loading };
	},
});

export const doSomething = createAction(DO_SOMETHING);

function* initCatalogue({ payload }) {
	console.log("initCatalogue");
	yield put({ type: SOMETHING_DONE, payload: { loading: false } });

	// console.log("**history**", history);
	// history.push("/home");
	// yield put(push("/home"));

	yield put({ type: SOMETHING_DONE, payload: { loading: true } });
}

export function* catalogueSaga() {
	yield all([yield takeLatest(DO_SOMETHING, initCatalogue)]);
}
