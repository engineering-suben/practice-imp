import React, { useEffect, useState, useRef } from "react";
import "./carousel.scss";

const Carousel = ({ data }) => {
	return (
		<div className="caurosel">
			<button className="caurosel-btn caurosel-btn-left">{"<"}</button>
			<div className="caurosel-body">
				{data.map((item, index) => {
					return (
						<div className="caurosel-item">
							<span>{index}</span>
						</div>
					);
				})}
			</div>
			<button className="caurosel-btn caurosel-btn-right">{">"}</button>
		</div>
	);
};

export default Carousel;
