import React, { Suspense } from "react";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import { ConnectedRouter } from "connected-react-router";

import { customHistory } from "../config";
import { PrivateRoute } from "../../common/components/routes/PrivateRoute";
import { PublicRoute } from "../../common/components/routes/PublicRoute";

// import Home from "../../pages/home/home";
// import Products from "../../pages/products/products";
// import Catalogue from "../../pages/catalogue/catalogue";

const Home = React.lazy(() => import("../../pages/home/home"));
const Products = React.lazy(() => import("../../pages/products/products"));
const Catalogue = React.lazy(() => import("../../pages/catalogue/catalogue"));
const Pinterest = React.lazy(() => import("../../pages/pinterest/pinterest"));

const Routes = () => {
	return (
		<Suspense fallback={<div>Loading...</div>}>
			<ConnectedRouter history={customHistory}>
				<Switch>
					<PublicRoute path="/home" component={Home} redirectPath="/home" />
					<Route path="/home" component={Home} redirectPath="/home" />
					<PublicRoute
						path="/products"
						component={Products}
						redirectPath="/products"
					/>
					<PublicRoute
						path="/catalogue"
						component={Catalogue}
						redirectPath="/catalogue"
					/>
					<PublicRoute
						path="/pinterest"
						component={Pinterest}
						redirectPath="/pinterest"
					/>
					<Route component={Home} />
				</Switch>
			</ConnectedRouter>
		</Suspense>
	);
};

export default Routes;
