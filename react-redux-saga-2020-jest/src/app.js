import React from "react";

import Routes from "./configs/routes/routes";

const App = () => <Routes />;

export default App;
