import { createAction, createReducer, createSelector } from "@reduxjs/toolkit";
import { put, all, takeLatest } from "redux-saga/effects";

const DO_SOMETHING = "[home] do something";
const SOMETHING_DONE = "[home] something done";
const LOGOUT = "[home] logout";

const initialState = {
	loading: true,
};

export const stateSelector = (state) => state.app.homeReducer;

export const getLoading = createSelector(stateSelector, (state) => {
	console.log("++state++", state);
	return state.loading;
});

export const homeReducer = createReducer(initialState, {
	[SOMETHING_DONE]: (state, { payload }) => {
		state.loading = !state.loading;
	},
	[LOGOUT]: (state, { payload }) => {
		// state = undefined;
		return undefined;
	},
});

export const doSomething = createAction(DO_SOMETHING);
export const logout = createAction(LOGOUT);

function* initHome({ payload }) {
	yield put({ type: SOMETHING_DONE, payload: { loading: false } });
}

export function* homeSaga() {
	yield all([yield takeLatest(DO_SOMETHING, initHome)]);
}
