import React, {
	useEffect,
	useLayoutEffect,
	useState,
	useRef,
	Suspense,
} from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { cloneDeep } from "lodash";

import { deepClone } from "../../common/utils/utils.js";
import { getLoading, doSomething, logout } from "./ducks";

import Catalogue from "../catalogue/catalogue";

const Header = React.lazy(() =>
	import("../../common/components/header/header")
);

const Home = ({ test }) => {
	useEffect(() => {
		setTimeout(() => {}, 1000);
		console.log(test);
	}, []);

	const a = false;
	const b = (true && false) || (false && false);

	return (
		<div>
			<Header />
			Home
			<Link to="/products">Products</Link>
			<button
				type="button"
				disabled={a}
				style={{ margin: 0, marginLeft: "10px" }}
			>
				Btn
			</button>
		</div>
	);
};

Home.propTypes = {
	test: PropTypes.string,
};

const enhance = connect(
	(state) => ({
		loading: getLoading(state),
	}),
	{
		doSomething,
		logout,
	}
);

export default enhance(Home);
