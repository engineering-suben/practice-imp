import React, { useEffect, useState, useRef } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

const Header = React.lazy(() =>
	import("../../common/components/header/header")
);

const Products = ({}) => {
	const d = useRef(null);

	useEffect(() => {}, []);

	return (
		<div>
			<Header />
			Products
			<Link to="/home">Home</Link>
			<div
				style={{ width: "500px", height: "500px", backgroundColor: "red" }}
				ref={d}
			/>
		</div>
	);
};

export default Products;
