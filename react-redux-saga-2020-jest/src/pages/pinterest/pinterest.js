import React, { useEffect, useState, useRef } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import DatePicker from "../../common/components/datepicker/datepicker";
import Carousel from "../../common/components/carousel/carousel";

import "./pinterest.scss";

const Header = React.lazy(() => import("../../common/components/header/header"));

const Pinterest = ({}) => {
	const d = useRef(null);
	const loader = useRef(null);

	useEffect(() => {}, []);

	const onBtnClick = (event) => {
		console.log(loader);
	};

	const date = new Date();
	date.setDate(31);

	return (
		<div>
			<Header />
			Products
			<Link to="/home">Home</Link>
			<div>Arsenal</div>
		</div>
	);
};

export default Pinterest;
