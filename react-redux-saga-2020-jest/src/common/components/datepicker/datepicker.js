import React, { useEffect, useState, useRef } from "react";
import "./datepicker.scss";

const getDaysInMonth = (year, month) => {
	return new Date(year, month + 1, 0).getDate();
};

const DatePicker = ({ date }) => {
	const [dDate, setDDate] = useState(date);

	const onClickChangeYear = (operation) => {
		const currentDay = dDate.getDate();
		dDate.setDate(1);
		if (operation === -1) {
			dDate.setYear(dDate.getFullYear() - 1);
		} else {
			dDate.setYear(dDate.getFullYear() + 1);
		}

		const daysInMonth = getDaysInMonth(dDate.getFullYear(), dDate.getMonth());
		if (currentDay < daysInMonth) {
			dDate.setDate(currentDay);
		} else {
			dDate.setDate(daysInMonth);
		}

		setDDate(new Date(dDate.toISOString()));
	};

	const onClickChangeMonth = (operation) => {
		const currentDay = dDate.getDate();
		dDate.setDate(1);
		if (operation === -1) {
			dDate.setMonth(dDate.getMonth() - 1);
		} else {
			dDate.setMonth(dDate.getMonth() + 1);
		}

		const daysInMonth = getDaysInMonth(dDate.getFullYear(), dDate.getMonth());
		if (currentDay < daysInMonth) {
			dDate.setDate(currentDay);
		} else {
			dDate.setDate(daysInMonth);
		}

		setDDate(new Date(dDate.toISOString()));
	};

	const onClickDate = (date) => {
		dDate.setDate(date);
		setDDate(new Date(dDate.toISOString()));
	};

	const daysInMonth = getDaysInMonth(dDate.getFullYear(), dDate.getMonth());
	const daysArr = new Array(daysInMonth).fill(0);
	const currentDay = dDate.getDate();

	return (
		<div className="date-picker">
			<div className="date-picker-year">
				<button onClick={() => onClickChangeYear(-1)}>{"<"}</button>
				<div>{dDate.getFullYear()}</div>
				<button onClick={() => onClickChangeYear(1)}>{">"}</button>
			</div>
			<div className="date-picker-month">
				<button onClick={() => onClickChangeMonth(-1)}>{"<"}</button>
				<div>{dDate.toLocaleString("en-US", { month: "long" })}</div>
				<button onClick={() => onClickChangeMonth(1)}>{">"}</button>
			</div>
			<div className="date-picker-month-days">
				{daysArr.map((item, index) => {
					return (
						<div
							key={index}
							className={
								currentDay - 1 === index ? "date-picker-current-day" : null
							}
							onClick={() => onClickDate(index + 1)}
						>
							{index + 1}
						</div>
					);
				})}
			</div>
		</div>
	);
};

export default DatePicker;
