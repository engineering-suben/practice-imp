function Stack(options) {
	this.length = 0;
	this.HEAD = null;

	if (options && Array.isArray(options.data)) {
		this.constructStack(options.data);
	}
}

Stack.prototype.constructor = Stack;

Stack.prototype.constructNode = function (d) {
	return {
		next: null,
		d: d,
	};
};

Stack.prototype.constructStack = function (data) {
	var len = data.length;
	for (var i = len - 1; i >= 0; i--) {
		this.push(data[i]);
	}
};

Stack.prototype.push = function (d) {
	var node = this.constructNode(d);
	if (this.HEAD) {
		node.next = this.HEAD;
		this.HEAD = node;
	} else {
		this.HEAD = node;
	}
	this.length++;
};

Stack.prototype.pop = function (node) {
	if (this.HEAD) {
		var temp = this.HEAD;
		this.HEAD = this.HEAD.next;
		temp.next = null;
		this.length--;
		return temp.d;
	} else {
		return null;
	}
};

Stack.isEmpty = function () {
	if (this.HEAD) {
		return false;
	}
	return true;
};

Stack.prototype.peak = function () {
	return this.HEAD?.d || null;
};

Stack.prototype.getData = function () {
	var iter = this.HEAD;
	var arr = new Array(this.length);
	var count = 0;
	while (iter != null) {
		arr[count++] = iter.d;
		iter = iter.next;
	}

	return arr;
};

Stack.prototype.getSize = function () {
	return this.length;
};

Stack.prototype.empty = function () {
	this.HEAD = null;
};

const resS = new Stack();
const countS = new Stack();
const keyS = new Stack();

const initStacks = (obj) => {
	if (Array.isArray(obj)) {
		const arr = new Array(obj.length);
		resS.push();
		countS.push({ cnt: obj.length, type: "array", len: obj.length });

		for (const item of obj) {
			keyS.push({ key: null, value: item });
		}

		return arr;
	} else if (typeof obj === "object") {
		const no = {};

		const keys = Object.keys(obj);
		resS.push(no);
		countS.push({ cnt: keys.length, type: "object", len: keys.length });

		for (const key of keys) {
			keyS.push({ key, value: obj[key] });
		}

		return no;
	}
};

const _checkType = (obj) => {
	if (Array.isArray(obj.value)) {
		return initStacks(obj.value);
	} else if (typeof obj.value === "object") {
		return initStacks(obj.value);
	}
};

export const deepCloneIter = (obj) => {
	const base = initStacks();

	while (!resS.isEmpty()) {
		const x = keyS.pop();
		const aOrO = _checkType(x);

		const countTop = countS.peak();
		countTop.cnt--;

		const resTop = resS.peak();

		if (!Array.isArray(aOrO) && typeof aOrO !== "object") {
			if (countTop.type === "array") {
				resTop[countTop.cnt] = x.value;
			} else {
				resTop[x.key] = x.value;
			}
		}
	}
};

// ******************************

// ******************************
const checkType = (obj) => {
	if (Array.isArray(obj)) {
		return copyArray(obj);
	} else if (typeof obj === "object") {
		return copyObject(obj);
	} else {
		return obj;
	}
};

const copyArray = (arr) => {
	const len = arr.length;
	const res = new Array(len);
	let iter = 0;

	while (iter < len) {
		res[iter] = checkType(arr[iter]);
		iter++;
	}

	return res;
};

const copyObject = (obj) => {
	const res = {};
	const keys = Object.keys(obj);

	for (const key of keys) {
		res[key] = checkType(obj[key]);
	}

	return res;
};

export const deepClone = (obj) => {
	return checkType(obj);
};
