import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

import { store } from "./configs/config";

import App from "./app";

import "./index.css";
import "./index.scss";

if ("serviceWorker" in navigator) {
	window.addEventListener("load", function (event) {
		// return;
		navigator.serviceWorker
			.register("/sw.js", { scope: "/" })
			.then(function (reg) {
				console.log("Registration succeded. Scope is ", reg.scope);
				console.log("Registration: ", reg);
				if (reg.installing) {
					console.log("Service worker installing: ", reg.installing);
				}
				if (reg.waiting) {
					console.log("Service worker waiting: ", reg.waiting);
				}
				if (reg.active) {
					console.log("Service worker active: ", reg.active);
				}
			})
			.catch(function (error) {
				console.error("Registration failed: ", error);
			});
	});
}

const Root = () => {
	return (
		<Provider store={store}>
			<App />
		</Provider>
	);
	// return <App />;
};

ReactDOM.render(<Root />, document.getElementById("root"));
