const CACHE_NAME_1 = "CACHE_1";
const URLs = [
	"/index.html",
	// "/assets/index.css",
	// "/assets/index.js",
	// "/assets/0.index.js",
	// "/assets/1.index.js",
	// "/assets/2.index.js",
	"/assets/",
];

self.addEventListener("install", function (event) {
	// if (process.env.NODE_MODE === "development") {
	// self.skipWaiting();
	// }

	event.waitUntil(
		caches
			.open(CACHE_NAME_1)
			.then(function (cache) {
				console.log("Opened cache: ", CACHE_NAME_1, " :", cache);
				return cache.addAll(URLs);
			})
			.catch(function (error) {
				console.error("Failed to install sw: ", error);
			})
	);
});

self.addEventListener("fetch", function (event) {
	console.log("Fetch Fired.", event.request);
	event.respondWith(
		caches.match(event.request).then(function (response) {
			if (response) {
				console.log("Cache mached and returning catched response: ", response);
				return response;
			}
			console.log("Not in cache: ", event.request);
			return fetch(event.request).then(function (response) {
				if (!response || response.status !== 200 || response.type !== "basic") {
					console.log("Not putting in cache: ", event.request);
					return response;
				}

				const responseToCache = response.clone();
				caches.open(CACHE_NAME_1).then(function (cache) {
					cache.put(event.request, responseToCache);
				});

				return response;
			});
		})
	);
});

self.addEventListener("activate", function (event) {
	const updatedCacheList = [CACHE_NAME_1];

	console.log("Activated new Service Worker: ", event);

	event.waitUntil(
		caches.keys().then(function (cacheNames) {
			console.log("Existing Caches: ", cacheNames);
			return Promise.all(
				cacheNames.map(function (cacheName) {
					if (updatedCacheList.indexOf(cacheName) === -1) {
						console.log("Deleting Cache: ", cacheName);
						return caches.delete(cacheName);
					}
				})
			);
		})
	);
});
