const fs = require("fs");

function main() {
	Promise.resolve().then(() => console.log("resolve"));

	process.nextTick(() => {
		console.log("nextTick");
	});

	setTimeout(() => {
		console.log("setTimeout");
		process.nextTick(() => {
			console.log("setTimeout: next Tick");
		});

		fs.readFile("file", (err, file) => {
			console.log(file.toString());
			setTimeout(() => {
				console.log("setTimeout File");
			});
			setImmediate(() => {
				console.log("setImmediate File");
			});
		});

		let cnt = 0;
		const i = setInterval(() => {
			console.log("setTimeout: setInterval");
			if (cnt === 3) {
				clearInterval(i);
			}
			cnt++;
			process.nextTick(() => {
				console.log("setTimeout: setInterval: next Tick");
			});
		});
	}, 0);

	let cnt = 0;
	const i = setInterval(() => {
		console.log("setInterval");
		if (cnt === 3) {
			clearInterval(i);
		}
		process.nextTick(() => {
			console.log("setInterval: next Tick");
		});
		cnt++;
	}, 0);

	// setTimeout(() => {
	// 	console.log("setTimeout");
	// 	process.nextTick(() => {
	// 		console.log("setTimeout: next Tick");
	// 	});
	// }, 0);

	setImmediate(() => {
		console.log("setImmediate");
		setTimeout(() => {
			console.log("setImmediate: setIimeout");
		});

		process.nextTick(() => {
			console.log("setImmediate: next Tick");
		});
	}, 0);
}

main();
