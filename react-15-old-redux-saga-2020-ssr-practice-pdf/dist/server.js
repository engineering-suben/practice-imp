module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/server/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/server/controllers/userController.js":
/*!**************************************************!*\
  !*** ./src/server/controllers/userController.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _models_userModel__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../models/userModel */ "./src/server/models/userModel.js");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var body_parser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! body-parser */ "body-parser");
/* harmony import */ var body_parser__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(body_parser__WEBPACK_IMPORTED_MODULE_2__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }




var userRouter = express__WEBPACK_IMPORTED_MODULE_1___default.a.Router(); // userRouter.use(bodyParser.urlencoded({ extended: false }));

userRouter.use(body_parser__WEBPACK_IMPORTED_MODULE_2___default.a.json());
userRouter.post("/login", /*#__PURE__*/_asyncToGenerator(function* () {}));
userRouter.post("/signup", /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator(function* (req, res) {
    var findResult = yield Object(_models_userModel__WEBPACK_IMPORTED_MODULE_0__["find"])({
      email: req.body.email
    });
    console.log("findResult", findResult);

    if (findResult.length) {
      res.send({
        message: "User already exists."
      });
      return;
    }

    var entry = yield Object(_models_userModel__WEBPACK_IMPORTED_MODULE_0__["create"])(_objectSpread(_objectSpread({}, req.body), {}, {
      email: req.body.email,
      password: req.body.password
    }));
    res.send(entry);
  });

  return function (_x, _x2) {
    return _ref2.apply(this, arguments);
  };
}());
userRouter.post("/update", /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator(function* (req, res) {
    var result = yield Object(_models_userModel__WEBPACK_IMPORTED_MODULE_0__["updateByEmail"])(req.body.email, req.body.data);
    res.send(result);
  });

  return function (_x3, _x4) {
    return _ref3.apply(this, arguments);
  };
}());
userRouter.post("/delete", /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator(function* (req, res) {
    var result = yield Object(_models_userModel__WEBPACK_IMPORTED_MODULE_0__["deleteByEmail"])(req.body.email);
    res.send(result);
  });

  return function (_x5, _x6) {
    return _ref4.apply(this, arguments);
  };
}());
/* harmony default export */ __webpack_exports__["default"] = (userRouter);

/***/ }),

/***/ "./src/server/index.js":
/*!*****************************!*\
  !*** ./src/server/index.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var https__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! https */ "https");
/* harmony import */ var https__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(https__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! fs */ "fs");
/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(fs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! mongoose */ "mongoose");
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(mongoose__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var memcached__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! memcached */ "memcached");
/* harmony import */ var memcached__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(memcached__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var cookie_parser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! cookie-parser */ "cookie-parser");
/* harmony import */ var cookie_parser__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(cookie_parser__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _middlewares_handleRender__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./middlewares/handleRender */ "./src/server/middlewares/handleRender.js");
/* harmony import */ var _controllers_userController__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./controllers/userController */ "./src/server/controllers/userController.js");
/* harmony import */ var jsdom__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! jsdom */ "jsdom");
/* harmony import */ var jsdom__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(jsdom__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var html_pdf__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! html-pdf */ "html-pdf");
/* harmony import */ var html_pdf__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(html_pdf__WEBPACK_IMPORTED_MODULE_9__);




 // import bodyParser from "body-parser";





 // mongoose.connect(
// 	"mongodb://localhost:27017/test1",
// 	{
// 		useNewUrlParser: true,
// 		useUnifiedTopology: true,
// 	},
// 	(err) => {
// 		if (err) {
// 			console.error("Mongo DB Connection Error!");
// 			console.error(err);
// 		} else {
// 			console.log("Mongo DB Connected");
// 		}
// 	}
// );
//

var html = "<html><head></head><body></body></html>";
var options = {};
var {
  JSDOM
} = jsdom__WEBPACK_IMPORTED_MODULE_8___default.a;
global.dom = new JSDOM(html, options);
global.window = dom.window;
global.document = dom.window.document; //

var memcached = new memcached__WEBPACK_IMPORTED_MODULE_4___default.a("127.0.0.1:11211");
var app = express__WEBPACK_IMPORTED_MODULE_2___default()();
app.use(express__WEBPACK_IMPORTED_MODULE_2___default.a.static("public"));
app.use(cookie_parser__WEBPACK_IMPORTED_MODULE_5___default()());
var publicRouter = express__WEBPACK_IMPORTED_MODULE_2___default.a.Router();
var apiRouter = express__WEBPACK_IMPORTED_MODULE_2___default.a.Router();
app.use("/", publicRouter);
app.use("/home", publicRouter);
app.use("/api", apiRouter); // publicRouter.use(handleRender());

publicRouter.get("/", Object(_middlewares_handleRender__WEBPACK_IMPORTED_MODULE_6__["handleRender"])(), (req, res) => {
  memcached.set("sth", "asd", 300, function (err, data) {
    console.log("/ err, data", err, data);
  }); // res.cookie("access-token", process.env.PORT, {
  // 	expires: new Date(Date.now() + 12000),
  // });

  console.log("/");
  console.log("req.Referer", req.get("Referrer")); // console.log("req.cookies", req.headers.cookie);

  res.header("Access-Control-Allow-Credentials", true); // res.header("Access-Control-Allow-Origin", "https://ldev.limber.co.in:9000");

  res.cookie("access-token", process.env.PORT, {
    httpOnly: true
  });
  res.cookie("random", "random");
  res.send(res.locals.html);
});
publicRouter.get("/home", Object(_middlewares_handleRender__WEBPACK_IMPORTED_MODULE_6__["handleRender"])(), (req, res) => {
  memcached.get("sth", function (err, data) {
    console.log("/home err, data", err, data);
  });
  console.log("/home");
  console.log("req.Referer", req.get("Referrer"));
  console.log("req.Origin", req.get("Origin"));
  console.log("req.cookies", req.headers.cookie);
  console.log("req.cookies", req.cookies); // res.header(
  // 	"Content-Security-Policy",
  // 	"frame-ancestors 'self'; connect-src 'self';"
  // );
  // res.cookie("access-token", process.env.PORT);

  res.send(res.locals.html);
});
publicRouter.get("/products", Object(_middlewares_handleRender__WEBPACK_IMPORTED_MODULE_6__["handleRender"])(), (req, res) => {
  console.log("/products");
  console.log("req.Referer", req.get("Referrer"));
  console.log("req.cookies", req.headers.cookie); // res.header("X-Frame-Options", "deny");
  // res.header(
  // 	"Content-Security-Policy",
  // 	"frame-ancestors 'self' https://ldev.limber.co.in:9000 https://localhost:9000; connect-src 'self'"
  // );
  // res.header("Content-Security-Policy", "connect-src 'self';");
  // res.cookie("access-token", process.env.PORT);

  res.send(res.locals.html);
});
publicRouter.get("/pdf", Object(_middlewares_handleRender__WEBPACK_IMPORTED_MODULE_6__["handleRender"])(), (req, res) => {
  console.log("/pdf");
  var filename = new Date().getTime();
  var options = {
    format: "Letter"
  };
  html_pdf__WEBPACK_IMPORTED_MODULE_9___default.a.create(res.locals.html, options).toFile("./output/" + filename + ".pdf", function (err, res) {
    if (err) return console.log(err);
    console.log(res);
  });
  res.send(res.locals.html);
});
apiRouter.use((req, res, next) => {
  console.log("apiRouter", req.get("Origin")); // res.header("Access-Control-Allow-Origin", req.get("Origin"));

  next();
});
apiRouter.use("/users", _controllers_userController__WEBPACK_IMPORTED_MODULE_7__["default"]);
apiRouter.options("/user", (req, res) => {
  console.log("****HERE****"); // res.header("Access-Control-Allow-Credentials", true);

  res.header("Access-Control-Allow-Headers", "Content-Type");
  res.send();
});
apiRouter.get("/user", (req, res) => {
  // res.cookie("access-token", process.env.PORT, {
  // 	expires: new Date(Date.now() + 12000),
  // });
  console.log("/user");
  console.log("req.Referer", req.get("Referrer"));
  console.log("req.cookies", req.headers.cookie); // res.set("Cache-Control", "private, max-age=30");
  // res.set("Cache-Control", "private");

  res.header("Access-Control-Allow-Credentials", true);
  res.cookie("access-token", process.env.PORT, {
    expires: new Date(Date.now() + 2 * 60 * 1000),
    sameSite: "None",
    secure: true
  }).cookie("somestring", "somerandomstring", {
    expires: new Date(Date.now() + 2 * 60 * 1000),
    sameSite: "None",
    secure: true
  });
  res.send(["arsenal", "chelsea", "manu", 4]);
  console.log("sent");
});
apiRouter.post("/user", (req, res) => {
  // res.cookie("access-token", process.env.PORT, {
  // 	expires: new Date(Date.now() + 12000),
  // });
  console.log("/user");
  console.log("req.Referer", req.get("Referrer"));
  console.log("req.cookies", req.headers.cookie);
  res.header("Access-Control-Allow-Credentials", true);
  res.cookie("access-token", process.env.PORT);
  res.send(["arsenal", "chelsea"]);
});
app.get("*", (req, res, next) => {
  res.redirect("/");
}); // app.listen(process.env.PORT, () => {
// 	console.log("#############################");
// 	console.log("server running on port: ", process.env.PORT);
// 	console.log("#############################");
// });

https__WEBPACK_IMPORTED_MODULE_0___default.a.createServer({
  key: fs__WEBPACK_IMPORTED_MODULE_1___default.a.readFileSync("localhost-privkey.pem"),
  cert: fs__WEBPACK_IMPORTED_MODULE_1___default.a.readFileSync("localhost-cert.pem")
}, app).listen(process.env.PORT, () => {
  console.log("#############################");
  console.log("server running on port: ", process.env.PORT);
  console.log("#############################");
});

/***/ }),

/***/ "./src/server/middlewares/handleRender.js":
/*!************************************************!*\
  !*** ./src/server/middlewares/handleRender.js ***!
  \************************************************/
/*! exports provided: handleRender, renderFullPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "handleRender", function() { return handleRender; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "renderFullPage", function() { return renderFullPage; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom_server__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom/server */ "react-dom/server");
/* harmony import */ var react_dom_server__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom_server__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _shared_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../shared/app */ "./src/shared/app.js");


 // import { createStore } from "redux";
// import { Provider } from "react-redux";
// import reducers from "../../shared/configs/reducers";

 // import { ChunkExtractor } from "@loadable/server";
// import path from "path";
// import render, { send } from "../processes/render/render";
// const statsFile = path.resolve(
// 	__dirname,
// 	"public/assets/web/loadable-stats.json"
// );
// const statsFile = path.resolve(__dirname, "./web/loadable-stats.json");
// console.log("statsFile", statsFile);

var handleRender = options => {
  return function (req, res, next) {
    // ****
    // const store = createStore(reducers);
    // const webExtractor = new ChunkExtractor({
    // 	statsFile,
    // 	entrypoints: ["index.js"],
    // });
    // const jsx = webExtractor.collectChunks(
    // 	<Provider store={store}>
    // 		<StaticRouter location={req.url}>
    // 			<App />
    // 		</StaticRouter>
    // 	</Provider>
    // );
    // const html = renderToString(jsx);
    // const preloadedState = store.getState();
    // res.locals.html = renderCodeSplit(html, webExtractor, preloadedState);
    // next();
    // ****
    // ****
    // const store = createStore(reducers);
    // const html = renderToString(
    // 	<Provider store={store}>
    // 		<StaticRouter location={req.url}>
    // 			<App />
    // 		</StaticRouter>
    // 	</Provider>
    // );
    var html = Object(react_dom_server__WEBPACK_IMPORTED_MODULE_1__["renderToString"])( /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_2__["StaticRouter"], {
      location: req.url
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_shared_app__WEBPACK_IMPORTED_MODULE_3__["default"], null))); // const preloadedState = store.getState();
    // res.locals.html = renderFullPage(html, preloadedState);

    res.locals.html = renderFullPage(html);
    next(); // ****
    // ****
    // render.send({ store, location: req.url });
    // render.on("message", (m) => {
    // 	const html = m.html;
    // 	const preloadedState = store.getState();
    // 	res.locals.html = renderFullPage(html, preloadedState);
    // 	next();
    // });
    // ****
  };
};
var renderFullPage = (html, preloadedState) => {
  return "<!DOCTYPE html>\n      <head>\n      \t<!--<meta http-equiv=\"X-Frame-Options\" content=\"deny\">--><!-- This doesn't work -->\n        <title>React SSR</title>\n        <link rel=\"stylesheet\" href=\"/assets/index.css\">\n        <script defer src=\"/assets/index.js\"></script>\n      </head>\n      <body>\n        <div id=\"root\">".concat(html, "</div>\n        <script>\n        \twindow.__PRELOADED_STATE__ = ").concat(JSON.stringify(preloadedState), "\n        </script>\n      </body>\n    </html>");
}; // export const renderCodeSplit = (html, webExtractor, preloadedState) => {
// 	console.log("webExtractor.getLinkTags", webExtractor.getLinkTags());
// 	console.log("webExtractor.getScriptTags", webExtractor.getScriptTags());
// 	return `<!DOCTYPE html>
//       <head>
//         <title>React SSR</title>
//         ${webExtractor.getLinkTags()}
//         ${webExtractor.getScriptTags()}
//       </head>
//       <body>
//         <div id="root">${html}</div>
//         <script>
//         	window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState)}
//         </script>
//       </body>
//     </html>`;
// };

/***/ }),

/***/ "./src/server/models/userModel.js":
/*!****************************************!*\
  !*** ./src/server/models/userModel.js ***!
  \****************************************/
/*! exports provided: create, find, updateByEmail, deleteByEmail, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "create", function() { return create; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "find", function() { return find; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateByEmail", function() { return updateByEmail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteByEmail", function() { return deleteByEmail; });
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! mongoose */ "mongoose");
/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mongoose__WEBPACK_IMPORTED_MODULE_0__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }


var UserSchema = mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.Schema({
  _id: {
    type: mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.Schema.Types.ObjectId,
    required: true,
    auto: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  }
});
var User = mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.model("Users", UserSchema, "Users");
var create = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator(function* (obj) {
    var email = obj.email;
    var password = obj.password;
    console.log("User", _objectSpread(_objectSpread({}, obj), {}, {
      email,
      password
    }));
    var user = new User(_objectSpread(_objectSpread({}, obj), {}, {
      email,
      password
    }));
    return yield user.save();
  });

  return function create(_x) {
    return _ref.apply(this, arguments);
  };
}();
var find = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator(function* (obj) {
    var email = obj.email;
    var result = yield User.find({
      email
    }).lean();
    return result;
  });

  return function find(_x2) {
    return _ref2.apply(this, arguments);
  };
}();
var updateByEmail = /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator(function* (email, obj) {
    var result = yield User.update({
      email
    }, obj);
    return result;
  });

  return function updateByEmail(_x3, _x4) {
    return _ref3.apply(this, arguments);
  };
}();
var deleteByEmail = /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator(function* (email) {
    var result = yield User.deleteOne({
      email
    });
    return result;
  });

  return function deleteByEmail(_x5) {
    return _ref4.apply(this, arguments);
  };
}();
/* harmony default export */ __webpack_exports__["default"] = (User);

/***/ }),

/***/ "./src/shared/app.js":
/*!***************************!*\
  !*** ./src/shared/app.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _configs_routes_routes__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./configs/routes/routes */ "./src/shared/configs/routes/routes.js");



var App = () => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_configs_routes_routes__WEBPACK_IMPORTED_MODULE_1__["default"], null);
};

/* harmony default export */ __webpack_exports__["default"] = (App);

/***/ }),

/***/ "./src/shared/common/components/routes/PrivateRoute.js":
/*!*************************************************************!*\
  !*** ./src/shared/common/components/routes/PrivateRoute.js ***!
  \*************************************************************/
/*! exports provided: PrivateRoute */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrivateRoute", function() { return PrivateRoute; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils_authUtils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../utils/authUtils */ "./src/shared/common/utils/authUtils.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }




var PrivateRoute = (_ref) => {
  var {
    component: Component,
    redirectPath
  } = _ref,
      rest = _objectWithoutProperties(_ref, ["component", "redirectPath"]);

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], _extends({}, rest, {
    render: props => Object(_utils_authUtils__WEBPACK_IMPORTED_MODULE_2__["isAccessGranted"])() ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Component, props) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Redirect"], {
      to: redirectPath
    })
  }));
};

/***/ }),

/***/ "./src/shared/common/components/routes/PublicRoute.js":
/*!************************************************************!*\
  !*** ./src/shared/common/components/routes/PublicRoute.js ***!
  \************************************************************/
/*! exports provided: PublicRoute */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PublicRoute", function() { return PublicRoute; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils_authUtils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../utils/authUtils */ "./src/shared/common/utils/authUtils.js");
function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }




var PublicRoute = (_ref) => {
  var {
    component: Component,
    redirectPath
  } = _ref,
      rest = _objectWithoutProperties(_ref, ["component", "redirectPath"]);

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], _extends({}, rest, {
    render: props => /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(Component, props)
  }));
};

/***/ }),

/***/ "./src/shared/common/utils/authUtils.js":
/*!**********************************************!*\
  !*** ./src/shared/common/utils/authUtils.js ***!
  \**********************************************/
/*! exports provided: isAccessGranted */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isAccessGranted", function() { return isAccessGranted; });
var isAccessGranted = () => {
  // check auth header in backend doing a hack now
  if (typeof localStorage === "undefined") {
    return true;
  }

  if (localStorage.getItem("access_token")) {
    return true;
  }

  return false;
};

/***/ }),

/***/ "./src/shared/configs/routes/routes.js":
/*!*********************************************!*\
  !*** ./src/shared/configs/routes/routes.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "react-router-dom");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_router_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _common_components_routes_PrivateRoute__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../common/components/routes/PrivateRoute */ "./src/shared/common/components/routes/PrivateRoute.js");
/* harmony import */ var _common_components_routes_PublicRoute__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../common/components/routes/PublicRoute */ "./src/shared/common/components/routes/PublicRoute.js");
/* harmony import */ var _pages_home_home__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../pages/home/home */ "./src/shared/pages/home/home.js");
/* harmony import */ var _pages_products_products__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../pages/products/products */ "./src/shared/pages/products/products.js");
/* harmony import */ var _pages_pdf_pdf__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../pages/pdf/pdf */ "./src/shared/pages/pdf/pdf.js");








var Routes = props => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Switch"], null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_components_routes_PublicRoute__WEBPACK_IMPORTED_MODULE_3__["PublicRoute"], {
    path: "/pdf",
    component: _pages_pdf_pdf__WEBPACK_IMPORTED_MODULE_6__["default"],
    redirectPath: "/pdf"
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Redirect"], {
    to: "/pdf"
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Routes);

/***/ }),

/***/ "./src/shared/pages/home/ducks.js":
/*!****************************************!*\
  !*** ./src/shared/pages/home/ducks.js ***!
  \****************************************/
/*! exports provided: stateSelector, getLoading, homeReducer, doSomething, homeSaga */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "stateSelector", function() { return stateSelector; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLoading", function() { return getLoading; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "homeReducer", function() { return homeReducer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "doSomething", function() { return doSomething; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "homeSaga", function() { return homeSaga; });
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @reduxjs/toolkit */ "@reduxjs/toolkit");
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__);


var DO_SOMETHING = "[home] do something";
var SOMETHING_DONE = "[home] something done";
var initialState = {
  loading: true
};
var stateSelector = state => state.homeReducer;
var getLoading = Object(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(stateSelector, state => {
  console.log(state);
  return state.loading;
});
var homeReducer = Object(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__["createReducer"])(initialState, {
  [SOMETHING_DONE]: (state, _ref) => {
    var {
      payload
    } = _ref;
    state.loading = false;
  }
});
var doSomething = Object(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__["createAction"])(DO_SOMETHING);

function* initHome(_ref2) {
  var {
    payload
  } = _ref2;
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])({
    type: SOMETHING_DONE,
    payload: {
      loading: false
    }
  });
}

function* homeSaga() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["all"])([yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["takeLatest"])(DO_SOMETHING, initHome)]);
}

/***/ }),

/***/ "./src/shared/pages/home/home.js":
/*!***************************************!*\
  !*** ./src/shared/pages/home/home.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ducks__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ducks */ "./src/shared/pages/home/ducks.js");



var aTag = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
  href: "https://localhost:3000/home"
}, "link");

var Home = (_ref) => {
  var {
    loading,
    doSomething
  } = _ref;
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {// fetch("https://ldev.limber.co.in:9000/api/user", {
    // 	method: "post",
    // 	// withCredentials: true,
    // 	credentials: "include",
    // 	data: {
    // 		asd: "asd",
    // 	},
    // 	headers: {
    // 		"Content-Type": "application/json",
    // 	},
    // })
    // 	.then((body) => {
    // 		console.log("body", body.headers.get("set-cookie"));
    // 		return body.json();
    // 	})
    // 	.then((res) => console.log("res", res));
    // fetch("https://ldev.limber.in:9001/api/user", {
    // 	credentials: "include",
    // })
    // 	.then((body) => {
    // 		console.log("body", body.headers.get("set-cookie"));
    // 		return body.json();
    // 	})
    // 	.then((res) => console.log("res", res));
    // setTimeout(() => {
    // 	doSomething();
    // }, 1000);
  }, []);

  var onClick = event => {
    console.log("onClick", event);
    event.preventDefault(); // fetch("https://localhost:3000/api/users")
    // 	.then((body) => body.json())
    // 	.then((res) => console.log("res", res));

    fetch("https://ldev.limber.co.in:9000/api/user", {
      credentials: "include",
      mode: "cors"
    }).then(body => {
      console.log("body", body.headers.get("set-cookie"));
      return body.json();
    }).then(res => console.log("res", res));
  };

  console.log("****Home****");
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "https://localhost:9000/home",
    onClick: onClick
  }, "link"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "https://ldev.limber.in:9001/api/user"
  }, "lnk"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("iframe", {
    src: "https://ldev.limber.in:9001/products",
    referrerPolicy: "strict-origin-when-cross-origin",
    sandbox: "allow-scripts allow-modals"
  }));
};

var enhance = Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(state => ({
  loading: Object(_ducks__WEBPACK_IMPORTED_MODULE_2__["getLoading"])(state)
}), {
  doSomething: _ducks__WEBPACK_IMPORTED_MODULE_2__["doSomething"]
});
/* harmony default export */ __webpack_exports__["default"] = (enhance(Home));

/***/ }),

/***/ "./src/shared/pages/pdf/pdf.js":
/*!*************************************!*\
  !*** ./src/shared/pages/pdf/pdf.js ***!
  \*************************************/
/*! exports provided: getBarChart, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getBarChart", function() { return getBarChart; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var d3__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! d3 */ "d3");
/* harmony import */ var d3__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(d3__WEBPACK_IMPORTED_MODULE_1__);
 // import { connect } from "react-redux";


var getBarChart = (width, height) => {
  var data = [{
    label: "A",
    value: 10
  }, {
    label: "B",
    value: 5
  }, {
    label: "C",
    value: 10
  }, {
    label: "D",
    value: 20
  }, {
    label: "E",
    value: 10
  }, {
    label: "F",
    value: 5
  }, {
    label: "G",
    value: 20
  }, {
    label: "H",
    value: 7
  }, {
    label: "I",
    value: 3
  }, {
    label: "J",
    value: 10
  }]; // const d3 = global.d3;
  // console.log("d3", global.d3);

  var margin = {
    top: 20,
    right: 20,
    bottom: 30,
    left: 40
  };
  var chartWidth = width - margin.left - margin.right;
  var chartHeight = height - margin.top - margin.bottom;
  var x = d3__WEBPACK_IMPORTED_MODULE_1__["scaleBand"]().rangeRound([0, chartWidth]).paddingInner(0.1);
  var y = d3__WEBPACK_IMPORTED_MODULE_1__["scaleLinear"]().rangeRound([chartHeight, 0]);
  var z = d3__WEBPACK_IMPORTED_MODULE_1__["scaleOrdinal"]().range(["#992600", "#004d00", "#003366"]);
  x.domain(data.map(function (d) {
    return d.label;
  }));
  y.domain([0, d3__WEBPACK_IMPORTED_MODULE_1__["max"](data, function (d) {
    return d.value;
  })]);
  var div = document.createElement("div");
  div.style.width = width + "px";
  div.style.height = height + "px";
  var svg = d3__WEBPACK_IMPORTED_MODULE_1__["select"](div).append("svg").attr("width", width).attr("height", height);
  var g = svg.append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")").attr("width", chartWidth).attr("height", chartHeight);
  g.selectAll(".bar").data(data).enter().append("rect").attr("class", "bar").attr("x", function (d) {
    return x(d.label);
  }).attr("width", x.bandwidth()).attr("y", function (d) {
    return y(d.value);
  }).attr("height", function (d) {
    return chartHeight - y(d.value);
  }).attr("fill", function (d) {
    return z(d.label);
  });
  g.append("g").attr("transform", "translate(0," + chartHeight + ")").call(d3__WEBPACK_IMPORTED_MODULE_1__["axisBottom"](x));
  g.append("g").call(d3__WEBPACK_IMPORTED_MODULE_1__["axisLeft"](y));
  return div.innerHTML;
};

function createMarkup(data) {
  return {
    __html: data
  };
}

class Pdf extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  constructor(props) {
    super(props);
  }

  render() {
    var html = getBarChart(500, 500);
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      dangerouslySetInnerHTML: createMarkup(html)
    });
  }

}

/* harmony default export */ __webpack_exports__["default"] = (Pdf);

/***/ }),

/***/ "./src/shared/pages/products/ducks.js":
/*!********************************************!*\
  !*** ./src/shared/pages/products/ducks.js ***!
  \********************************************/
/*! exports provided: stateSelector, getLoading, productsReducer, doSomething, productsSaga */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "stateSelector", function() { return stateSelector; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLoading", function() { return getLoading; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "productsReducer", function() { return productsReducer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "doSomething", function() { return doSomething; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "productsSaga", function() { return productsSaga; });
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @reduxjs/toolkit */ "@reduxjs/toolkit");
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! redux-saga/effects */ "redux-saga/effects");
/* harmony import */ var redux_saga_effects__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__);


var DO_SOMETHING = "[products] do something";
var SOMETHING_DONE = "[products] something done";
var initialState = {
  loading: true
};
var stateSelector = state => state.productsReducer;
var getLoading = Object(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__["createSelector"])(stateSelector, state => {
  console.log(state);
  return state.loading;
});
var productsReducer = Object(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__["createReducer"])(initialState, {
  [SOMETHING_DONE]: (state, _ref) => {
    var {
      payload
    } = _ref;
    state.loading = false;
  }
});
var doSomething = Object(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__["createAction"])(DO_SOMETHING);

function* initProducts(_ref2) {
  var {
    payload
  } = _ref2;
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["put"])({
    type: SOMETHING_DONE,
    payload: {
      loading: false
    }
  });
}

function* productsSaga() {
  yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["all"])([yield Object(redux_saga_effects__WEBPACK_IMPORTED_MODULE_1__["takeLatest"])(DO_SOMETHING, initProducts)]);
}

/***/ }),

/***/ "./src/shared/pages/products/products.js":
/*!***********************************************!*\
  !*** ./src/shared/pages/products/products.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ducks__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ducks */ "./src/shared/pages/products/ducks.js");



var aTag = /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
  href: "https://localhost:3000/home"
}, "link");

var Products = (_ref) => {
  var {
    loading,
    doSomething
  } = _ref;
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {// fetch("https://ldev.limber.co.in:9001/api/user", {
    // 	method: "post",
    // 	// withCredentials: true,
    // 	credentials: "include",
    // 	data: {
    // 		asd: "asd",
    // 	},
    // 	headers: {
    // 		"Content-Type": "application/json",
    // 	},
    // })
    // 	.then((body) => {
    // 		console.log("body", body.headers.get("set-cookie"));
    // 		return body.json();
    // 	})
    // 	.then((res) => console.log("res", res));
    // fetch("https://ldev.limber.in:9001/api/user", {
    // 	credentials: "include",
    // })
    // 	.then((body) => {
    // 		console.log("body", body.headers.get("set-cookie"));
    // 		return body.json();
    // 	})
    // 	.then((res) => console.log("res", res));
    // setTimeout(() => {
    // 	doSomething();
    // }, 1000);
  }, []);

  var onClick = event => {
    console.log("onClick", event);
    event.preventDefault(); // fetch("https://localhost:3000/api/users")
    // 	.then((body) => body.json())
    // 	.then((res) => console.log("res", res));

    fetch("https://ldev.limber.in:9001/api/user", {// credentials: "include",
    }).then(body => {
      console.log("body", body.headers.get("set-cookie"));
      return body.json();
    }).then(res => console.log("res", res));
    alert("Hello");
  };

  console.log("****Products****");
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "https://localhost:9000/home",
    onClick: onClick
  }, "link"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "https://ldev.limber.in:9001/api/user"
  }, "lnk"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", null));
};

var enhance = Object(react_redux__WEBPACK_IMPORTED_MODULE_1__["connect"])(state => ({
  loading: Object(_ducks__WEBPACK_IMPORTED_MODULE_2__["getLoading"])(state)
}), {
  doSomething: _ducks__WEBPACK_IMPORTED_MODULE_2__["doSomething"]
});
/* harmony default export */ __webpack_exports__["default"] = (enhance(Products));

/***/ }),

/***/ "@reduxjs/toolkit":
/*!***********************************!*\
  !*** external "@reduxjs/toolkit" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@reduxjs/toolkit");

/***/ }),

/***/ "body-parser":
/*!******************************!*\
  !*** external "body-parser" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("body-parser");

/***/ }),

/***/ "cookie-parser":
/*!********************************!*\
  !*** external "cookie-parser" ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("cookie-parser");

/***/ }),

/***/ "d3":
/*!*********************!*\
  !*** external "d3" ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("d3");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),

/***/ "fs":
/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ }),

/***/ "html-pdf":
/*!***************************!*\
  !*** external "html-pdf" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("html-pdf");

/***/ }),

/***/ "https":
/*!************************!*\
  !*** external "https" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("https");

/***/ }),

/***/ "jsdom":
/*!************************!*\
  !*** external "jsdom" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("jsdom");

/***/ }),

/***/ "memcached":
/*!****************************!*\
  !*** external "memcached" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("memcached");

/***/ }),

/***/ "mongoose":
/*!***************************!*\
  !*** external "mongoose" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("mongoose");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-dom/server":
/*!***********************************!*\
  !*** external "react-dom/server" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-dom/server");

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ "react-router-dom":
/*!***********************************!*\
  !*** external "react-router-dom" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-router-dom");

/***/ }),

/***/ "redux-saga/effects":
/*!*************************************!*\
  !*** external "redux-saga/effects" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("redux-saga/effects");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NlcnZlci9jb250cm9sbGVycy91c2VyQ29udHJvbGxlci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2VydmVyL2luZGV4LmpzIiwid2VicGFjazovLy8uL3NyYy9zZXJ2ZXIvbWlkZGxld2FyZXMvaGFuZGxlUmVuZGVyLmpzIiwid2VicGFjazovLy8uL3NyYy9zZXJ2ZXIvbW9kZWxzL3VzZXJNb2RlbC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2hhcmVkL2FwcC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2hhcmVkL2NvbW1vbi9jb21wb25lbnRzL3JvdXRlcy9Qcml2YXRlUm91dGUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NoYXJlZC9jb21tb24vY29tcG9uZW50cy9yb3V0ZXMvUHVibGljUm91dGUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NoYXJlZC9jb21tb24vdXRpbHMvYXV0aFV0aWxzLmpzIiwid2VicGFjazovLy8uL3NyYy9zaGFyZWQvY29uZmlncy9yb3V0ZXMvcm91dGVzLmpzIiwid2VicGFjazovLy8uL3NyYy9zaGFyZWQvcGFnZXMvaG9tZS9kdWNrcy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2hhcmVkL3BhZ2VzL2hvbWUvaG9tZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2hhcmVkL3BhZ2VzL3BkZi9wZGYuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NoYXJlZC9wYWdlcy9wcm9kdWN0cy9kdWNrcy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2hhcmVkL3BhZ2VzL3Byb2R1Y3RzL3Byb2R1Y3RzLmpzIiwid2VicGFjazovLy9leHRlcm5hbCBcIkByZWR1eGpzL3Rvb2xraXRcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJib2R5LXBhcnNlclwiIiwid2VicGFjazovLy9leHRlcm5hbCBcImNvb2tpZS1wYXJzZXJcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJkM1wiIiwid2VicGFjazovLy9leHRlcm5hbCBcImV4cHJlc3NcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJmc1wiIiwid2VicGFjazovLy9leHRlcm5hbCBcImh0bWwtcGRmXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiaHR0cHNcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJqc2RvbVwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIm1lbWNhY2hlZFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIm1vbmdvb3NlXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVhY3RcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC1kb20vc2VydmVyXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVhY3QtcmVkdXhcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdC1yb3V0ZXItZG9tXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVkdXgtc2FnYS9lZmZlY3RzXCIiXSwibmFtZXMiOlsidXNlclJvdXRlciIsImV4cHJlc3MiLCJSb3V0ZXIiLCJ1c2UiLCJib2R5UGFyc2VyIiwianNvbiIsInBvc3QiLCJyZXEiLCJyZXMiLCJmaW5kUmVzdWx0IiwiZmluZCIsImVtYWlsIiwiYm9keSIsImNvbnNvbGUiLCJsb2ciLCJsZW5ndGgiLCJzZW5kIiwibWVzc2FnZSIsImVudHJ5IiwiY3JlYXRlIiwicGFzc3dvcmQiLCJyZXN1bHQiLCJ1cGRhdGVCeUVtYWlsIiwiZGF0YSIsImRlbGV0ZUJ5RW1haWwiLCJodG1sIiwib3B0aW9ucyIsIkpTRE9NIiwianNkb20iLCJnbG9iYWwiLCJkb20iLCJ3aW5kb3ciLCJkb2N1bWVudCIsIm1lbWNhY2hlZCIsIk1lbWNhY2hlZCIsImFwcCIsInN0YXRpYyIsImNvb2tpZVBhcnNlciIsInB1YmxpY1JvdXRlciIsImFwaVJvdXRlciIsImdldCIsImhhbmRsZVJlbmRlciIsInNldCIsImVyciIsImhlYWRlciIsImNvb2tpZSIsInByb2Nlc3MiLCJlbnYiLCJQT1JUIiwiaHR0cE9ubHkiLCJsb2NhbHMiLCJoZWFkZXJzIiwiY29va2llcyIsImZpbGVuYW1lIiwiRGF0ZSIsImdldFRpbWUiLCJmb3JtYXQiLCJwZGYiLCJ0b0ZpbGUiLCJuZXh0IiwiZXhwaXJlcyIsIm5vdyIsInNhbWVTaXRlIiwic2VjdXJlIiwicmVkaXJlY3QiLCJodHRwcyIsImNyZWF0ZVNlcnZlciIsImtleSIsImZzIiwicmVhZEZpbGVTeW5jIiwiY2VydCIsImxpc3RlbiIsInJlbmRlclRvU3RyaW5nIiwidXJsIiwicmVuZGVyRnVsbFBhZ2UiLCJwcmVsb2FkZWRTdGF0ZSIsIkpTT04iLCJzdHJpbmdpZnkiLCJVc2VyU2NoZW1hIiwibW9uZ29vc2UiLCJTY2hlbWEiLCJfaWQiLCJ0eXBlIiwiVHlwZXMiLCJPYmplY3RJZCIsInJlcXVpcmVkIiwiYXV0byIsIlN0cmluZyIsInVuaXF1ZSIsIlVzZXIiLCJtb2RlbCIsIm9iaiIsInVzZXIiLCJzYXZlIiwibGVhbiIsInVwZGF0ZSIsImRlbGV0ZU9uZSIsIkFwcCIsIlByaXZhdGVSb3V0ZSIsImNvbXBvbmVudCIsIkNvbXBvbmVudCIsInJlZGlyZWN0UGF0aCIsInJlc3QiLCJwcm9wcyIsImlzQWNjZXNzR3JhbnRlZCIsIlB1YmxpY1JvdXRlIiwibG9jYWxTdG9yYWdlIiwiZ2V0SXRlbSIsIlJvdXRlcyIsIlBkZiIsIkRPX1NPTUVUSElORyIsIlNPTUVUSElOR19ET05FIiwiaW5pdGlhbFN0YXRlIiwibG9hZGluZyIsInN0YXRlU2VsZWN0b3IiLCJzdGF0ZSIsImhvbWVSZWR1Y2VyIiwiZ2V0TG9hZGluZyIsImNyZWF0ZVNlbGVjdG9yIiwiY3JlYXRlUmVkdWNlciIsInBheWxvYWQiLCJkb1NvbWV0aGluZyIsImNyZWF0ZUFjdGlvbiIsImluaXRIb21lIiwicHV0IiwiaG9tZVNhZ2EiLCJhbGwiLCJ0YWtlTGF0ZXN0IiwiYVRhZyIsIkhvbWUiLCJ1c2VFZmZlY3QiLCJvbkNsaWNrIiwiZXZlbnQiLCJwcmV2ZW50RGVmYXVsdCIsImZldGNoIiwiY3JlZGVudGlhbHMiLCJtb2RlIiwidGhlbiIsImVuaGFuY2UiLCJjb25uZWN0IiwiZ2V0QmFyQ2hhcnQiLCJ3aWR0aCIsImhlaWdodCIsImxhYmVsIiwidmFsdWUiLCJtYXJnaW4iLCJ0b3AiLCJyaWdodCIsImJvdHRvbSIsImxlZnQiLCJjaGFydFdpZHRoIiwiY2hhcnRIZWlnaHQiLCJ4IiwiZDMiLCJyYW5nZVJvdW5kIiwicGFkZGluZ0lubmVyIiwieSIsInoiLCJyYW5nZSIsImRvbWFpbiIsIm1hcCIsImQiLCJkaXYiLCJjcmVhdGVFbGVtZW50Iiwic3R5bGUiLCJzdmciLCJhcHBlbmQiLCJhdHRyIiwiZyIsInNlbGVjdEFsbCIsImVudGVyIiwiYmFuZHdpZHRoIiwiY2FsbCIsImlubmVySFRNTCIsImNyZWF0ZU1hcmt1cCIsIl9faHRtbCIsIlJlYWN0IiwiY29uc3RydWN0b3IiLCJyZW5kZXIiLCJwcm9kdWN0c1JlZHVjZXIiLCJpbml0UHJvZHVjdHMiLCJwcm9kdWN0c1NhZ2EiLCJQcm9kdWN0cyIsImFsZXJ0Il0sIm1hcHBpbmdzIjoiOztRQUFBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNsRkE7QUFNQTtBQUNBO0FBRUEsSUFBTUEsVUFBVSxHQUFHQyw4Q0FBTyxDQUFDQyxNQUFSLEVBQW5CLEMsQ0FDQTs7QUFDQUYsVUFBVSxDQUFDRyxHQUFYLENBQWVDLGtEQUFVLENBQUNDLElBQVgsRUFBZjtBQUVBTCxVQUFVLENBQUNNLElBQVgsQ0FBZ0IsUUFBaEIsaUNBQTBCLGFBQVksQ0FBRSxDQUF4QztBQUVBTixVQUFVLENBQUNNLElBQVgsQ0FBZ0IsU0FBaEI7QUFBQSxnQ0FBMkIsV0FBT0MsR0FBUCxFQUFZQyxHQUFaLEVBQW9CO0FBQzlDLFFBQU1DLFVBQVUsU0FBU0MsOERBQUksQ0FBQztBQUFFQyxXQUFLLEVBQUVKLEdBQUcsQ0FBQ0ssSUFBSixDQUFTRDtBQUFsQixLQUFELENBQTdCO0FBQ0FFLFdBQU8sQ0FBQ0MsR0FBUixDQUFZLFlBQVosRUFBMEJMLFVBQTFCOztBQUNBLFFBQUlBLFVBQVUsQ0FBQ00sTUFBZixFQUF1QjtBQUN0QlAsU0FBRyxDQUFDUSxJQUFKLENBQVM7QUFBRUMsZUFBTyxFQUFFO0FBQVgsT0FBVDtBQUNBO0FBQ0E7O0FBQ0QsUUFBTUMsS0FBSyxTQUFTQyxnRUFBTSxpQ0FDdEJaLEdBQUcsQ0FBQ0ssSUFEa0I7QUFFekJELFdBQUssRUFBRUosR0FBRyxDQUFDSyxJQUFKLENBQVNELEtBRlM7QUFHekJTLGNBQVEsRUFBRWIsR0FBRyxDQUFDSyxJQUFKLENBQVNRO0FBSE0sT0FBMUI7QUFLQVosT0FBRyxDQUFDUSxJQUFKLENBQVNFLEtBQVQ7QUFDQSxHQWJEOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZUFsQixVQUFVLENBQUNNLElBQVgsQ0FBZ0IsU0FBaEI7QUFBQSxnQ0FBMkIsV0FBT0MsR0FBUCxFQUFZQyxHQUFaLEVBQW9CO0FBQzlDLFFBQU1hLE1BQU0sU0FBU0MsdUVBQWEsQ0FBQ2YsR0FBRyxDQUFDSyxJQUFKLENBQVNELEtBQVYsRUFBaUJKLEdBQUcsQ0FBQ0ssSUFBSixDQUFTVyxJQUExQixDQUFsQztBQUNBZixPQUFHLENBQUNRLElBQUosQ0FBU0ssTUFBVDtBQUNBLEdBSEQ7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFLQXJCLFVBQVUsQ0FBQ00sSUFBWCxDQUFnQixTQUFoQjtBQUFBLGdDQUEyQixXQUFPQyxHQUFQLEVBQVlDLEdBQVosRUFBb0I7QUFDOUMsUUFBTWEsTUFBTSxTQUFTRyx1RUFBYSxDQUFDakIsR0FBRyxDQUFDSyxJQUFKLENBQVNELEtBQVYsQ0FBbEM7QUFDQUgsT0FBRyxDQUFDUSxJQUFKLENBQVNLLE1BQVQ7QUFDQSxHQUhEOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBS2VyQix5RUFBZixFOzs7Ozs7Ozs7Ozs7QUN4Q0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtDQUVBOztBQUNBO0FBQ0E7QUFFQTtBQUVBO0NBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7O0FBQ0EsSUFBTXlCLElBQUksR0FBRyx5Q0FBYjtBQUVBLElBQU1DLE9BQU8sR0FBRyxFQUFoQjtBQUVBLElBQU07QUFBRUM7QUFBRixJQUFZQyw0Q0FBbEI7QUFDQUMsTUFBTSxDQUFDQyxHQUFQLEdBQWEsSUFBSUgsS0FBSixDQUFVRixJQUFWLEVBQWdCQyxPQUFoQixDQUFiO0FBQ0FHLE1BQU0sQ0FBQ0UsTUFBUCxHQUFnQkQsR0FBRyxDQUFDQyxNQUFwQjtBQUNBRixNQUFNLENBQUNHLFFBQVAsR0FBa0JGLEdBQUcsQ0FBQ0MsTUFBSixDQUFXQyxRQUE3QixDLENBQ0E7O0FBRUEsSUFBTUMsU0FBUyxHQUFHLElBQUlDLGdEQUFKLENBQWMsaUJBQWQsQ0FBbEI7QUFFQSxJQUFNQyxHQUFHLEdBQUdsQyw4Q0FBTyxFQUFuQjtBQUNBa0MsR0FBRyxDQUFDaEMsR0FBSixDQUFRRiw4Q0FBTyxDQUFDbUMsTUFBUixDQUFlLFFBQWYsQ0FBUjtBQUNBRCxHQUFHLENBQUNoQyxHQUFKLENBQVFrQyxvREFBWSxFQUFwQjtBQUVBLElBQU1DLFlBQVksR0FBR3JDLDhDQUFPLENBQUNDLE1BQVIsRUFBckI7QUFDQSxJQUFNcUMsU0FBUyxHQUFHdEMsOENBQU8sQ0FBQ0MsTUFBUixFQUFsQjtBQUVBaUMsR0FBRyxDQUFDaEMsR0FBSixDQUFRLEdBQVIsRUFBYW1DLFlBQWI7QUFDQUgsR0FBRyxDQUFDaEMsR0FBSixDQUFRLE9BQVIsRUFBaUJtQyxZQUFqQjtBQUNBSCxHQUFHLENBQUNoQyxHQUFKLENBQVEsTUFBUixFQUFnQm9DLFNBQWhCLEUsQ0FFQTs7QUFFQUQsWUFBWSxDQUFDRSxHQUFiLENBQWlCLEdBQWpCLEVBQXNCQyw4RUFBWSxFQUFsQyxFQUFzQyxDQUFDbEMsR0FBRCxFQUFNQyxHQUFOLEtBQWM7QUFDbkR5QixXQUFTLENBQUNTLEdBQVYsQ0FBYyxLQUFkLEVBQXFCLEtBQXJCLEVBQTRCLEdBQTVCLEVBQWlDLFVBQVVDLEdBQVYsRUFBZXBCLElBQWYsRUFBcUI7QUFDckRWLFdBQU8sQ0FBQ0MsR0FBUixDQUFZLGFBQVosRUFBMkI2QixHQUEzQixFQUFnQ3BCLElBQWhDO0FBQ0EsR0FGRCxFQURtRCxDQUtuRDtBQUNBO0FBQ0E7O0FBQ0FWLFNBQU8sQ0FBQ0MsR0FBUixDQUFZLEdBQVo7QUFDQUQsU0FBTyxDQUFDQyxHQUFSLENBQVksYUFBWixFQUEyQlAsR0FBRyxDQUFDaUMsR0FBSixDQUFRLFVBQVIsQ0FBM0IsRUFUbUQsQ0FVbkQ7O0FBQ0FoQyxLQUFHLENBQUNvQyxNQUFKLENBQVcsa0NBQVgsRUFBK0MsSUFBL0MsRUFYbUQsQ0FZbkQ7O0FBQ0FwQyxLQUFHLENBQUNxQyxNQUFKLENBQVcsY0FBWCxFQUEyQkMsT0FBTyxDQUFDQyxHQUFSLENBQVlDLElBQXZDLEVBQTZDO0FBQUVDLFlBQVEsRUFBRTtBQUFaLEdBQTdDO0FBQ0F6QyxLQUFHLENBQUNxQyxNQUFKLENBQVcsUUFBWCxFQUFxQixRQUFyQjtBQUNBckMsS0FBRyxDQUFDUSxJQUFKLENBQVNSLEdBQUcsQ0FBQzBDLE1BQUosQ0FBV3pCLElBQXBCO0FBQ0EsQ0FoQkQ7QUFrQkFhLFlBQVksQ0FBQ0UsR0FBYixDQUFpQixPQUFqQixFQUEwQkMsOEVBQVksRUFBdEMsRUFBMEMsQ0FBQ2xDLEdBQUQsRUFBTUMsR0FBTixLQUFjO0FBQ3ZEeUIsV0FBUyxDQUFDTyxHQUFWLENBQWMsS0FBZCxFQUFxQixVQUFVRyxHQUFWLEVBQWVwQixJQUFmLEVBQXFCO0FBQ3pDVixXQUFPLENBQUNDLEdBQVIsQ0FBWSxpQkFBWixFQUErQjZCLEdBQS9CLEVBQW9DcEIsSUFBcEM7QUFDQSxHQUZEO0FBSUFWLFNBQU8sQ0FBQ0MsR0FBUixDQUFZLE9BQVo7QUFDQUQsU0FBTyxDQUFDQyxHQUFSLENBQVksYUFBWixFQUEyQlAsR0FBRyxDQUFDaUMsR0FBSixDQUFRLFVBQVIsQ0FBM0I7QUFDQTNCLFNBQU8sQ0FBQ0MsR0FBUixDQUFZLFlBQVosRUFBMEJQLEdBQUcsQ0FBQ2lDLEdBQUosQ0FBUSxRQUFSLENBQTFCO0FBQ0EzQixTQUFPLENBQUNDLEdBQVIsQ0FBWSxhQUFaLEVBQTJCUCxHQUFHLENBQUM0QyxPQUFKLENBQVlOLE1BQXZDO0FBQ0FoQyxTQUFPLENBQUNDLEdBQVIsQ0FBWSxhQUFaLEVBQTJCUCxHQUFHLENBQUM2QyxPQUEvQixFQVR1RCxDQVV2RDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBNUMsS0FBRyxDQUFDUSxJQUFKLENBQVNSLEdBQUcsQ0FBQzBDLE1BQUosQ0FBV3pCLElBQXBCO0FBQ0EsQ0FoQkQ7QUFrQkFhLFlBQVksQ0FBQ0UsR0FBYixDQUFpQixXQUFqQixFQUE4QkMsOEVBQVksRUFBMUMsRUFBOEMsQ0FBQ2xDLEdBQUQsRUFBTUMsR0FBTixLQUFjO0FBQzNESyxTQUFPLENBQUNDLEdBQVIsQ0FBWSxXQUFaO0FBQ0FELFNBQU8sQ0FBQ0MsR0FBUixDQUFZLGFBQVosRUFBMkJQLEdBQUcsQ0FBQ2lDLEdBQUosQ0FBUSxVQUFSLENBQTNCO0FBQ0EzQixTQUFPLENBQUNDLEdBQVIsQ0FBWSxhQUFaLEVBQTJCUCxHQUFHLENBQUM0QyxPQUFKLENBQVlOLE1BQXZDLEVBSDJELENBSTNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBckMsS0FBRyxDQUFDUSxJQUFKLENBQVNSLEdBQUcsQ0FBQzBDLE1BQUosQ0FBV3pCLElBQXBCO0FBQ0EsQ0FaRDtBQWNBYSxZQUFZLENBQUNFLEdBQWIsQ0FBaUIsTUFBakIsRUFBeUJDLDhFQUFZLEVBQXJDLEVBQXlDLENBQUNsQyxHQUFELEVBQU1DLEdBQU4sS0FBYztBQUN0REssU0FBTyxDQUFDQyxHQUFSLENBQVksTUFBWjtBQUVBLE1BQU11QyxRQUFRLEdBQUcsSUFBSUMsSUFBSixHQUFXQyxPQUFYLEVBQWpCO0FBQ0EsTUFBTTdCLE9BQU8sR0FBRztBQUFFOEIsVUFBTSxFQUFFO0FBQVYsR0FBaEI7QUFDQUMsaURBQUcsQ0FDRHRDLE1BREYsQ0FDU1gsR0FBRyxDQUFDMEMsTUFBSixDQUFXekIsSUFEcEIsRUFDMEJDLE9BRDFCLEVBRUVnQyxNQUZGLENBRVMsY0FBY0wsUUFBZCxHQUF5QixNQUZsQyxFQUUwQyxVQUFVVixHQUFWLEVBQWVuQyxHQUFmLEVBQW9CO0FBQzVELFFBQUltQyxHQUFKLEVBQVMsT0FBTzlCLE9BQU8sQ0FBQ0MsR0FBUixDQUFZNkIsR0FBWixDQUFQO0FBQ1Q5QixXQUFPLENBQUNDLEdBQVIsQ0FBWU4sR0FBWjtBQUNBLEdBTEY7QUFPQUEsS0FBRyxDQUFDUSxJQUFKLENBQVNSLEdBQUcsQ0FBQzBDLE1BQUosQ0FBV3pCLElBQXBCO0FBQ0EsQ0FiRDtBQWVBYyxTQUFTLENBQUNwQyxHQUFWLENBQWMsQ0FBQ0ksR0FBRCxFQUFNQyxHQUFOLEVBQVdtRCxJQUFYLEtBQW9CO0FBQ2pDOUMsU0FBTyxDQUFDQyxHQUFSLENBQVksV0FBWixFQUF5QlAsR0FBRyxDQUFDaUMsR0FBSixDQUFRLFFBQVIsQ0FBekIsRUFEaUMsQ0FFakM7O0FBQ0FtQixNQUFJO0FBQ0osQ0FKRDtBQU1BcEIsU0FBUyxDQUFDcEMsR0FBVixDQUFjLFFBQWQsRUFBd0JILG1FQUF4QjtBQUVBdUMsU0FBUyxDQUFDYixPQUFWLENBQWtCLE9BQWxCLEVBQTJCLENBQUNuQixHQUFELEVBQU1DLEdBQU4sS0FBYztBQUN4Q0ssU0FBTyxDQUFDQyxHQUFSLENBQVksY0FBWixFQUR3QyxDQUV4Qzs7QUFDQU4sS0FBRyxDQUFDb0MsTUFBSixDQUFXLDhCQUFYLEVBQTJDLGNBQTNDO0FBQ0FwQyxLQUFHLENBQUNRLElBQUo7QUFDQSxDQUxEO0FBT0F1QixTQUFTLENBQUNDLEdBQVYsQ0FBYyxPQUFkLEVBQXVCLENBQUNqQyxHQUFELEVBQU1DLEdBQU4sS0FBYztBQUNwQztBQUNBO0FBQ0E7QUFDQUssU0FBTyxDQUFDQyxHQUFSLENBQVksT0FBWjtBQUNBRCxTQUFPLENBQUNDLEdBQVIsQ0FBWSxhQUFaLEVBQTJCUCxHQUFHLENBQUNpQyxHQUFKLENBQVEsVUFBUixDQUEzQjtBQUNBM0IsU0FBTyxDQUFDQyxHQUFSLENBQVksYUFBWixFQUEyQlAsR0FBRyxDQUFDNEMsT0FBSixDQUFZTixNQUF2QyxFQU5vQyxDQU9wQztBQUNBOztBQUNBckMsS0FBRyxDQUFDb0MsTUFBSixDQUFXLGtDQUFYLEVBQStDLElBQS9DO0FBQ0FwQyxLQUFHLENBQ0RxQyxNQURGLENBQ1MsY0FEVCxFQUN5QkMsT0FBTyxDQUFDQyxHQUFSLENBQVlDLElBRHJDLEVBQzJDO0FBQ3pDWSxXQUFPLEVBQUUsSUFBSU4sSUFBSixDQUFTQSxJQUFJLENBQUNPLEdBQUwsS0FBYSxJQUFJLEVBQUosR0FBUyxJQUEvQixDQURnQztBQUV6Q0MsWUFBUSxFQUFFLE1BRitCO0FBR3pDQyxVQUFNLEVBQUU7QUFIaUMsR0FEM0MsRUFNRWxCLE1BTkYsQ0FNUyxZQU5ULEVBTXVCLGtCQU52QixFQU0yQztBQUN6Q2UsV0FBTyxFQUFFLElBQUlOLElBQUosQ0FBU0EsSUFBSSxDQUFDTyxHQUFMLEtBQWEsSUFBSSxFQUFKLEdBQVMsSUFBL0IsQ0FEZ0M7QUFFekNDLFlBQVEsRUFBRSxNQUYrQjtBQUd6Q0MsVUFBTSxFQUFFO0FBSGlDLEdBTjNDO0FBV0F2RCxLQUFHLENBQUNRLElBQUosQ0FBUyxDQUFDLFNBQUQsRUFBWSxTQUFaLEVBQXVCLE1BQXZCLEVBQStCLENBQS9CLENBQVQ7QUFDQUgsU0FBTyxDQUFDQyxHQUFSLENBQVksTUFBWjtBQUNBLENBdkJEO0FBeUJBeUIsU0FBUyxDQUFDakMsSUFBVixDQUFlLE9BQWYsRUFBd0IsQ0FBQ0MsR0FBRCxFQUFNQyxHQUFOLEtBQWM7QUFDckM7QUFDQTtBQUNBO0FBQ0FLLFNBQU8sQ0FBQ0MsR0FBUixDQUFZLE9BQVo7QUFDQUQsU0FBTyxDQUFDQyxHQUFSLENBQVksYUFBWixFQUEyQlAsR0FBRyxDQUFDaUMsR0FBSixDQUFRLFVBQVIsQ0FBM0I7QUFDQTNCLFNBQU8sQ0FBQ0MsR0FBUixDQUFZLGFBQVosRUFBMkJQLEdBQUcsQ0FBQzRDLE9BQUosQ0FBWU4sTUFBdkM7QUFDQXJDLEtBQUcsQ0FBQ29DLE1BQUosQ0FBVyxrQ0FBWCxFQUErQyxJQUEvQztBQUNBcEMsS0FBRyxDQUFDcUMsTUFBSixDQUFXLGNBQVgsRUFBMkJDLE9BQU8sQ0FBQ0MsR0FBUixDQUFZQyxJQUF2QztBQUNBeEMsS0FBRyxDQUFDUSxJQUFKLENBQVMsQ0FBQyxTQUFELEVBQVksU0FBWixDQUFUO0FBQ0EsQ0FWRDtBQVlBbUIsR0FBRyxDQUFDSyxHQUFKLENBQVEsR0FBUixFQUFhLENBQUNqQyxHQUFELEVBQU1DLEdBQU4sRUFBV21ELElBQVgsS0FBb0I7QUFDaENuRCxLQUFHLENBQUN3RCxRQUFKLENBQWEsR0FBYjtBQUNBLENBRkQsRSxDQUlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUFDLDRDQUFLLENBQ0hDLFlBREYsQ0FFRTtBQUNDQyxLQUFHLEVBQUVDLHlDQUFFLENBQUNDLFlBQUgsQ0FBZ0IsdUJBQWhCLENBRE47QUFFQ0MsTUFBSSxFQUFFRix5Q0FBRSxDQUFDQyxZQUFILENBQWdCLG9CQUFoQjtBQUZQLENBRkYsRUFNRWxDLEdBTkYsRUFRRW9DLE1BUkYsQ0FRU3pCLE9BQU8sQ0FBQ0MsR0FBUixDQUFZQyxJQVJyQixFQVEyQixNQUFNO0FBQy9CbkMsU0FBTyxDQUFDQyxHQUFSLENBQVksK0JBQVo7QUFDQUQsU0FBTyxDQUFDQyxHQUFSLENBQVksMEJBQVosRUFBd0NnQyxPQUFPLENBQUNDLEdBQVIsQ0FBWUMsSUFBcEQ7QUFDQW5DLFNBQU8sQ0FBQ0MsR0FBUixDQUFZLCtCQUFaO0FBQ0EsQ0FaRixFOzs7Ozs7Ozs7Ozs7QUN2TEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0NBRUE7QUFDQTtBQUVBOztDQUdBO0FBQ0E7QUFFQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTs7QUFFTyxJQUFNMkIsWUFBWSxHQUFJZixPQUFELElBQWE7QUFDeEMsU0FBTyxVQUFVbkIsR0FBVixFQUFlQyxHQUFmLEVBQW9CbUQsSUFBcEIsRUFBMEI7QUFDaEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBTWxDLElBQUksR0FBRytDLHVFQUFjLGVBQzFCLDJEQUFDLDZEQUFEO0FBQWMsY0FBUSxFQUFFakUsR0FBRyxDQUFDa0U7QUFBNUIsb0JBQ0MsMkRBQUMsbURBQUQsT0FERCxDQUQwQixDQUEzQixDQTVCZ0MsQ0FpQ2hDO0FBQ0E7O0FBQ0FqRSxPQUFHLENBQUMwQyxNQUFKLENBQVd6QixJQUFYLEdBQWtCaUQsY0FBYyxDQUFDakQsSUFBRCxDQUFoQztBQUNBa0MsUUFBSSxHQXBDNEIsQ0FxQ2hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0EvQ0Q7QUFnREEsQ0FqRE07QUFtREEsSUFBTWUsY0FBYyxHQUFHLENBQUNqRCxJQUFELEVBQU9rRCxjQUFQLEtBQTBCO0FBQ3ZELG1XQVF3QmxELElBUnhCLDhFQVV1Q21ELElBQUksQ0FBQ0MsU0FBTCxDQUFlRixjQUFmLENBVnZDO0FBY0EsQ0FmTSxDLENBaUJQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMxR0E7QUFFQSxJQUFNRyxVQUFVLEdBQUdDLCtDQUFRLENBQUNDLE1BQVQsQ0FBZ0I7QUFDbENDLEtBQUcsRUFBRTtBQUFFQyxRQUFJLEVBQUVILCtDQUFRLENBQUNDLE1BQVQsQ0FBZ0JHLEtBQWhCLENBQXNCQyxRQUE5QjtBQUF3Q0MsWUFBUSxFQUFFLElBQWxEO0FBQXdEQyxRQUFJLEVBQUU7QUFBOUQsR0FENkI7QUFFbEMzRSxPQUFLLEVBQUU7QUFDTnVFLFFBQUksRUFBRUssTUFEQTtBQUVORixZQUFRLEVBQUUsSUFGSjtBQUdORyxVQUFNLEVBQUU7QUFIRixHQUYyQjtBQU9sQ3BFLFVBQVEsRUFBRTtBQUFFOEQsUUFBSSxFQUFFSyxNQUFSO0FBQWdCRixZQUFRLEVBQUU7QUFBMUI7QUFQd0IsQ0FBaEIsQ0FBbkI7QUFVQSxJQUFNSSxJQUFJLEdBQUdWLCtDQUFRLENBQUNXLEtBQVQsQ0FBZSxPQUFmLEVBQXdCWixVQUF4QixFQUFvQyxPQUFwQyxDQUFiO0FBRU8sSUFBTTNELE1BQU07QUFBQSwrQkFBRyxXQUFnQndFLEdBQWhCLEVBQXFCO0FBQzFDLFFBQU1oRixLQUFLLEdBQUdnRixHQUFHLENBQUNoRixLQUFsQjtBQUNBLFFBQU1TLFFBQVEsR0FBR3VFLEdBQUcsQ0FBQ3ZFLFFBQXJCO0FBQ0FQLFdBQU8sQ0FBQ0MsR0FBUixDQUFZLE1BQVosa0NBQXlCNkUsR0FBekI7QUFBOEJoRixXQUE5QjtBQUFxQ1M7QUFBckM7QUFDQSxRQUFNd0UsSUFBSSxHQUFHLElBQUlILElBQUosaUNBQWNFLEdBQWQ7QUFBbUJoRixXQUFuQjtBQUEwQlM7QUFBMUIsT0FBYjtBQUNBLGlCQUFhd0UsSUFBSSxDQUFDQyxJQUFMLEVBQWI7QUFDQSxHQU5rQjs7QUFBQSxrQkFBTjFFLE1BQU07QUFBQTtBQUFBO0FBQUEsR0FBWjtBQVFBLElBQU1ULElBQUk7QUFBQSxnQ0FBRyxXQUFnQmlGLEdBQWhCLEVBQXFCO0FBQ3hDLFFBQU1oRixLQUFLLEdBQUdnRixHQUFHLENBQUNoRixLQUFsQjtBQUNBLFFBQU1VLE1BQU0sU0FBU29FLElBQUksQ0FBQy9FLElBQUwsQ0FBVTtBQUFFQztBQUFGLEtBQVYsRUFBcUJtRixJQUFyQixFQUFyQjtBQUNBLFdBQU96RSxNQUFQO0FBQ0EsR0FKZ0I7O0FBQUEsa0JBQUpYLElBQUk7QUFBQTtBQUFBO0FBQUEsR0FBVjtBQU1BLElBQU1ZLGFBQWE7QUFBQSxnQ0FBRyxXQUFnQlgsS0FBaEIsRUFBdUJnRixHQUF2QixFQUE0QjtBQUN4RCxRQUFNdEUsTUFBTSxTQUFTb0UsSUFBSSxDQUFDTSxNQUFMLENBQVk7QUFBRXBGO0FBQUYsS0FBWixFQUF1QmdGLEdBQXZCLENBQXJCO0FBQ0EsV0FBT3RFLE1BQVA7QUFDQSxHQUh5Qjs7QUFBQSxrQkFBYkMsYUFBYTtBQUFBO0FBQUE7QUFBQSxHQUFuQjtBQUtBLElBQU1FLGFBQWE7QUFBQSxnQ0FBRyxXQUFnQmIsS0FBaEIsRUFBdUI7QUFDbkQsUUFBTVUsTUFBTSxTQUFTb0UsSUFBSSxDQUFDTyxTQUFMLENBQWU7QUFBRXJGO0FBQUYsS0FBZixDQUFyQjtBQUNBLFdBQU9VLE1BQVA7QUFDQSxHQUh5Qjs7QUFBQSxrQkFBYkcsYUFBYTtBQUFBO0FBQUE7QUFBQSxHQUFuQjtBQUtRaUUsbUVBQWYsRTs7Ozs7Ozs7Ozs7O0FDdENBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTs7QUFFQSxJQUFNUSxHQUFHLEdBQUcsTUFBTTtBQUNqQixzQkFBTywyREFBQyw4REFBRCxPQUFQO0FBQ0EsQ0FGRDs7QUFJZUEsa0VBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1JBO0FBQ0E7QUFFQTtBQUVPLElBQU1DLFlBQVksR0FBRyxVQUl0QjtBQUFBLE1BSnVCO0FBQzVCQyxhQUFTLEVBQUVDLFNBRGlCO0FBRTVCQztBQUY0QixHQUl2QjtBQUFBLE1BREZDLElBQ0U7O0FBQ0wsc0JBQ0MsMkRBQUMsc0RBQUQsZUFDS0EsSUFETDtBQUVDLFVBQU0sRUFBR0MsS0FBRCxJQUNQQyx3RUFBZSxrQkFDZCwyREFBQyxTQUFELEVBQWVELEtBQWYsQ0FEYyxnQkFHZCwyREFBQyx5REFBRDtBQUFVLFFBQUUsRUFBRUY7QUFBZDtBQU5ILEtBREQ7QUFZQSxDQWpCTSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDTFA7QUFDQTtBQUVBO0FBRU8sSUFBTUksV0FBVyxHQUFHLFVBSXJCO0FBQUEsTUFKc0I7QUFDM0JOLGFBQVMsRUFBRUMsU0FEZ0I7QUFFM0JDO0FBRjJCLEdBSXRCO0FBQUEsTUFERkMsSUFDRTs7QUFDTCxzQkFBTywyREFBQyxzREFBRCxlQUFXQSxJQUFYO0FBQWlCLFVBQU0sRUFBR0MsS0FBRCxpQkFBVywyREFBQyxTQUFELEVBQWVBLEtBQWY7QUFBcEMsS0FBUDtBQUNBLENBTk0sQzs7Ozs7Ozs7Ozs7O0FDTFA7QUFBQTtBQUFPLElBQU1DLGVBQWUsR0FBRyxNQUFNO0FBQ3BDO0FBQ0EsTUFBSSxPQUFPRSxZQUFQLEtBQXdCLFdBQTVCLEVBQXlDO0FBQ3hDLFdBQU8sSUFBUDtBQUNBOztBQUVELE1BQUlBLFlBQVksQ0FBQ0MsT0FBYixDQUFxQixjQUFyQixDQUFKLEVBQTBDO0FBQ3pDLFdBQU8sSUFBUDtBQUNBOztBQUNELFNBQU8sS0FBUDtBQUNBLENBVk0sQzs7Ozs7Ozs7Ozs7O0FDQVA7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRUE7QUFDQTtBQUVBO0FBQ0E7QUFDQTs7QUFFQSxJQUFNQyxNQUFNLEdBQUlMLEtBQUQsSUFBVztBQUN6QixzQkFDQywyREFBQyx1REFBRCxxQkFPQywyREFBQyxpRkFBRDtBQUFhLFFBQUksRUFBQyxNQUFsQjtBQUF5QixhQUFTLEVBQUVNLHNEQUFwQztBQUF5QyxnQkFBWSxFQUFDO0FBQXRELElBUEQsZUFRQywyREFBQyx5REFBRDtBQUFVLE1BQUUsRUFBQztBQUFiLElBUkQsQ0FERDtBQVlBLENBYkQ7O0FBZWVELHFFQUFmLEU7Ozs7Ozs7Ozs7OztBQ3pCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQSxJQUFNRSxZQUFZLEdBQUcscUJBQXJCO0FBQ0EsSUFBTUMsY0FBYyxHQUFHLHVCQUF2QjtBQUVBLElBQU1DLFlBQVksR0FBRztBQUNwQkMsU0FBTyxFQUFFO0FBRFcsQ0FBckI7QUFJTyxJQUFNQyxhQUFhLEdBQUlDLEtBQUQsSUFBV0EsS0FBSyxDQUFDQyxXQUF2QztBQUVBLElBQU1DLFVBQVUsR0FBR0MsdUVBQWMsQ0FBQ0osYUFBRCxFQUFpQkMsS0FBRCxJQUFXO0FBQ2xFdEcsU0FBTyxDQUFDQyxHQUFSLENBQVlxRyxLQUFaO0FBQ0EsU0FBT0EsS0FBSyxDQUFDRixPQUFiO0FBQ0EsQ0FIdUMsQ0FBakM7QUFLQSxJQUFNRyxXQUFXLEdBQUdHLHNFQUFhLENBQUNQLFlBQUQsRUFBZTtBQUN0RCxHQUFDRCxjQUFELEdBQWtCLENBQUNJLEtBQUQsV0FBd0I7QUFBQSxRQUFoQjtBQUFFSztBQUFGLEtBQWdCO0FBQ3pDTCxTQUFLLENBQUNGLE9BQU4sR0FBZ0IsS0FBaEI7QUFDQTtBQUhxRCxDQUFmLENBQWpDO0FBTUEsSUFBTVEsV0FBVyxHQUFHQyxxRUFBWSxDQUFDWixZQUFELENBQWhDOztBQUVQLFVBQVVhLFFBQVYsUUFBZ0M7QUFBQSxNQUFiO0FBQUVIO0FBQUYsR0FBYTtBQUMvQixRQUFNSSw4REFBRyxDQUFDO0FBQUUxQyxRQUFJLEVBQUU2QixjQUFSO0FBQXdCUyxXQUFPLEVBQUU7QUFBRVAsYUFBTyxFQUFFO0FBQVg7QUFBakMsR0FBRCxDQUFUO0FBQ0E7O0FBRU0sVUFBVVksUUFBVixHQUFxQjtBQUMzQixRQUFNQyw4REFBRyxDQUFDLENBQUMsTUFBTUMscUVBQVUsQ0FBQ2pCLFlBQUQsRUFBZWEsUUFBZixDQUFqQixDQUFELENBQVQ7QUFDQSxDOzs7Ozs7Ozs7Ozs7QUMvQkQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBRUEsSUFBTUssSUFBSSxnQkFBRztBQUFHLE1BQUksRUFBQztBQUFSLFVBQWI7O0FBRUEsSUFBTUMsSUFBSSxHQUFHLFVBQThCO0FBQUEsTUFBN0I7QUFBRWhCLFdBQUY7QUFBV1E7QUFBWCxHQUE2QjtBQUMxQ1MseURBQVMsQ0FBQyxNQUFNLENBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0E1QlEsRUE0Qk4sRUE1Qk0sQ0FBVDs7QUE4QkEsTUFBTUMsT0FBTyxHQUFJQyxLQUFELElBQVc7QUFDMUJ2SCxXQUFPLENBQUNDLEdBQVIsQ0FBWSxTQUFaLEVBQXVCc0gsS0FBdkI7QUFDQUEsU0FBSyxDQUFDQyxjQUFOLEdBRjBCLENBSTFCO0FBQ0E7QUFDQTs7QUFFQUMsU0FBSyxDQUFDLHlDQUFELEVBQTRDO0FBQ2hEQyxpQkFBVyxFQUFFLFNBRG1DO0FBRWhEQyxVQUFJLEVBQUU7QUFGMEMsS0FBNUMsQ0FBTCxDQUlFQyxJQUpGLENBSVE3SCxJQUFELElBQVU7QUFDZkMsYUFBTyxDQUFDQyxHQUFSLENBQVksTUFBWixFQUFvQkYsSUFBSSxDQUFDdUMsT0FBTCxDQUFhWCxHQUFiLENBQWlCLFlBQWpCLENBQXBCO0FBQ0EsYUFBTzVCLElBQUksQ0FBQ1AsSUFBTCxFQUFQO0FBQ0EsS0FQRixFQVFFb0ksSUFSRixDQVFRakksR0FBRCxJQUFTSyxPQUFPLENBQUNDLEdBQVIsQ0FBWSxLQUFaLEVBQW1CTixHQUFuQixDQVJoQjtBQVNBLEdBakJEOztBQW1CQUssU0FBTyxDQUFDQyxHQUFSLENBQVksY0FBWjtBQUVBLHNCQUNDLHFGQUNDO0FBQUcsUUFBSSxFQUFDLDZCQUFSO0FBQXNDLFdBQU8sRUFBRXFIO0FBQS9DLFlBREQsZUFJQyxzRUFKRCxlQUtDO0FBQUcsUUFBSSxFQUFDO0FBQVIsV0FMRCxlQU1DLHNFQU5ELGVBT0M7QUFDQyxPQUFHLEVBQUMsc0NBREw7QUFFQyxrQkFBYyxFQUFDLGlDQUZoQjtBQUdDLFdBQU8sRUFBQztBQUhULElBUEQsQ0FERDtBQWdCQSxDQXBFRDs7QUFzRUEsSUFBTU8sT0FBTyxHQUFHQywyREFBTyxDQUNyQnhCLEtBQUQsS0FBWTtBQUNYRixTQUFPLEVBQUVJLHlEQUFVLENBQUNGLEtBQUQ7QUFEUixDQUFaLENBRHNCLEVBSXRCO0FBQ0NNLGlFQUFXQTtBQURaLENBSnNCLENBQXZCO0FBU2VpQixzRUFBTyxDQUFDVCxJQUFELENBQXRCLEU7Ozs7Ozs7Ozs7OztBQ3RGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Q0FDQTs7QUFDQTtBQUVPLElBQU1XLFdBQVcsR0FBRyxDQUFDQyxLQUFELEVBQVFDLE1BQVIsS0FBbUI7QUFDN0MsTUFBTXZILElBQUksR0FBRyxDQUNaO0FBQUV3SCxTQUFLLEVBQUUsR0FBVDtBQUFjQyxTQUFLLEVBQUU7QUFBckIsR0FEWSxFQUVaO0FBQUVELFNBQUssRUFBRSxHQUFUO0FBQWNDLFNBQUssRUFBRTtBQUFyQixHQUZZLEVBR1o7QUFBRUQsU0FBSyxFQUFFLEdBQVQ7QUFBY0MsU0FBSyxFQUFFO0FBQXJCLEdBSFksRUFJWjtBQUFFRCxTQUFLLEVBQUUsR0FBVDtBQUFjQyxTQUFLLEVBQUU7QUFBckIsR0FKWSxFQUtaO0FBQUVELFNBQUssRUFBRSxHQUFUO0FBQWNDLFNBQUssRUFBRTtBQUFyQixHQUxZLEVBTVo7QUFBRUQsU0FBSyxFQUFFLEdBQVQ7QUFBY0MsU0FBSyxFQUFFO0FBQXJCLEdBTlksRUFPWjtBQUFFRCxTQUFLLEVBQUUsR0FBVDtBQUFjQyxTQUFLLEVBQUU7QUFBckIsR0FQWSxFQVFaO0FBQUVELFNBQUssRUFBRSxHQUFUO0FBQWNDLFNBQUssRUFBRTtBQUFyQixHQVJZLEVBU1o7QUFBRUQsU0FBSyxFQUFFLEdBQVQ7QUFBY0MsU0FBSyxFQUFFO0FBQXJCLEdBVFksRUFVWjtBQUFFRCxTQUFLLEVBQUUsR0FBVDtBQUFjQyxTQUFLLEVBQUU7QUFBckIsR0FWWSxDQUFiLENBRDZDLENBYzdDO0FBQ0E7O0FBQ0EsTUFBTUMsTUFBTSxHQUFHO0FBQUVDLE9BQUcsRUFBRSxFQUFQO0FBQVdDLFNBQUssRUFBRSxFQUFsQjtBQUFzQkMsVUFBTSxFQUFFLEVBQTlCO0FBQWtDQyxRQUFJLEVBQUU7QUFBeEMsR0FBZjtBQUNBLE1BQU1DLFVBQVUsR0FBR1QsS0FBSyxHQUFHSSxNQUFNLENBQUNJLElBQWYsR0FBc0JKLE1BQU0sQ0FBQ0UsS0FBaEQ7QUFDQSxNQUFNSSxXQUFXLEdBQUdULE1BQU0sR0FBR0csTUFBTSxDQUFDQyxHQUFoQixHQUFzQkQsTUFBTSxDQUFDRyxNQUFqRDtBQUVBLE1BQU1JLENBQUMsR0FBR0MsNENBQUEsR0FBZUMsVUFBZixDQUEwQixDQUFDLENBQUQsRUFBSUosVUFBSixDQUExQixFQUEyQ0ssWUFBM0MsQ0FBd0QsR0FBeEQsQ0FBVjtBQUNBLE1BQU1DLENBQUMsR0FBR0gsOENBQUEsR0FBaUJDLFVBQWpCLENBQTRCLENBQUNILFdBQUQsRUFBYyxDQUFkLENBQTVCLENBQVY7QUFDQSxNQUFNTSxDQUFDLEdBQUdKLCtDQUFBLEdBQWtCSyxLQUFsQixDQUF3QixDQUFDLFNBQUQsRUFBWSxTQUFaLEVBQXVCLFNBQXZCLENBQXhCLENBQVY7QUFDQU4sR0FBQyxDQUFDTyxNQUFGLENBQ0N4SSxJQUFJLENBQUN5SSxHQUFMLENBQVMsVUFBVUMsQ0FBVixFQUFhO0FBQ3JCLFdBQU9BLENBQUMsQ0FBQ2xCLEtBQVQ7QUFDQSxHQUZELENBREQ7QUFLQWEsR0FBQyxDQUFDRyxNQUFGLENBQVMsQ0FDUixDQURRLEVBRVJOLHNDQUFBLENBQU9sSSxJQUFQLEVBQWEsVUFBVTBJLENBQVYsRUFBYTtBQUN6QixXQUFPQSxDQUFDLENBQUNqQixLQUFUO0FBQ0EsR0FGRCxDQUZRLENBQVQ7QUFPQSxNQUFNa0IsR0FBRyxHQUFHbEksUUFBUSxDQUFDbUksYUFBVCxDQUF1QixLQUF2QixDQUFaO0FBQ0FELEtBQUcsQ0FBQ0UsS0FBSixDQUFVdkIsS0FBVixHQUFrQkEsS0FBSyxHQUFHLElBQTFCO0FBQ0FxQixLQUFHLENBQUNFLEtBQUosQ0FBVXRCLE1BQVYsR0FBbUJBLE1BQU0sR0FBRyxJQUE1QjtBQUVBLE1BQU11QixHQUFHLEdBQUdaLHlDQUFBLENBQ0hTLEdBREcsRUFFVkksTUFGVSxDQUVILEtBRkcsRUFHVkMsSUFIVSxDQUdMLE9BSEssRUFHSTFCLEtBSEosRUFJVjBCLElBSlUsQ0FJTCxRQUpLLEVBSUt6QixNQUpMLENBQVo7QUFLQSxNQUFNMEIsQ0FBQyxHQUFHSCxHQUFHLENBQ1hDLE1BRFEsQ0FDRCxHQURDLEVBRVJDLElBRlEsQ0FFSCxXQUZHLEVBRVUsZUFBZXRCLE1BQU0sQ0FBQ0ksSUFBdEIsR0FBNkIsR0FBN0IsR0FBbUNKLE1BQU0sQ0FBQ0MsR0FBMUMsR0FBZ0QsR0FGMUQsRUFHUnFCLElBSFEsQ0FHSCxPQUhHLEVBR01qQixVQUhOLEVBSVJpQixJQUpRLENBSUgsUUFKRyxFQUlPaEIsV0FKUCxDQUFWO0FBS0FpQixHQUFDLENBQUNDLFNBQUYsQ0FBWSxNQUFaLEVBQ0VsSixJQURGLENBQ09BLElBRFAsRUFFRW1KLEtBRkYsR0FHRUosTUFIRixDQUdTLE1BSFQsRUFJRUMsSUFKRixDQUlPLE9BSlAsRUFJZ0IsS0FKaEIsRUFLRUEsSUFMRixDQUtPLEdBTFAsRUFLWSxVQUFVTixDQUFWLEVBQWE7QUFDdkIsV0FBT1QsQ0FBQyxDQUFDUyxDQUFDLENBQUNsQixLQUFILENBQVI7QUFDQSxHQVBGLEVBUUV3QixJQVJGLENBUU8sT0FSUCxFQVFnQmYsQ0FBQyxDQUFDbUIsU0FBRixFQVJoQixFQVNFSixJQVRGLENBU08sR0FUUCxFQVNZLFVBQVVOLENBQVYsRUFBYTtBQUN2QixXQUFPTCxDQUFDLENBQUNLLENBQUMsQ0FBQ2pCLEtBQUgsQ0FBUjtBQUNBLEdBWEYsRUFZRXVCLElBWkYsQ0FZTyxRQVpQLEVBWWlCLFVBQVVOLENBQVYsRUFBYTtBQUM1QixXQUFPVixXQUFXLEdBQUdLLENBQUMsQ0FBQ0ssQ0FBQyxDQUFDakIsS0FBSCxDQUF0QjtBQUNBLEdBZEYsRUFlRXVCLElBZkYsQ0FlTyxNQWZQLEVBZWUsVUFBVU4sQ0FBVixFQUFhO0FBQzFCLFdBQU9KLENBQUMsQ0FBQ0ksQ0FBQyxDQUFDbEIsS0FBSCxDQUFSO0FBQ0EsR0FqQkY7QUFtQkF5QixHQUFDLENBQUNGLE1BQUYsQ0FBUyxHQUFULEVBQ0VDLElBREYsQ0FDTyxXQURQLEVBQ29CLGlCQUFpQmhCLFdBQWpCLEdBQStCLEdBRG5ELEVBRUVxQixJQUZGLENBRU9uQiw2Q0FBQSxDQUFjRCxDQUFkLENBRlA7QUFJQWdCLEdBQUMsQ0FBQ0YsTUFBRixDQUFTLEdBQVQsRUFBY00sSUFBZCxDQUFtQm5CLDJDQUFBLENBQVlHLENBQVosQ0FBbkI7QUFFQSxTQUFPTSxHQUFHLENBQUNXLFNBQVg7QUFDQSxDQTNFTTs7QUE2RVAsU0FBU0MsWUFBVCxDQUFzQnZKLElBQXRCLEVBQTRCO0FBQzNCLFNBQU87QUFBRXdKLFVBQU0sRUFBRXhKO0FBQVYsR0FBUDtBQUNBOztBQUVELE1BQU1zRixHQUFOLFNBQWtCbUUsNENBQUssQ0FBQzVFLFNBQXhCLENBQWtDO0FBQ2pDNkUsYUFBVyxDQUFDMUUsS0FBRCxFQUFRO0FBQ2xCLFVBQU1BLEtBQU47QUFDQTs7QUFFRDJFLFFBQU0sR0FBRztBQUNSLFFBQU16SixJQUFJLEdBQUdtSCxXQUFXLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FBeEI7QUFDQSx3QkFBTztBQUFLLDZCQUF1QixFQUFFa0MsWUFBWSxDQUFDckosSUFBRDtBQUExQyxNQUFQO0FBQ0E7O0FBUmdDOztBQVduQm9GLGtFQUFmLEU7Ozs7Ozs7Ozs7OztBQ2hHQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFFQSxJQUFNQyxZQUFZLEdBQUcseUJBQXJCO0FBQ0EsSUFBTUMsY0FBYyxHQUFHLDJCQUF2QjtBQUVBLElBQU1DLFlBQVksR0FBRztBQUNwQkMsU0FBTyxFQUFFO0FBRFcsQ0FBckI7QUFJTyxJQUFNQyxhQUFhLEdBQUlDLEtBQUQsSUFBV0EsS0FBSyxDQUFDZ0UsZUFBdkM7QUFFQSxJQUFNOUQsVUFBVSxHQUFHQyx1RUFBYyxDQUFDSixhQUFELEVBQWlCQyxLQUFELElBQVc7QUFDbEV0RyxTQUFPLENBQUNDLEdBQVIsQ0FBWXFHLEtBQVo7QUFDQSxTQUFPQSxLQUFLLENBQUNGLE9BQWI7QUFDQSxDQUh1QyxDQUFqQztBQUtBLElBQU1rRSxlQUFlLEdBQUc1RCxzRUFBYSxDQUFDUCxZQUFELEVBQWU7QUFDMUQsR0FBQ0QsY0FBRCxHQUFrQixDQUFDSSxLQUFELFdBQXdCO0FBQUEsUUFBaEI7QUFBRUs7QUFBRixLQUFnQjtBQUN6Q0wsU0FBSyxDQUFDRixPQUFOLEdBQWdCLEtBQWhCO0FBQ0E7QUFIeUQsQ0FBZixDQUFyQztBQU1BLElBQU1RLFdBQVcsR0FBR0MscUVBQVksQ0FBQ1osWUFBRCxDQUFoQzs7QUFFUCxVQUFVc0UsWUFBVixRQUFvQztBQUFBLE1BQWI7QUFBRTVEO0FBQUYsR0FBYTtBQUNuQyxRQUFNSSw4REFBRyxDQUFDO0FBQUUxQyxRQUFJLEVBQUU2QixjQUFSO0FBQXdCUyxXQUFPLEVBQUU7QUFBRVAsYUFBTyxFQUFFO0FBQVg7QUFBakMsR0FBRCxDQUFUO0FBQ0E7O0FBRU0sVUFBVW9FLFlBQVYsR0FBeUI7QUFDL0IsUUFBTXZELDhEQUFHLENBQUMsQ0FBQyxNQUFNQyxxRUFBVSxDQUFDakIsWUFBRCxFQUFlc0UsWUFBZixDQUFqQixDQUFELENBQVQ7QUFDQSxDOzs7Ozs7Ozs7Ozs7QUMvQkQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBRUEsSUFBTXBELElBQUksZ0JBQUc7QUFBRyxNQUFJLEVBQUM7QUFBUixVQUFiOztBQUVBLElBQU1zRCxRQUFRLEdBQUcsVUFBOEI7QUFBQSxNQUE3QjtBQUFFckUsV0FBRjtBQUFXUTtBQUFYLEdBQTZCO0FBQzlDUyx5REFBUyxDQUFDLE1BQU0sQ0FDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQTVCUSxFQTRCTixFQTVCTSxDQUFUOztBQThCQSxNQUFNQyxPQUFPLEdBQUlDLEtBQUQsSUFBVztBQUMxQnZILFdBQU8sQ0FBQ0MsR0FBUixDQUFZLFNBQVosRUFBdUJzSCxLQUF2QjtBQUNBQSxTQUFLLENBQUNDLGNBQU4sR0FGMEIsQ0FJMUI7QUFDQTtBQUNBOztBQUVBQyxTQUFLLENBQUMsc0NBQUQsRUFBeUMsQ0FDN0M7QUFENkMsS0FBekMsQ0FBTCxDQUdFRyxJQUhGLENBR1E3SCxJQUFELElBQVU7QUFDZkMsYUFBTyxDQUFDQyxHQUFSLENBQVksTUFBWixFQUFvQkYsSUFBSSxDQUFDdUMsT0FBTCxDQUFhWCxHQUFiLENBQWlCLFlBQWpCLENBQXBCO0FBQ0EsYUFBTzVCLElBQUksQ0FBQ1AsSUFBTCxFQUFQO0FBQ0EsS0FORixFQU9Fb0ksSUFQRixDQU9RakksR0FBRCxJQUFTSyxPQUFPLENBQUNDLEdBQVIsQ0FBWSxLQUFaLEVBQW1CTixHQUFuQixDQVBoQjtBQVNBK0ssU0FBSyxDQUFDLE9BQUQsQ0FBTDtBQUNBLEdBbEJEOztBQW9CQTFLLFNBQU8sQ0FBQ0MsR0FBUixDQUFZLGtCQUFaO0FBRUEsc0JBQ0MscUZBQ0M7QUFBRyxRQUFJLEVBQUMsNkJBQVI7QUFBc0MsV0FBTyxFQUFFcUg7QUFBL0MsWUFERCxlQUlDLHNFQUpELGVBS0M7QUFBRyxRQUFJLEVBQUM7QUFBUixXQUxELGVBTUMsc0VBTkQsQ0FERDtBQVdBLENBaEVEOztBQWtFQSxJQUFNTyxPQUFPLEdBQUdDLDJEQUFPLENBQ3JCeEIsS0FBRCxLQUFZO0FBQ1hGLFNBQU8sRUFBRUkseURBQVUsQ0FBQ0YsS0FBRDtBQURSLENBQVosQ0FEc0IsRUFJdEI7QUFDQ00saUVBQVdBO0FBRFosQ0FKc0IsQ0FBdkI7QUFTZWlCLHNFQUFPLENBQUM0QyxRQUFELENBQXRCLEU7Ozs7Ozs7Ozs7O0FDbEZBLDZDOzs7Ozs7Ozs7OztBQ0FBLHdDOzs7Ozs7Ozs7OztBQ0FBLDBDOzs7Ozs7Ozs7OztBQ0FBLCtCOzs7Ozs7Ozs7OztBQ0FBLG9DOzs7Ozs7Ozs7OztBQ0FBLCtCOzs7Ozs7Ozs7OztBQ0FBLHFDOzs7Ozs7Ozs7OztBQ0FBLGtDOzs7Ozs7Ozs7OztBQ0FBLGtDOzs7Ozs7Ozs7OztBQ0FBLHNDOzs7Ozs7Ozs7OztBQ0FBLHFDOzs7Ozs7Ozs7OztBQ0FBLGtDOzs7Ozs7Ozs7OztBQ0FBLDZDOzs7Ozs7Ozs7OztBQ0FBLHdDOzs7Ozs7Ozs7OztBQ0FBLDZDOzs7Ozs7Ozs7OztBQ0FBLCtDIiwiZmlsZSI6ImRpc3Qvc2VydmVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9zcmMvc2VydmVyL2luZGV4LmpzXCIpO1xuIiwiaW1wb3J0IHtcblx0Y3JlYXRlLFxuXHRmaW5kLFxuXHR1cGRhdGVCeUVtYWlsLFxuXHRkZWxldGVCeUVtYWlsLFxufSBmcm9tIFwiLi4vbW9kZWxzL3VzZXJNb2RlbFwiO1xuaW1wb3J0IGV4cHJlc3MgZnJvbSBcImV4cHJlc3NcIjtcbmltcG9ydCBib2R5UGFyc2VyIGZyb20gXCJib2R5LXBhcnNlclwiO1xuXG5jb25zdCB1c2VyUm91dGVyID0gZXhwcmVzcy5Sb3V0ZXIoKTtcbi8vIHVzZXJSb3V0ZXIudXNlKGJvZHlQYXJzZXIudXJsZW5jb2RlZCh7IGV4dGVuZGVkOiBmYWxzZSB9KSk7XG51c2VyUm91dGVyLnVzZShib2R5UGFyc2VyLmpzb24oKSk7XG5cbnVzZXJSb3V0ZXIucG9zdChcIi9sb2dpblwiLCBhc3luYyAoKSA9PiB7fSk7XG5cbnVzZXJSb3V0ZXIucG9zdChcIi9zaWdudXBcIiwgYXN5bmMgKHJlcSwgcmVzKSA9PiB7XG5cdGNvbnN0IGZpbmRSZXN1bHQgPSBhd2FpdCBmaW5kKHsgZW1haWw6IHJlcS5ib2R5LmVtYWlsIH0pO1xuXHRjb25zb2xlLmxvZyhcImZpbmRSZXN1bHRcIiwgZmluZFJlc3VsdCk7XG5cdGlmIChmaW5kUmVzdWx0Lmxlbmd0aCkge1xuXHRcdHJlcy5zZW5kKHsgbWVzc2FnZTogXCJVc2VyIGFscmVhZHkgZXhpc3RzLlwiIH0pO1xuXHRcdHJldHVybjtcblx0fVxuXHRjb25zdCBlbnRyeSA9IGF3YWl0IGNyZWF0ZSh7XG5cdFx0Li4ucmVxLmJvZHksXG5cdFx0ZW1haWw6IHJlcS5ib2R5LmVtYWlsLFxuXHRcdHBhc3N3b3JkOiByZXEuYm9keS5wYXNzd29yZCxcblx0fSk7XG5cdHJlcy5zZW5kKGVudHJ5KTtcbn0pO1xuXG51c2VyUm91dGVyLnBvc3QoXCIvdXBkYXRlXCIsIGFzeW5jIChyZXEsIHJlcykgPT4ge1xuXHRjb25zdCByZXN1bHQgPSBhd2FpdCB1cGRhdGVCeUVtYWlsKHJlcS5ib2R5LmVtYWlsLCByZXEuYm9keS5kYXRhKTtcblx0cmVzLnNlbmQocmVzdWx0KTtcbn0pO1xuXG51c2VyUm91dGVyLnBvc3QoXCIvZGVsZXRlXCIsIGFzeW5jIChyZXEsIHJlcykgPT4ge1xuXHRjb25zdCByZXN1bHQgPSBhd2FpdCBkZWxldGVCeUVtYWlsKHJlcS5ib2R5LmVtYWlsKTtcblx0cmVzLnNlbmQocmVzdWx0KTtcbn0pO1xuXG5leHBvcnQgZGVmYXVsdCB1c2VyUm91dGVyO1xuIiwiaW1wb3J0IGh0dHBzIGZyb20gXCJodHRwc1wiO1xuaW1wb3J0IGZzIGZyb20gXCJmc1wiO1xuaW1wb3J0IGV4cHJlc3MgZnJvbSBcImV4cHJlc3NcIjtcbmltcG9ydCBtb25nb29zZSBmcm9tIFwibW9uZ29vc2VcIjtcbmltcG9ydCBNZW1jYWNoZWQgZnJvbSBcIm1lbWNhY2hlZFwiO1xuLy8gaW1wb3J0IGJvZHlQYXJzZXIgZnJvbSBcImJvZHktcGFyc2VyXCI7XG5pbXBvcnQgY29va2llUGFyc2VyIGZyb20gXCJjb29raWUtcGFyc2VyXCI7XG5pbXBvcnQgeyBoYW5kbGVSZW5kZXIgfSBmcm9tIFwiLi9taWRkbGV3YXJlcy9oYW5kbGVSZW5kZXJcIjtcblxuaW1wb3J0IHVzZXJSb3V0ZXIgZnJvbSBcIi4vY29udHJvbGxlcnMvdXNlckNvbnRyb2xsZXJcIjtcblxuaW1wb3J0IGpzZG9tIGZyb20gXCJqc2RvbVwiO1xuaW1wb3J0IHBkZiBmcm9tIFwiaHRtbC1wZGZcIjtcblxuLy8gbW9uZ29vc2UuY29ubmVjdChcbi8vIFx0XCJtb25nb2RiOi8vbG9jYWxob3N0OjI3MDE3L3Rlc3QxXCIsXG4vLyBcdHtcbi8vIFx0XHR1c2VOZXdVcmxQYXJzZXI6IHRydWUsXG4vLyBcdFx0dXNlVW5pZmllZFRvcG9sb2d5OiB0cnVlLFxuLy8gXHR9LFxuLy8gXHQoZXJyKSA9PiB7XG4vLyBcdFx0aWYgKGVycikge1xuLy8gXHRcdFx0Y29uc29sZS5lcnJvcihcIk1vbmdvIERCIENvbm5lY3Rpb24gRXJyb3IhXCIpO1xuLy8gXHRcdFx0Y29uc29sZS5lcnJvcihlcnIpO1xuLy8gXHRcdH0gZWxzZSB7XG4vLyBcdFx0XHRjb25zb2xlLmxvZyhcIk1vbmdvIERCIENvbm5lY3RlZFwiKTtcbi8vIFx0XHR9XG4vLyBcdH1cbi8vICk7XG5cbi8vXG5jb25zdCBodG1sID0gXCI8aHRtbD48aGVhZD48L2hlYWQ+PGJvZHk+PC9ib2R5PjwvaHRtbD5cIjtcblxuY29uc3Qgb3B0aW9ucyA9IHt9O1xuXG5jb25zdCB7IEpTRE9NIH0gPSBqc2RvbTtcbmdsb2JhbC5kb20gPSBuZXcgSlNET00oaHRtbCwgb3B0aW9ucyk7XG5nbG9iYWwud2luZG93ID0gZG9tLndpbmRvdztcbmdsb2JhbC5kb2N1bWVudCA9IGRvbS53aW5kb3cuZG9jdW1lbnQ7XG4vL1xuXG5jb25zdCBtZW1jYWNoZWQgPSBuZXcgTWVtY2FjaGVkKFwiMTI3LjAuMC4xOjExMjExXCIpO1xuXG5jb25zdCBhcHAgPSBleHByZXNzKCk7XG5hcHAudXNlKGV4cHJlc3Muc3RhdGljKFwicHVibGljXCIpKTtcbmFwcC51c2UoY29va2llUGFyc2VyKCkpO1xuXG5jb25zdCBwdWJsaWNSb3V0ZXIgPSBleHByZXNzLlJvdXRlcigpO1xuY29uc3QgYXBpUm91dGVyID0gZXhwcmVzcy5Sb3V0ZXIoKTtcblxuYXBwLnVzZShcIi9cIiwgcHVibGljUm91dGVyKTtcbmFwcC51c2UoXCIvaG9tZVwiLCBwdWJsaWNSb3V0ZXIpO1xuYXBwLnVzZShcIi9hcGlcIiwgYXBpUm91dGVyKTtcblxuLy8gcHVibGljUm91dGVyLnVzZShoYW5kbGVSZW5kZXIoKSk7XG5cbnB1YmxpY1JvdXRlci5nZXQoXCIvXCIsIGhhbmRsZVJlbmRlcigpLCAocmVxLCByZXMpID0+IHtcblx0bWVtY2FjaGVkLnNldChcInN0aFwiLCBcImFzZFwiLCAzMDAsIGZ1bmN0aW9uIChlcnIsIGRhdGEpIHtcblx0XHRjb25zb2xlLmxvZyhcIi8gZXJyLCBkYXRhXCIsIGVyciwgZGF0YSk7XG5cdH0pO1xuXG5cdC8vIHJlcy5jb29raWUoXCJhY2Nlc3MtdG9rZW5cIiwgcHJvY2Vzcy5lbnYuUE9SVCwge1xuXHQvLyBcdGV4cGlyZXM6IG5ldyBEYXRlKERhdGUubm93KCkgKyAxMjAwMCksXG5cdC8vIH0pO1xuXHRjb25zb2xlLmxvZyhcIi9cIik7XG5cdGNvbnNvbGUubG9nKFwicmVxLlJlZmVyZXJcIiwgcmVxLmdldChcIlJlZmVycmVyXCIpKTtcblx0Ly8gY29uc29sZS5sb2coXCJyZXEuY29va2llc1wiLCByZXEuaGVhZGVycy5jb29raWUpO1xuXHRyZXMuaGVhZGVyKFwiQWNjZXNzLUNvbnRyb2wtQWxsb3ctQ3JlZGVudGlhbHNcIiwgdHJ1ZSk7XG5cdC8vIHJlcy5oZWFkZXIoXCJBY2Nlc3MtQ29udHJvbC1BbGxvdy1PcmlnaW5cIiwgXCJodHRwczovL2xkZXYubGltYmVyLmNvLmluOjkwMDBcIik7XG5cdHJlcy5jb29raWUoXCJhY2Nlc3MtdG9rZW5cIiwgcHJvY2Vzcy5lbnYuUE9SVCwgeyBodHRwT25seTogdHJ1ZSB9KTtcblx0cmVzLmNvb2tpZShcInJhbmRvbVwiLCBcInJhbmRvbVwiKTtcblx0cmVzLnNlbmQocmVzLmxvY2Fscy5odG1sKTtcbn0pO1xuXG5wdWJsaWNSb3V0ZXIuZ2V0KFwiL2hvbWVcIiwgaGFuZGxlUmVuZGVyKCksIChyZXEsIHJlcykgPT4ge1xuXHRtZW1jYWNoZWQuZ2V0KFwic3RoXCIsIGZ1bmN0aW9uIChlcnIsIGRhdGEpIHtcblx0XHRjb25zb2xlLmxvZyhcIi9ob21lIGVyciwgZGF0YVwiLCBlcnIsIGRhdGEpO1xuXHR9KTtcblxuXHRjb25zb2xlLmxvZyhcIi9ob21lXCIpO1xuXHRjb25zb2xlLmxvZyhcInJlcS5SZWZlcmVyXCIsIHJlcS5nZXQoXCJSZWZlcnJlclwiKSk7XG5cdGNvbnNvbGUubG9nKFwicmVxLk9yaWdpblwiLCByZXEuZ2V0KFwiT3JpZ2luXCIpKTtcblx0Y29uc29sZS5sb2coXCJyZXEuY29va2llc1wiLCByZXEuaGVhZGVycy5jb29raWUpO1xuXHRjb25zb2xlLmxvZyhcInJlcS5jb29raWVzXCIsIHJlcS5jb29raWVzKTtcblx0Ly8gcmVzLmhlYWRlcihcblx0Ly8gXHRcIkNvbnRlbnQtU2VjdXJpdHktUG9saWN5XCIsXG5cdC8vIFx0XCJmcmFtZS1hbmNlc3RvcnMgJ3NlbGYnOyBjb25uZWN0LXNyYyAnc2VsZic7XCJcblx0Ly8gKTtcblx0Ly8gcmVzLmNvb2tpZShcImFjY2Vzcy10b2tlblwiLCBwcm9jZXNzLmVudi5QT1JUKTtcblx0cmVzLnNlbmQocmVzLmxvY2Fscy5odG1sKTtcbn0pO1xuXG5wdWJsaWNSb3V0ZXIuZ2V0KFwiL3Byb2R1Y3RzXCIsIGhhbmRsZVJlbmRlcigpLCAocmVxLCByZXMpID0+IHtcblx0Y29uc29sZS5sb2coXCIvcHJvZHVjdHNcIik7XG5cdGNvbnNvbGUubG9nKFwicmVxLlJlZmVyZXJcIiwgcmVxLmdldChcIlJlZmVycmVyXCIpKTtcblx0Y29uc29sZS5sb2coXCJyZXEuY29va2llc1wiLCByZXEuaGVhZGVycy5jb29raWUpO1xuXHQvLyByZXMuaGVhZGVyKFwiWC1GcmFtZS1PcHRpb25zXCIsIFwiZGVueVwiKTtcblx0Ly8gcmVzLmhlYWRlcihcblx0Ly8gXHRcIkNvbnRlbnQtU2VjdXJpdHktUG9saWN5XCIsXG5cdC8vIFx0XCJmcmFtZS1hbmNlc3RvcnMgJ3NlbGYnIGh0dHBzOi8vbGRldi5saW1iZXIuY28uaW46OTAwMCBodHRwczovL2xvY2FsaG9zdDo5MDAwOyBjb25uZWN0LXNyYyAnc2VsZidcIlxuXHQvLyApO1xuXHQvLyByZXMuaGVhZGVyKFwiQ29udGVudC1TZWN1cml0eS1Qb2xpY3lcIiwgXCJjb25uZWN0LXNyYyAnc2VsZic7XCIpO1xuXHQvLyByZXMuY29va2llKFwiYWNjZXNzLXRva2VuXCIsIHByb2Nlc3MuZW52LlBPUlQpO1xuXHRyZXMuc2VuZChyZXMubG9jYWxzLmh0bWwpO1xufSk7XG5cbnB1YmxpY1JvdXRlci5nZXQoXCIvcGRmXCIsIGhhbmRsZVJlbmRlcigpLCAocmVxLCByZXMpID0+IHtcblx0Y29uc29sZS5sb2coXCIvcGRmXCIpO1xuXG5cdGNvbnN0IGZpbGVuYW1lID0gbmV3IERhdGUoKS5nZXRUaW1lKCk7XG5cdGNvbnN0IG9wdGlvbnMgPSB7IGZvcm1hdDogXCJMZXR0ZXJcIiB9O1xuXHRwZGZcblx0XHQuY3JlYXRlKHJlcy5sb2NhbHMuaHRtbCwgb3B0aW9ucylcblx0XHQudG9GaWxlKFwiLi9vdXRwdXQvXCIgKyBmaWxlbmFtZSArIFwiLnBkZlwiLCBmdW5jdGlvbiAoZXJyLCByZXMpIHtcblx0XHRcdGlmIChlcnIpIHJldHVybiBjb25zb2xlLmxvZyhlcnIpO1xuXHRcdFx0Y29uc29sZS5sb2cocmVzKTtcblx0XHR9KTtcblxuXHRyZXMuc2VuZChyZXMubG9jYWxzLmh0bWwpO1xufSk7XG5cbmFwaVJvdXRlci51c2UoKHJlcSwgcmVzLCBuZXh0KSA9PiB7XG5cdGNvbnNvbGUubG9nKFwiYXBpUm91dGVyXCIsIHJlcS5nZXQoXCJPcmlnaW5cIikpO1xuXHQvLyByZXMuaGVhZGVyKFwiQWNjZXNzLUNvbnRyb2wtQWxsb3ctT3JpZ2luXCIsIHJlcS5nZXQoXCJPcmlnaW5cIikpO1xuXHRuZXh0KCk7XG59KTtcblxuYXBpUm91dGVyLnVzZShcIi91c2Vyc1wiLCB1c2VyUm91dGVyKTtcblxuYXBpUm91dGVyLm9wdGlvbnMoXCIvdXNlclwiLCAocmVxLCByZXMpID0+IHtcblx0Y29uc29sZS5sb2coXCIqKioqSEVSRSoqKipcIik7XG5cdC8vIHJlcy5oZWFkZXIoXCJBY2Nlc3MtQ29udHJvbC1BbGxvdy1DcmVkZW50aWFsc1wiLCB0cnVlKTtcblx0cmVzLmhlYWRlcihcIkFjY2Vzcy1Db250cm9sLUFsbG93LUhlYWRlcnNcIiwgXCJDb250ZW50LVR5cGVcIik7XG5cdHJlcy5zZW5kKCk7XG59KTtcblxuYXBpUm91dGVyLmdldChcIi91c2VyXCIsIChyZXEsIHJlcykgPT4ge1xuXHQvLyByZXMuY29va2llKFwiYWNjZXNzLXRva2VuXCIsIHByb2Nlc3MuZW52LlBPUlQsIHtcblx0Ly8gXHRleHBpcmVzOiBuZXcgRGF0ZShEYXRlLm5vdygpICsgMTIwMDApLFxuXHQvLyB9KTtcblx0Y29uc29sZS5sb2coXCIvdXNlclwiKTtcblx0Y29uc29sZS5sb2coXCJyZXEuUmVmZXJlclwiLCByZXEuZ2V0KFwiUmVmZXJyZXJcIikpO1xuXHRjb25zb2xlLmxvZyhcInJlcS5jb29raWVzXCIsIHJlcS5oZWFkZXJzLmNvb2tpZSk7XG5cdC8vIHJlcy5zZXQoXCJDYWNoZS1Db250cm9sXCIsIFwicHJpdmF0ZSwgbWF4LWFnZT0zMFwiKTtcblx0Ly8gcmVzLnNldChcIkNhY2hlLUNvbnRyb2xcIiwgXCJwcml2YXRlXCIpO1xuXHRyZXMuaGVhZGVyKFwiQWNjZXNzLUNvbnRyb2wtQWxsb3ctQ3JlZGVudGlhbHNcIiwgdHJ1ZSk7XG5cdHJlc1xuXHRcdC5jb29raWUoXCJhY2Nlc3MtdG9rZW5cIiwgcHJvY2Vzcy5lbnYuUE9SVCwge1xuXHRcdFx0ZXhwaXJlczogbmV3IERhdGUoRGF0ZS5ub3coKSArIDIgKiA2MCAqIDEwMDApLFxuXHRcdFx0c2FtZVNpdGU6IFwiTm9uZVwiLFxuXHRcdFx0c2VjdXJlOiB0cnVlLFxuXHRcdH0pXG5cdFx0LmNvb2tpZShcInNvbWVzdHJpbmdcIiwgXCJzb21lcmFuZG9tc3RyaW5nXCIsIHtcblx0XHRcdGV4cGlyZXM6IG5ldyBEYXRlKERhdGUubm93KCkgKyAyICogNjAgKiAxMDAwKSxcblx0XHRcdHNhbWVTaXRlOiBcIk5vbmVcIixcblx0XHRcdHNlY3VyZTogdHJ1ZSxcblx0XHR9KTtcblx0cmVzLnNlbmQoW1wiYXJzZW5hbFwiLCBcImNoZWxzZWFcIiwgXCJtYW51XCIsIDRdKTtcblx0Y29uc29sZS5sb2coXCJzZW50XCIpO1xufSk7XG5cbmFwaVJvdXRlci5wb3N0KFwiL3VzZXJcIiwgKHJlcSwgcmVzKSA9PiB7XG5cdC8vIHJlcy5jb29raWUoXCJhY2Nlc3MtdG9rZW5cIiwgcHJvY2Vzcy5lbnYuUE9SVCwge1xuXHQvLyBcdGV4cGlyZXM6IG5ldyBEYXRlKERhdGUubm93KCkgKyAxMjAwMCksXG5cdC8vIH0pO1xuXHRjb25zb2xlLmxvZyhcIi91c2VyXCIpO1xuXHRjb25zb2xlLmxvZyhcInJlcS5SZWZlcmVyXCIsIHJlcS5nZXQoXCJSZWZlcnJlclwiKSk7XG5cdGNvbnNvbGUubG9nKFwicmVxLmNvb2tpZXNcIiwgcmVxLmhlYWRlcnMuY29va2llKTtcblx0cmVzLmhlYWRlcihcIkFjY2Vzcy1Db250cm9sLUFsbG93LUNyZWRlbnRpYWxzXCIsIHRydWUpO1xuXHRyZXMuY29va2llKFwiYWNjZXNzLXRva2VuXCIsIHByb2Nlc3MuZW52LlBPUlQpO1xuXHRyZXMuc2VuZChbXCJhcnNlbmFsXCIsIFwiY2hlbHNlYVwiXSk7XG59KTtcblxuYXBwLmdldChcIipcIiwgKHJlcSwgcmVzLCBuZXh0KSA9PiB7XG5cdHJlcy5yZWRpcmVjdChcIi9cIik7XG59KTtcblxuLy8gYXBwLmxpc3Rlbihwcm9jZXNzLmVudi5QT1JULCAoKSA9PiB7XG4vLyBcdGNvbnNvbGUubG9nKFwiIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyNcIik7XG4vLyBcdGNvbnNvbGUubG9nKFwic2VydmVyIHJ1bm5pbmcgb24gcG9ydDogXCIsIHByb2Nlc3MuZW52LlBPUlQpO1xuLy8gXHRjb25zb2xlLmxvZyhcIiMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjXCIpO1xuLy8gfSk7XG5cbmh0dHBzXG5cdC5jcmVhdGVTZXJ2ZXIoXG5cdFx0e1xuXHRcdFx0a2V5OiBmcy5yZWFkRmlsZVN5bmMoXCJsb2NhbGhvc3QtcHJpdmtleS5wZW1cIiksXG5cdFx0XHRjZXJ0OiBmcy5yZWFkRmlsZVN5bmMoXCJsb2NhbGhvc3QtY2VydC5wZW1cIiksXG5cdFx0fSxcblx0XHRhcHBcblx0KVxuXHQubGlzdGVuKHByb2Nlc3MuZW52LlBPUlQsICgpID0+IHtcblx0XHRjb25zb2xlLmxvZyhcIiMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjXCIpO1xuXHRcdGNvbnNvbGUubG9nKFwic2VydmVyIHJ1bm5pbmcgb24gcG9ydDogXCIsIHByb2Nlc3MuZW52LlBPUlQpO1xuXHRcdGNvbnNvbGUubG9nKFwiIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyNcIik7XG5cdH0pO1xuIiwiaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IHsgcmVuZGVyVG9TdHJpbmcsIHJlbmRlclRvTm9kZVN0cmVhbSB9IGZyb20gXCJyZWFjdC1kb20vc2VydmVyXCI7XG5pbXBvcnQgeyBTdGF0aWNSb3V0ZXIgfSBmcm9tIFwicmVhY3Qtcm91dGVyLWRvbVwiO1xuLy8gaW1wb3J0IHsgY3JlYXRlU3RvcmUgfSBmcm9tIFwicmVkdXhcIjtcbi8vIGltcG9ydCB7IFByb3ZpZGVyIH0gZnJvbSBcInJlYWN0LXJlZHV4XCI7XG5cbi8vIGltcG9ydCByZWR1Y2VycyBmcm9tIFwiLi4vLi4vc2hhcmVkL2NvbmZpZ3MvcmVkdWNlcnNcIjtcbmltcG9ydCBBcHAgZnJvbSBcIi4uLy4uL3NoYXJlZC9hcHBcIjtcblxuLy8gaW1wb3J0IHsgQ2h1bmtFeHRyYWN0b3IgfSBmcm9tIFwiQGxvYWRhYmxlL3NlcnZlclwiO1xuLy8gaW1wb3J0IHBhdGggZnJvbSBcInBhdGhcIjtcblxuLy8gaW1wb3J0IHJlbmRlciwgeyBzZW5kIH0gZnJvbSBcIi4uL3Byb2Nlc3Nlcy9yZW5kZXIvcmVuZGVyXCI7XG5cbi8vIGNvbnN0IHN0YXRzRmlsZSA9IHBhdGgucmVzb2x2ZShcbi8vIFx0X19kaXJuYW1lLFxuLy8gXHRcInB1YmxpYy9hc3NldHMvd2ViL2xvYWRhYmxlLXN0YXRzLmpzb25cIlxuLy8gKTtcblxuLy8gY29uc3Qgc3RhdHNGaWxlID0gcGF0aC5yZXNvbHZlKF9fZGlybmFtZSwgXCIuL3dlYi9sb2FkYWJsZS1zdGF0cy5qc29uXCIpO1xuLy8gY29uc29sZS5sb2coXCJzdGF0c0ZpbGVcIiwgc3RhdHNGaWxlKTtcblxuZXhwb3J0IGNvbnN0IGhhbmRsZVJlbmRlciA9IChvcHRpb25zKSA9PiB7XG5cdHJldHVybiBmdW5jdGlvbiAocmVxLCByZXMsIG5leHQpIHtcblx0XHQvLyAqKioqXG5cdFx0Ly8gY29uc3Qgc3RvcmUgPSBjcmVhdGVTdG9yZShyZWR1Y2Vycyk7XG5cdFx0Ly8gY29uc3Qgd2ViRXh0cmFjdG9yID0gbmV3IENodW5rRXh0cmFjdG9yKHtcblx0XHQvLyBcdHN0YXRzRmlsZSxcblx0XHQvLyBcdGVudHJ5cG9pbnRzOiBbXCJpbmRleC5qc1wiXSxcblx0XHQvLyB9KTtcblx0XHQvLyBjb25zdCBqc3ggPSB3ZWJFeHRyYWN0b3IuY29sbGVjdENodW5rcyhcblx0XHQvLyBcdDxQcm92aWRlciBzdG9yZT17c3RvcmV9PlxuXHRcdC8vIFx0XHQ8U3RhdGljUm91dGVyIGxvY2F0aW9uPXtyZXEudXJsfT5cblx0XHQvLyBcdFx0XHQ8QXBwIC8+XG5cdFx0Ly8gXHRcdDwvU3RhdGljUm91dGVyPlxuXHRcdC8vIFx0PC9Qcm92aWRlcj5cblx0XHQvLyApO1xuXHRcdC8vIGNvbnN0IGh0bWwgPSByZW5kZXJUb1N0cmluZyhqc3gpO1xuXHRcdC8vIGNvbnN0IHByZWxvYWRlZFN0YXRlID0gc3RvcmUuZ2V0U3RhdGUoKTtcblx0XHQvLyByZXMubG9jYWxzLmh0bWwgPSByZW5kZXJDb2RlU3BsaXQoaHRtbCwgd2ViRXh0cmFjdG9yLCBwcmVsb2FkZWRTdGF0ZSk7XG5cdFx0Ly8gbmV4dCgpO1xuXHRcdC8vICoqKipcblx0XHQvLyAqKioqXG5cdFx0Ly8gY29uc3Qgc3RvcmUgPSBjcmVhdGVTdG9yZShyZWR1Y2Vycyk7XG5cdFx0Ly8gY29uc3QgaHRtbCA9IHJlbmRlclRvU3RyaW5nKFxuXHRcdC8vIFx0PFByb3ZpZGVyIHN0b3JlPXtzdG9yZX0+XG5cdFx0Ly8gXHRcdDxTdGF0aWNSb3V0ZXIgbG9jYXRpb249e3JlcS51cmx9PlxuXHRcdC8vIFx0XHRcdDxBcHAgLz5cblx0XHQvLyBcdFx0PC9TdGF0aWNSb3V0ZXI+XG5cdFx0Ly8gXHQ8L1Byb3ZpZGVyPlxuXHRcdC8vICk7XG5cdFx0Y29uc3QgaHRtbCA9IHJlbmRlclRvU3RyaW5nKFxuXHRcdFx0PFN0YXRpY1JvdXRlciBsb2NhdGlvbj17cmVxLnVybH0+XG5cdFx0XHRcdDxBcHAgLz5cblx0XHRcdDwvU3RhdGljUm91dGVyPlxuXHRcdCk7XG5cdFx0Ly8gY29uc3QgcHJlbG9hZGVkU3RhdGUgPSBzdG9yZS5nZXRTdGF0ZSgpO1xuXHRcdC8vIHJlcy5sb2NhbHMuaHRtbCA9IHJlbmRlckZ1bGxQYWdlKGh0bWwsIHByZWxvYWRlZFN0YXRlKTtcblx0XHRyZXMubG9jYWxzLmh0bWwgPSByZW5kZXJGdWxsUGFnZShodG1sKTtcblx0XHRuZXh0KCk7XG5cdFx0Ly8gKioqKlxuXHRcdC8vICoqKipcblx0XHQvLyByZW5kZXIuc2VuZCh7IHN0b3JlLCBsb2NhdGlvbjogcmVxLnVybCB9KTtcblx0XHQvLyByZW5kZXIub24oXCJtZXNzYWdlXCIsIChtKSA9PiB7XG5cdFx0Ly8gXHRjb25zdCBodG1sID0gbS5odG1sO1xuXHRcdC8vIFx0Y29uc3QgcHJlbG9hZGVkU3RhdGUgPSBzdG9yZS5nZXRTdGF0ZSgpO1xuXHRcdC8vIFx0cmVzLmxvY2Fscy5odG1sID0gcmVuZGVyRnVsbFBhZ2UoaHRtbCwgcHJlbG9hZGVkU3RhdGUpO1xuXHRcdC8vIFx0bmV4dCgpO1xuXHRcdC8vIH0pO1xuXHRcdC8vICoqKipcblx0fTtcbn07XG5cbmV4cG9ydCBjb25zdCByZW5kZXJGdWxsUGFnZSA9IChodG1sLCBwcmVsb2FkZWRTdGF0ZSkgPT4ge1xuXHRyZXR1cm4gYDwhRE9DVFlQRSBodG1sPlxuICAgICAgPGhlYWQ+XG4gICAgICBcdDwhLS08bWV0YSBodHRwLWVxdWl2PVwiWC1GcmFtZS1PcHRpb25zXCIgY29udGVudD1cImRlbnlcIj4tLT48IS0tIFRoaXMgZG9lc24ndCB3b3JrIC0tPlxuICAgICAgICA8dGl0bGU+UmVhY3QgU1NSPC90aXRsZT5cbiAgICAgICAgPGxpbmsgcmVsPVwic3R5bGVzaGVldFwiIGhyZWY9XCIvYXNzZXRzL2luZGV4LmNzc1wiPlxuICAgICAgICA8c2NyaXB0IGRlZmVyIHNyYz1cIi9hc3NldHMvaW5kZXguanNcIj48L3NjcmlwdD5cbiAgICAgIDwvaGVhZD5cbiAgICAgIDxib2R5PlxuICAgICAgICA8ZGl2IGlkPVwicm9vdFwiPiR7aHRtbH08L2Rpdj5cbiAgICAgICAgPHNjcmlwdD5cbiAgICAgICAgXHR3aW5kb3cuX19QUkVMT0FERURfU1RBVEVfXyA9ICR7SlNPTi5zdHJpbmdpZnkocHJlbG9hZGVkU3RhdGUpfVxuICAgICAgICA8L3NjcmlwdD5cbiAgICAgIDwvYm9keT5cbiAgICA8L2h0bWw+YDtcbn07XG5cbi8vIGV4cG9ydCBjb25zdCByZW5kZXJDb2RlU3BsaXQgPSAoaHRtbCwgd2ViRXh0cmFjdG9yLCBwcmVsb2FkZWRTdGF0ZSkgPT4ge1xuLy8gXHRjb25zb2xlLmxvZyhcIndlYkV4dHJhY3Rvci5nZXRMaW5rVGFnc1wiLCB3ZWJFeHRyYWN0b3IuZ2V0TGlua1RhZ3MoKSk7XG4vLyBcdGNvbnNvbGUubG9nKFwid2ViRXh0cmFjdG9yLmdldFNjcmlwdFRhZ3NcIiwgd2ViRXh0cmFjdG9yLmdldFNjcmlwdFRhZ3MoKSk7XG4vLyBcdHJldHVybiBgPCFET0NUWVBFIGh0bWw+XG4vLyAgICAgICA8aGVhZD5cbi8vICAgICAgICAgPHRpdGxlPlJlYWN0IFNTUjwvdGl0bGU+XG4vLyAgICAgICAgICR7d2ViRXh0cmFjdG9yLmdldExpbmtUYWdzKCl9XG4vLyAgICAgICAgICR7d2ViRXh0cmFjdG9yLmdldFNjcmlwdFRhZ3MoKX1cbi8vICAgICAgIDwvaGVhZD5cbi8vICAgICAgIDxib2R5PlxuLy8gICAgICAgICA8ZGl2IGlkPVwicm9vdFwiPiR7aHRtbH08L2Rpdj5cbi8vICAgICAgICAgPHNjcmlwdD5cbi8vICAgICAgICAgXHR3aW5kb3cuX19QUkVMT0FERURfU1RBVEVfXyA9ICR7SlNPTi5zdHJpbmdpZnkocHJlbG9hZGVkU3RhdGUpfVxuLy8gICAgICAgICA8L3NjcmlwdD5cbi8vICAgICAgIDwvYm9keT5cbi8vICAgICA8L2h0bWw+YDtcbi8vIH07XG4iLCJpbXBvcnQgbW9uZ29vc2UgZnJvbSBcIm1vbmdvb3NlXCI7XG5cbmNvbnN0IFVzZXJTY2hlbWEgPSBtb25nb29zZS5TY2hlbWEoe1xuXHRfaWQ6IHsgdHlwZTogbW9uZ29vc2UuU2NoZW1hLlR5cGVzLk9iamVjdElkLCByZXF1aXJlZDogdHJ1ZSwgYXV0bzogdHJ1ZSB9LFxuXHRlbWFpbDoge1xuXHRcdHR5cGU6IFN0cmluZyxcblx0XHRyZXF1aXJlZDogdHJ1ZSxcblx0XHR1bmlxdWU6IHRydWUsXG5cdH0sXG5cdHBhc3N3b3JkOiB7IHR5cGU6IFN0cmluZywgcmVxdWlyZWQ6IHRydWUgfSxcbn0pO1xuXG5jb25zdCBVc2VyID0gbW9uZ29vc2UubW9kZWwoXCJVc2Vyc1wiLCBVc2VyU2NoZW1hLCBcIlVzZXJzXCIpO1xuXG5leHBvcnQgY29uc3QgY3JlYXRlID0gYXN5bmMgZnVuY3Rpb24gKG9iaikge1xuXHRjb25zdCBlbWFpbCA9IG9iai5lbWFpbDtcblx0Y29uc3QgcGFzc3dvcmQgPSBvYmoucGFzc3dvcmQ7XG5cdGNvbnNvbGUubG9nKFwiVXNlclwiLCB7IC4uLm9iaiwgZW1haWwsIHBhc3N3b3JkIH0pO1xuXHRjb25zdCB1c2VyID0gbmV3IFVzZXIoeyAuLi5vYmosIGVtYWlsLCBwYXNzd29yZCB9KTtcblx0cmV0dXJuIGF3YWl0IHVzZXIuc2F2ZSgpO1xufTtcblxuZXhwb3J0IGNvbnN0IGZpbmQgPSBhc3luYyBmdW5jdGlvbiAob2JqKSB7XG5cdGNvbnN0IGVtYWlsID0gb2JqLmVtYWlsO1xuXHRjb25zdCByZXN1bHQgPSBhd2FpdCBVc2VyLmZpbmQoeyBlbWFpbCB9KS5sZWFuKCk7XG5cdHJldHVybiByZXN1bHQ7XG59O1xuXG5leHBvcnQgY29uc3QgdXBkYXRlQnlFbWFpbCA9IGFzeW5jIGZ1bmN0aW9uIChlbWFpbCwgb2JqKSB7XG5cdGNvbnN0IHJlc3VsdCA9IGF3YWl0IFVzZXIudXBkYXRlKHsgZW1haWwgfSwgb2JqKTtcblx0cmV0dXJuIHJlc3VsdDtcbn07XG5cbmV4cG9ydCBjb25zdCBkZWxldGVCeUVtYWlsID0gYXN5bmMgZnVuY3Rpb24gKGVtYWlsKSB7XG5cdGNvbnN0IHJlc3VsdCA9IGF3YWl0IFVzZXIuZGVsZXRlT25lKHsgZW1haWwgfSk7XG5cdHJldHVybiByZXN1bHQ7XG59O1xuXG5leHBvcnQgZGVmYXVsdCBVc2VyO1xuIiwiaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xuXG5pbXBvcnQgUm91dGVzIGZyb20gXCIuL2NvbmZpZ3Mvcm91dGVzL3JvdXRlc1wiO1xuXG5jb25zdCBBcHAgPSAoKSA9PiB7XG5cdHJldHVybiA8Um91dGVzPjwvUm91dGVzPjtcbn07XG5cbmV4cG9ydCBkZWZhdWx0IEFwcDtcbiIsImltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCB7IFJvdXRlLCBSZWRpcmVjdCB9IGZyb20gXCJyZWFjdC1yb3V0ZXItZG9tXCI7XG5cbmltcG9ydCB7IGlzQWNjZXNzR3JhbnRlZCB9IGZyb20gXCIuLi8uLi91dGlscy9hdXRoVXRpbHNcIjtcblxuZXhwb3J0IGNvbnN0IFByaXZhdGVSb3V0ZSA9ICh7XG5cdGNvbXBvbmVudDogQ29tcG9uZW50LFxuXHRyZWRpcmVjdFBhdGgsXG5cdC4uLnJlc3Rcbn0pID0+IHtcblx0cmV0dXJuIChcblx0XHQ8Um91dGVcblx0XHRcdHsuLi5yZXN0fVxuXHRcdFx0cmVuZGVyPXsocHJvcHMpID0+XG5cdFx0XHRcdGlzQWNjZXNzR3JhbnRlZCgpID8gKFxuXHRcdFx0XHRcdDxDb21wb25lbnQgey4uLnByb3BzfSAvPlxuXHRcdFx0XHQpIDogKFxuXHRcdFx0XHRcdDxSZWRpcmVjdCB0bz17cmVkaXJlY3RQYXRofSAvPlxuXHRcdFx0XHQpXG5cdFx0XHR9XG5cdFx0Lz5cblx0KTtcbn07XG4iLCJpbXBvcnQgUmVhY3QgZnJvbSBcInJlYWN0XCI7XG5pbXBvcnQgeyBSb3V0ZSwgUmVkaXJlY3QgfSBmcm9tIFwicmVhY3Qtcm91dGVyLWRvbVwiO1xuXG5pbXBvcnQgeyBpc0FjY2Vzc0dyYW50ZWQgfSBmcm9tIFwiLi4vLi4vdXRpbHMvYXV0aFV0aWxzXCI7XG5cbmV4cG9ydCBjb25zdCBQdWJsaWNSb3V0ZSA9ICh7XG5cdGNvbXBvbmVudDogQ29tcG9uZW50LFxuXHRyZWRpcmVjdFBhdGgsXG5cdC4uLnJlc3Rcbn0pID0+IHtcblx0cmV0dXJuIDxSb3V0ZSB7Li4ucmVzdH0gcmVuZGVyPXsocHJvcHMpID0+IDxDb21wb25lbnQgey4uLnByb3BzfSAvPn0gLz47XG59O1xuIiwiZXhwb3J0IGNvbnN0IGlzQWNjZXNzR3JhbnRlZCA9ICgpID0+IHtcblx0Ly8gY2hlY2sgYXV0aCBoZWFkZXIgaW4gYmFja2VuZCBkb2luZyBhIGhhY2sgbm93XG5cdGlmICh0eXBlb2YgbG9jYWxTdG9yYWdlID09PSBcInVuZGVmaW5lZFwiKSB7XG5cdFx0cmV0dXJuIHRydWU7XG5cdH1cblxuXHRpZiAobG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJhY2Nlc3NfdG9rZW5cIikpIHtcblx0XHRyZXR1cm4gdHJ1ZTtcblx0fVxuXHRyZXR1cm4gZmFsc2U7XG59O1xuIiwiaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IHsgU3dpdGNoLCBSZWRpcmVjdCB9IGZyb20gXCJyZWFjdC1yb3V0ZXItZG9tXCI7XG5cbmltcG9ydCB7IFByaXZhdGVSb3V0ZSB9IGZyb20gXCIuLi8uLi9jb21tb24vY29tcG9uZW50cy9yb3V0ZXMvUHJpdmF0ZVJvdXRlXCI7XG5pbXBvcnQgeyBQdWJsaWNSb3V0ZSB9IGZyb20gXCIuLi8uLi9jb21tb24vY29tcG9uZW50cy9yb3V0ZXMvUHVibGljUm91dGVcIjtcblxuaW1wb3J0IEhvbWUgZnJvbSBcIi4uLy4uL3BhZ2VzL2hvbWUvaG9tZVwiO1xuaW1wb3J0IFByb2R1Y3RzIGZyb20gXCIuLi8uLi9wYWdlcy9wcm9kdWN0cy9wcm9kdWN0c1wiO1xuaW1wb3J0IFBkZiBmcm9tIFwiLi4vLi4vcGFnZXMvcGRmL3BkZlwiO1xuXG5jb25zdCBSb3V0ZXMgPSAocHJvcHMpID0+IHtcblx0cmV0dXJuIChcblx0XHQ8U3dpdGNoPlxuXHRcdFx0ey8qPFB1YmxpY1JvdXRlIHBhdGg9XCIvaG9tZVwiIGNvbXBvbmVudD17SG9tZX0gcmVkaXJlY3RQYXRoPVwiL2hvbWVcIiAvPlxuXHRcdFx0PFB1YmxpY1JvdXRlXG5cdFx0XHRcdHBhdGg9XCIvcHJvZHVjdHNcIlxuXHRcdFx0XHRjb21wb25lbnQ9e1Byb2R1Y3RzfVxuXHRcdFx0XHRyZWRpcmVjdFBhdGg9XCIvcHJvZHVjdHNcIlxuXHRcdFx0Lz4qL31cblx0XHRcdDxQdWJsaWNSb3V0ZSBwYXRoPVwiL3BkZlwiIGNvbXBvbmVudD17UGRmfSByZWRpcmVjdFBhdGg9XCIvcGRmXCIgLz5cblx0XHRcdDxSZWRpcmVjdCB0bz1cIi9wZGZcIiAvPlxuXHRcdDwvU3dpdGNoPlxuXHQpO1xufTtcblxuZXhwb3J0IGRlZmF1bHQgUm91dGVzO1xuIiwiaW1wb3J0IHsgY3JlYXRlQWN0aW9uLCBjcmVhdGVSZWR1Y2VyLCBjcmVhdGVTZWxlY3RvciB9IGZyb20gXCJAcmVkdXhqcy90b29sa2l0XCI7XG5pbXBvcnQgeyBwdXQsIGFsbCwgdGFrZUxhdGVzdCB9IGZyb20gXCJyZWR1eC1zYWdhL2VmZmVjdHNcIjtcblxuY29uc3QgRE9fU09NRVRISU5HID0gXCJbaG9tZV0gZG8gc29tZXRoaW5nXCI7XG5jb25zdCBTT01FVEhJTkdfRE9ORSA9IFwiW2hvbWVdIHNvbWV0aGluZyBkb25lXCI7XG5cbmNvbnN0IGluaXRpYWxTdGF0ZSA9IHtcblx0bG9hZGluZzogdHJ1ZSxcbn07XG5cbmV4cG9ydCBjb25zdCBzdGF0ZVNlbGVjdG9yID0gKHN0YXRlKSA9PiBzdGF0ZS5ob21lUmVkdWNlcjtcblxuZXhwb3J0IGNvbnN0IGdldExvYWRpbmcgPSBjcmVhdGVTZWxlY3RvcihzdGF0ZVNlbGVjdG9yLCAoc3RhdGUpID0+IHtcblx0Y29uc29sZS5sb2coc3RhdGUpO1xuXHRyZXR1cm4gc3RhdGUubG9hZGluZztcbn0pO1xuXG5leHBvcnQgY29uc3QgaG9tZVJlZHVjZXIgPSBjcmVhdGVSZWR1Y2VyKGluaXRpYWxTdGF0ZSwge1xuXHRbU09NRVRISU5HX0RPTkVdOiAoc3RhdGUsIHsgcGF5bG9hZCB9KSA9PiB7XG5cdFx0c3RhdGUubG9hZGluZyA9IGZhbHNlO1xuXHR9LFxufSk7XG5cbmV4cG9ydCBjb25zdCBkb1NvbWV0aGluZyA9IGNyZWF0ZUFjdGlvbihET19TT01FVEhJTkcpO1xuXG5mdW5jdGlvbiogaW5pdEhvbWUoeyBwYXlsb2FkIH0pIHtcblx0eWllbGQgcHV0KHsgdHlwZTogU09NRVRISU5HX0RPTkUsIHBheWxvYWQ6IHsgbG9hZGluZzogZmFsc2UgfSB9KTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uKiBob21lU2FnYSgpIHtcblx0eWllbGQgYWxsKFt5aWVsZCB0YWtlTGF0ZXN0KERPX1NPTUVUSElORywgaW5pdEhvbWUpXSk7XG59XG4iLCJpbXBvcnQgUmVhY3QsIHsgdXNlRWZmZWN0IH0gZnJvbSBcInJlYWN0XCI7XG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSBcInJlYWN0LXJlZHV4XCI7XG5cbmltcG9ydCB7IGdldExvYWRpbmcsIGRvU29tZXRoaW5nIH0gZnJvbSBcIi4vZHVja3NcIjtcblxuY29uc3QgYVRhZyA9IDxhIGhyZWY9XCJodHRwczovL2xvY2FsaG9zdDozMDAwL2hvbWVcIj5saW5rPC9hPjtcblxuY29uc3QgSG9tZSA9ICh7IGxvYWRpbmcsIGRvU29tZXRoaW5nIH0pID0+IHtcblx0dXNlRWZmZWN0KCgpID0+IHtcblx0XHQvLyBmZXRjaChcImh0dHBzOi8vbGRldi5saW1iZXIuY28uaW46OTAwMC9hcGkvdXNlclwiLCB7XG5cdFx0Ly8gXHRtZXRob2Q6IFwicG9zdFwiLFxuXHRcdC8vIFx0Ly8gd2l0aENyZWRlbnRpYWxzOiB0cnVlLFxuXHRcdC8vIFx0Y3JlZGVudGlhbHM6IFwiaW5jbHVkZVwiLFxuXHRcdC8vIFx0ZGF0YToge1xuXHRcdC8vIFx0XHRhc2Q6IFwiYXNkXCIsXG5cdFx0Ly8gXHR9LFxuXHRcdC8vIFx0aGVhZGVyczoge1xuXHRcdC8vIFx0XHRcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIixcblx0XHQvLyBcdH0sXG5cdFx0Ly8gfSlcblx0XHQvLyBcdC50aGVuKChib2R5KSA9PiB7XG5cdFx0Ly8gXHRcdGNvbnNvbGUubG9nKFwiYm9keVwiLCBib2R5LmhlYWRlcnMuZ2V0KFwic2V0LWNvb2tpZVwiKSk7XG5cdFx0Ly8gXHRcdHJldHVybiBib2R5Lmpzb24oKTtcblx0XHQvLyBcdH0pXG5cdFx0Ly8gXHQudGhlbigocmVzKSA9PiBjb25zb2xlLmxvZyhcInJlc1wiLCByZXMpKTtcblx0XHQvLyBmZXRjaChcImh0dHBzOi8vbGRldi5saW1iZXIuaW46OTAwMS9hcGkvdXNlclwiLCB7XG5cdFx0Ly8gXHRjcmVkZW50aWFsczogXCJpbmNsdWRlXCIsXG5cdFx0Ly8gfSlcblx0XHQvLyBcdC50aGVuKChib2R5KSA9PiB7XG5cdFx0Ly8gXHRcdGNvbnNvbGUubG9nKFwiYm9keVwiLCBib2R5LmhlYWRlcnMuZ2V0KFwic2V0LWNvb2tpZVwiKSk7XG5cdFx0Ly8gXHRcdHJldHVybiBib2R5Lmpzb24oKTtcblx0XHQvLyBcdH0pXG5cdFx0Ly8gXHQudGhlbigocmVzKSA9PiBjb25zb2xlLmxvZyhcInJlc1wiLCByZXMpKTtcblx0XHQvLyBzZXRUaW1lb3V0KCgpID0+IHtcblx0XHQvLyBcdGRvU29tZXRoaW5nKCk7XG5cdFx0Ly8gfSwgMTAwMCk7XG5cdH0sIFtdKTtcblxuXHRjb25zdCBvbkNsaWNrID0gKGV2ZW50KSA9PiB7XG5cdFx0Y29uc29sZS5sb2coXCJvbkNsaWNrXCIsIGV2ZW50KTtcblx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG5cdFx0Ly8gZmV0Y2goXCJodHRwczovL2xvY2FsaG9zdDozMDAwL2FwaS91c2Vyc1wiKVxuXHRcdC8vIFx0LnRoZW4oKGJvZHkpID0+IGJvZHkuanNvbigpKVxuXHRcdC8vIFx0LnRoZW4oKHJlcykgPT4gY29uc29sZS5sb2coXCJyZXNcIiwgcmVzKSk7XG5cblx0XHRmZXRjaChcImh0dHBzOi8vbGRldi5saW1iZXIuY28uaW46OTAwMC9hcGkvdXNlclwiLCB7XG5cdFx0XHRjcmVkZW50aWFsczogXCJpbmNsdWRlXCIsXG5cdFx0XHRtb2RlOiBcImNvcnNcIixcblx0XHR9KVxuXHRcdFx0LnRoZW4oKGJvZHkpID0+IHtcblx0XHRcdFx0Y29uc29sZS5sb2coXCJib2R5XCIsIGJvZHkuaGVhZGVycy5nZXQoXCJzZXQtY29va2llXCIpKTtcblx0XHRcdFx0cmV0dXJuIGJvZHkuanNvbigpO1xuXHRcdFx0fSlcblx0XHRcdC50aGVuKChyZXMpID0+IGNvbnNvbGUubG9nKFwicmVzXCIsIHJlcykpO1xuXHR9O1xuXG5cdGNvbnNvbGUubG9nKFwiKioqKkhvbWUqKioqXCIpO1xuXG5cdHJldHVybiAoXG5cdFx0PGRpdj5cblx0XHRcdDxhIGhyZWY9XCJodHRwczovL2xvY2FsaG9zdDo5MDAwL2hvbWVcIiBvbkNsaWNrPXtvbkNsaWNrfT5cblx0XHRcdFx0bGlua1xuXHRcdFx0PC9hPlxuXHRcdFx0PGJyIC8+XG5cdFx0XHQ8YSBocmVmPVwiaHR0cHM6Ly9sZGV2LmxpbWJlci5pbjo5MDAxL2FwaS91c2VyXCI+bG5rPC9hPlxuXHRcdFx0PGJyIC8+XG5cdFx0XHQ8aWZyYW1lXG5cdFx0XHRcdHNyYz1cImh0dHBzOi8vbGRldi5saW1iZXIuaW46OTAwMS9wcm9kdWN0c1wiXG5cdFx0XHRcdHJlZmVycmVyUG9saWN5PVwic3RyaWN0LW9yaWdpbi13aGVuLWNyb3NzLW9yaWdpblwiXG5cdFx0XHRcdHNhbmRib3g9XCJhbGxvdy1zY3JpcHRzIGFsbG93LW1vZGFsc1wiXG5cdFx0XHQvPlxuXHRcdFx0ey8qPGlmcmFtZSBzcmM9XCJodHRwczovL2RldmVsb3Blci5tb3ppbGxhLm9yZy9lbi1VUy9kb2NzL1dlYi9IVE1ML0VsZW1lbnQvaWZyYW1lXCIgLz4qL31cblx0XHQ8L2Rpdj5cblx0KTtcbn07XG5cbmNvbnN0IGVuaGFuY2UgPSBjb25uZWN0KFxuXHQoc3RhdGUpID0+ICh7XG5cdFx0bG9hZGluZzogZ2V0TG9hZGluZyhzdGF0ZSksXG5cdH0pLFxuXHR7XG5cdFx0ZG9Tb21ldGhpbmcsXG5cdH1cbik7XG5cbmV4cG9ydCBkZWZhdWx0IGVuaGFuY2UoSG9tZSk7XG4iLCJpbXBvcnQgUmVhY3QgZnJvbSBcInJlYWN0XCI7XG4vLyBpbXBvcnQgeyBjb25uZWN0IH0gZnJvbSBcInJlYWN0LXJlZHV4XCI7XG5pbXBvcnQgKiBhcyBkMyBmcm9tIFwiZDNcIjtcblxuZXhwb3J0IGNvbnN0IGdldEJhckNoYXJ0ID0gKHdpZHRoLCBoZWlnaHQpID0+IHtcblx0Y29uc3QgZGF0YSA9IFtcblx0XHR7IGxhYmVsOiBcIkFcIiwgdmFsdWU6IDEwIH0sXG5cdFx0eyBsYWJlbDogXCJCXCIsIHZhbHVlOiA1IH0sXG5cdFx0eyBsYWJlbDogXCJDXCIsIHZhbHVlOiAxMCB9LFxuXHRcdHsgbGFiZWw6IFwiRFwiLCB2YWx1ZTogMjAgfSxcblx0XHR7IGxhYmVsOiBcIkVcIiwgdmFsdWU6IDEwIH0sXG5cdFx0eyBsYWJlbDogXCJGXCIsIHZhbHVlOiA1IH0sXG5cdFx0eyBsYWJlbDogXCJHXCIsIHZhbHVlOiAyMCB9LFxuXHRcdHsgbGFiZWw6IFwiSFwiLCB2YWx1ZTogNyB9LFxuXHRcdHsgbGFiZWw6IFwiSVwiLCB2YWx1ZTogMyB9LFxuXHRcdHsgbGFiZWw6IFwiSlwiLCB2YWx1ZTogMTAgfSxcblx0XTtcblxuXHQvLyBjb25zdCBkMyA9IGdsb2JhbC5kMztcblx0Ly8gY29uc29sZS5sb2coXCJkM1wiLCBnbG9iYWwuZDMpO1xuXHRjb25zdCBtYXJnaW4gPSB7IHRvcDogMjAsIHJpZ2h0OiAyMCwgYm90dG9tOiAzMCwgbGVmdDogNDAgfTtcblx0Y29uc3QgY2hhcnRXaWR0aCA9IHdpZHRoIC0gbWFyZ2luLmxlZnQgLSBtYXJnaW4ucmlnaHQ7XG5cdGNvbnN0IGNoYXJ0SGVpZ2h0ID0gaGVpZ2h0IC0gbWFyZ2luLnRvcCAtIG1hcmdpbi5ib3R0b207XG5cblx0Y29uc3QgeCA9IGQzLnNjYWxlQmFuZCgpLnJhbmdlUm91bmQoWzAsIGNoYXJ0V2lkdGhdKS5wYWRkaW5nSW5uZXIoMC4xKTtcblx0Y29uc3QgeSA9IGQzLnNjYWxlTGluZWFyKCkucmFuZ2VSb3VuZChbY2hhcnRIZWlnaHQsIDBdKTtcblx0Y29uc3QgeiA9IGQzLnNjYWxlT3JkaW5hbCgpLnJhbmdlKFtcIiM5OTI2MDBcIiwgXCIjMDA0ZDAwXCIsIFwiIzAwMzM2NlwiXSk7XG5cdHguZG9tYWluKFxuXHRcdGRhdGEubWFwKGZ1bmN0aW9uIChkKSB7XG5cdFx0XHRyZXR1cm4gZC5sYWJlbDtcblx0XHR9KVxuXHQpO1xuXHR5LmRvbWFpbihbXG5cdFx0MCxcblx0XHRkMy5tYXgoZGF0YSwgZnVuY3Rpb24gKGQpIHtcblx0XHRcdHJldHVybiBkLnZhbHVlO1xuXHRcdH0pLFxuXHRdKTtcblxuXHRjb25zdCBkaXYgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO1xuXHRkaXYuc3R5bGUud2lkdGggPSB3aWR0aCArIFwicHhcIjtcblx0ZGl2LnN0eWxlLmhlaWdodCA9IGhlaWdodCArIFwicHhcIjtcblxuXHRjb25zdCBzdmcgPSBkM1xuXHRcdC5zZWxlY3QoZGl2KVxuXHRcdC5hcHBlbmQoXCJzdmdcIilcblx0XHQuYXR0cihcIndpZHRoXCIsIHdpZHRoKVxuXHRcdC5hdHRyKFwiaGVpZ2h0XCIsIGhlaWdodCk7XG5cdGNvbnN0IGcgPSBzdmdcblx0XHQuYXBwZW5kKFwiZ1wiKVxuXHRcdC5hdHRyKFwidHJhbnNmb3JtXCIsIFwidHJhbnNsYXRlKFwiICsgbWFyZ2luLmxlZnQgKyBcIixcIiArIG1hcmdpbi50b3AgKyBcIilcIilcblx0XHQuYXR0cihcIndpZHRoXCIsIGNoYXJ0V2lkdGgpXG5cdFx0LmF0dHIoXCJoZWlnaHRcIiwgY2hhcnRIZWlnaHQpO1xuXHRnLnNlbGVjdEFsbChcIi5iYXJcIilcblx0XHQuZGF0YShkYXRhKVxuXHRcdC5lbnRlcigpXG5cdFx0LmFwcGVuZChcInJlY3RcIilcblx0XHQuYXR0cihcImNsYXNzXCIsIFwiYmFyXCIpXG5cdFx0LmF0dHIoXCJ4XCIsIGZ1bmN0aW9uIChkKSB7XG5cdFx0XHRyZXR1cm4geChkLmxhYmVsKTtcblx0XHR9KVxuXHRcdC5hdHRyKFwid2lkdGhcIiwgeC5iYW5kd2lkdGgoKSlcblx0XHQuYXR0cihcInlcIiwgZnVuY3Rpb24gKGQpIHtcblx0XHRcdHJldHVybiB5KGQudmFsdWUpO1xuXHRcdH0pXG5cdFx0LmF0dHIoXCJoZWlnaHRcIiwgZnVuY3Rpb24gKGQpIHtcblx0XHRcdHJldHVybiBjaGFydEhlaWdodCAtIHkoZC52YWx1ZSk7XG5cdFx0fSlcblx0XHQuYXR0cihcImZpbGxcIiwgZnVuY3Rpb24gKGQpIHtcblx0XHRcdHJldHVybiB6KGQubGFiZWwpO1xuXHRcdH0pO1xuXG5cdGcuYXBwZW5kKFwiZ1wiKVxuXHRcdC5hdHRyKFwidHJhbnNmb3JtXCIsIFwidHJhbnNsYXRlKDAsXCIgKyBjaGFydEhlaWdodCArIFwiKVwiKVxuXHRcdC5jYWxsKGQzLmF4aXNCb3R0b20oeCkpO1xuXG5cdGcuYXBwZW5kKFwiZ1wiKS5jYWxsKGQzLmF4aXNMZWZ0KHkpKTtcblxuXHRyZXR1cm4gZGl2LmlubmVySFRNTDtcbn07XG5cbmZ1bmN0aW9uIGNyZWF0ZU1hcmt1cChkYXRhKSB7XG5cdHJldHVybiB7IF9faHRtbDogZGF0YSB9O1xufVxuXG5jbGFzcyBQZGYgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuXHRjb25zdHJ1Y3Rvcihwcm9wcykge1xuXHRcdHN1cGVyKHByb3BzKTtcblx0fVxuXG5cdHJlbmRlcigpIHtcblx0XHRjb25zdCBodG1sID0gZ2V0QmFyQ2hhcnQoNTAwLCA1MDApO1xuXHRcdHJldHVybiA8ZGl2IGRhbmdlcm91c2x5U2V0SW5uZXJIVE1MPXtjcmVhdGVNYXJrdXAoaHRtbCl9PjwvZGl2Pjtcblx0fVxufVxuXG5leHBvcnQgZGVmYXVsdCBQZGY7XG4iLCJpbXBvcnQgeyBjcmVhdGVBY3Rpb24sIGNyZWF0ZVJlZHVjZXIsIGNyZWF0ZVNlbGVjdG9yIH0gZnJvbSBcIkByZWR1eGpzL3Rvb2xraXRcIjtcbmltcG9ydCB7IHB1dCwgYWxsLCB0YWtlTGF0ZXN0IH0gZnJvbSBcInJlZHV4LXNhZ2EvZWZmZWN0c1wiO1xuXG5jb25zdCBET19TT01FVEhJTkcgPSBcIltwcm9kdWN0c10gZG8gc29tZXRoaW5nXCI7XG5jb25zdCBTT01FVEhJTkdfRE9ORSA9IFwiW3Byb2R1Y3RzXSBzb21ldGhpbmcgZG9uZVwiO1xuXG5jb25zdCBpbml0aWFsU3RhdGUgPSB7XG5cdGxvYWRpbmc6IHRydWUsXG59O1xuXG5leHBvcnQgY29uc3Qgc3RhdGVTZWxlY3RvciA9IChzdGF0ZSkgPT4gc3RhdGUucHJvZHVjdHNSZWR1Y2VyO1xuXG5leHBvcnQgY29uc3QgZ2V0TG9hZGluZyA9IGNyZWF0ZVNlbGVjdG9yKHN0YXRlU2VsZWN0b3IsIChzdGF0ZSkgPT4ge1xuXHRjb25zb2xlLmxvZyhzdGF0ZSk7XG5cdHJldHVybiBzdGF0ZS5sb2FkaW5nO1xufSk7XG5cbmV4cG9ydCBjb25zdCBwcm9kdWN0c1JlZHVjZXIgPSBjcmVhdGVSZWR1Y2VyKGluaXRpYWxTdGF0ZSwge1xuXHRbU09NRVRISU5HX0RPTkVdOiAoc3RhdGUsIHsgcGF5bG9hZCB9KSA9PiB7XG5cdFx0c3RhdGUubG9hZGluZyA9IGZhbHNlO1xuXHR9LFxufSk7XG5cbmV4cG9ydCBjb25zdCBkb1NvbWV0aGluZyA9IGNyZWF0ZUFjdGlvbihET19TT01FVEhJTkcpO1xuXG5mdW5jdGlvbiogaW5pdFByb2R1Y3RzKHsgcGF5bG9hZCB9KSB7XG5cdHlpZWxkIHB1dCh7IHR5cGU6IFNPTUVUSElOR19ET05FLCBwYXlsb2FkOiB7IGxvYWRpbmc6IGZhbHNlIH0gfSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiogcHJvZHVjdHNTYWdhKCkge1xuXHR5aWVsZCBhbGwoW3lpZWxkIHRha2VMYXRlc3QoRE9fU09NRVRISU5HLCBpbml0UHJvZHVjdHMpXSk7XG59XG4iLCJpbXBvcnQgUmVhY3QsIHsgdXNlRWZmZWN0IH0gZnJvbSBcInJlYWN0XCI7XG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSBcInJlYWN0LXJlZHV4XCI7XG5cbmltcG9ydCB7IGdldExvYWRpbmcsIGRvU29tZXRoaW5nIH0gZnJvbSBcIi4vZHVja3NcIjtcblxuY29uc3QgYVRhZyA9IDxhIGhyZWY9XCJodHRwczovL2xvY2FsaG9zdDozMDAwL2hvbWVcIj5saW5rPC9hPjtcblxuY29uc3QgUHJvZHVjdHMgPSAoeyBsb2FkaW5nLCBkb1NvbWV0aGluZyB9KSA9PiB7XG5cdHVzZUVmZmVjdCgoKSA9PiB7XG5cdFx0Ly8gZmV0Y2goXCJodHRwczovL2xkZXYubGltYmVyLmNvLmluOjkwMDEvYXBpL3VzZXJcIiwge1xuXHRcdC8vIFx0bWV0aG9kOiBcInBvc3RcIixcblx0XHQvLyBcdC8vIHdpdGhDcmVkZW50aWFsczogdHJ1ZSxcblx0XHQvLyBcdGNyZWRlbnRpYWxzOiBcImluY2x1ZGVcIixcblx0XHQvLyBcdGRhdGE6IHtcblx0XHQvLyBcdFx0YXNkOiBcImFzZFwiLFxuXHRcdC8vIFx0fSxcblx0XHQvLyBcdGhlYWRlcnM6IHtcblx0XHQvLyBcdFx0XCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCIsXG5cdFx0Ly8gXHR9LFxuXHRcdC8vIH0pXG5cdFx0Ly8gXHQudGhlbigoYm9keSkgPT4ge1xuXHRcdC8vIFx0XHRjb25zb2xlLmxvZyhcImJvZHlcIiwgYm9keS5oZWFkZXJzLmdldChcInNldC1jb29raWVcIikpO1xuXHRcdC8vIFx0XHRyZXR1cm4gYm9keS5qc29uKCk7XG5cdFx0Ly8gXHR9KVxuXHRcdC8vIFx0LnRoZW4oKHJlcykgPT4gY29uc29sZS5sb2coXCJyZXNcIiwgcmVzKSk7XG5cdFx0Ly8gZmV0Y2goXCJodHRwczovL2xkZXYubGltYmVyLmluOjkwMDEvYXBpL3VzZXJcIiwge1xuXHRcdC8vIFx0Y3JlZGVudGlhbHM6IFwiaW5jbHVkZVwiLFxuXHRcdC8vIH0pXG5cdFx0Ly8gXHQudGhlbigoYm9keSkgPT4ge1xuXHRcdC8vIFx0XHRjb25zb2xlLmxvZyhcImJvZHlcIiwgYm9keS5oZWFkZXJzLmdldChcInNldC1jb29raWVcIikpO1xuXHRcdC8vIFx0XHRyZXR1cm4gYm9keS5qc29uKCk7XG5cdFx0Ly8gXHR9KVxuXHRcdC8vIFx0LnRoZW4oKHJlcykgPT4gY29uc29sZS5sb2coXCJyZXNcIiwgcmVzKSk7XG5cdFx0Ly8gc2V0VGltZW91dCgoKSA9PiB7XG5cdFx0Ly8gXHRkb1NvbWV0aGluZygpO1xuXHRcdC8vIH0sIDEwMDApO1xuXHR9LCBbXSk7XG5cblx0Y29uc3Qgb25DbGljayA9IChldmVudCkgPT4ge1xuXHRcdGNvbnNvbGUubG9nKFwib25DbGlja1wiLCBldmVudCk7XG5cdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcblxuXHRcdC8vIGZldGNoKFwiaHR0cHM6Ly9sb2NhbGhvc3Q6MzAwMC9hcGkvdXNlcnNcIilcblx0XHQvLyBcdC50aGVuKChib2R5KSA9PiBib2R5Lmpzb24oKSlcblx0XHQvLyBcdC50aGVuKChyZXMpID0+IGNvbnNvbGUubG9nKFwicmVzXCIsIHJlcykpO1xuXG5cdFx0ZmV0Y2goXCJodHRwczovL2xkZXYubGltYmVyLmluOjkwMDEvYXBpL3VzZXJcIiwge1xuXHRcdFx0Ly8gY3JlZGVudGlhbHM6IFwiaW5jbHVkZVwiLFxuXHRcdH0pXG5cdFx0XHQudGhlbigoYm9keSkgPT4ge1xuXHRcdFx0XHRjb25zb2xlLmxvZyhcImJvZHlcIiwgYm9keS5oZWFkZXJzLmdldChcInNldC1jb29raWVcIikpO1xuXHRcdFx0XHRyZXR1cm4gYm9keS5qc29uKCk7XG5cdFx0XHR9KVxuXHRcdFx0LnRoZW4oKHJlcykgPT4gY29uc29sZS5sb2coXCJyZXNcIiwgcmVzKSk7XG5cblx0XHRhbGVydChcIkhlbGxvXCIpO1xuXHR9O1xuXG5cdGNvbnNvbGUubG9nKFwiKioqKlByb2R1Y3RzKioqKlwiKTtcblxuXHRyZXR1cm4gKFxuXHRcdDxkaXY+XG5cdFx0XHQ8YSBocmVmPVwiaHR0cHM6Ly9sb2NhbGhvc3Q6OTAwMC9ob21lXCIgb25DbGljaz17b25DbGlja30+XG5cdFx0XHRcdGxpbmtcblx0XHRcdDwvYT5cblx0XHRcdDxiciAvPlxuXHRcdFx0PGEgaHJlZj1cImh0dHBzOi8vbGRldi5saW1iZXIuaW46OTAwMS9hcGkvdXNlclwiPmxuazwvYT5cblx0XHRcdDxiciAvPlxuXHRcdFx0ey8qPGlmcmFtZSBzcmM9XCJodHRwczovL2RldmVsb3Blci5tb3ppbGxhLm9yZy9lbi1VUy9kb2NzL1dlYi9IVE1ML0VsZW1lbnQvaWZyYW1lXCIgLz4qL31cblx0XHQ8L2Rpdj5cblx0KTtcbn07XG5cbmNvbnN0IGVuaGFuY2UgPSBjb25uZWN0KFxuXHQoc3RhdGUpID0+ICh7XG5cdFx0bG9hZGluZzogZ2V0TG9hZGluZyhzdGF0ZSksXG5cdH0pLFxuXHR7XG5cdFx0ZG9Tb21ldGhpbmcsXG5cdH1cbik7XG5cbmV4cG9ydCBkZWZhdWx0IGVuaGFuY2UoUHJvZHVjdHMpO1xuIiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiQHJlZHV4anMvdG9vbGtpdFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJib2R5LXBhcnNlclwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJjb29raWUtcGFyc2VyXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImQzXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImV4cHJlc3NcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZnNcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiaHRtbC1wZGZcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiaHR0cHNcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwianNkb21cIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwibWVtY2FjaGVkXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIm1vbmdvb3NlXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0LWRvbS9zZXJ2ZXJcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QtcmVkdXhcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3Qtcm91dGVyLWRvbVwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWR1eC1zYWdhL2VmZmVjdHNcIik7Il0sInNvdXJjZVJvb3QiOiIifQ==