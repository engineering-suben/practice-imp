import { all } from "redux-saga/effects";

import { homeSaga } from "../pages/home/ducks";
import { productsSaga } from "../pages/products/ducks";

export default function* () {
	yield all([
		// exampleSaga(),
		homeSaga(),
		productsSaga(),
	]);
}
