import cp from "child_process";
// const cp = require("child_process");

const render = cp.fork("./renderCore.js");

render.on("message", (m) => {
	console.log("message received");
});

render.on("error", (m) => {
	console.log("render process error", m);
});

export const send = (obj) => {
	render.send(obj);
};

export default render;
