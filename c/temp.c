#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>

pthread_mutex_t lock;
long count = 0;

void *createThread (void *vargp) {
  pthread_mutex_lock(&lock);
  long count = 0;
  long pCount = 0;
  long n = 1000000000;
  while (count != n) {
    if (count % 25000000 == 0) {
      printf("count 25: %ld, thread ID: %ld\n", count, pthread_self());
      count = count + 25000000;
    }

    count++;
    // pthread_mutex_lock(&lock);
    if (count % 25000000 == 0) {
      pCount++;
      printf("%ld - Printing From pthread - %ld, Thread ID - %ld: %ld\n", pCount, *(long*)vargp, pthread_self(), count);
      

      // if (pCount == 2) {
        // printf("Cancel thread %ld\n", pthread_self());
        
        // pthread_kill(pthread_self(), SIGKILL);
        // pthread_cancel(pthread_self());
      // }
      // printf("Printing complete %ld\n", pthread_self());
    }
    // pthread_mutex_unlock(&lock);
  }
  
  pthread_mutex_unlock(&lock);

  return NULL;
}

int main () {
  // pthread_t thread_1 = 0, thread_2 = 0;
  
  // pthread_mutex_init(&lock, NULL);

  // printf("Before Thread\n");
  // printf("thread_1: %ld, thread_2: %ld\n", thread_1, thread_2);
  
  // pthread_create(&thread_1, NULL, createThread, &thread_1);
  // pthread_create(&thread_2, NULL, createThread, &thread_2);
  
  // printf("thread_1: %ld, thread_2: %ld\n", thread_1, thread_2);
  
  // pthread_join(thread_1, NULL);
  // pthread_join(thread_2, NULL);
  
  // printf("After Thread\n");

  // pthread_mutex_destroy(&lock);
  exit(0);
}
