#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

pthread_mutex_t lock;

// char c[1000];
int len = 0;
// FILE *fptr;

void *createThread(void *vargp) {
  pthread_mutex_lock(&lock);

  printf("thread ID: %ld, vargp: %s\n", pthread_self(), (char *)vargp);

  char c[1000];
  // static int len = 0;
  FILE *fptr;

  pthread_mutex_unlock(&lock);

  // printf("opening file thread ID: %ld\n", pthread_self());
  if ((fptr = fopen((char *)vargp, "r")) == NULL) {
    printf("error opening file");
    return NULL;
  }

  // printf("scanning file thread ID: %ld\n", pthread_self());
  fscanf(fptr, "%[^\n]", c);
  len = strlen(c);

  printf("readning and scanning done threadID: %ld\n", pthread_self());

  pthread_mutex_lock(&lock);

  printf("data from threadID %ld: %s, %d\n", pthread_self(), c, len);

  fclose(fptr);

  pthread_mutex_unlock(&lock);

  return NULL;
}

int main() {
  pthread_t thread_1 = 0, thread_2 = 0;

  pthread_mutex_init(&lock, NULL);

  printf("Before Thread\n");
  // printf("thread_1: %ld, thread_2: %ld\n", thread_1, thread_2);

  char s1[9] = {'f', 'i', 'l', 'e', '.', 't', 'x', 't', '\0'};
  char s2[10] = {'f', 'i', 'l', 'e', '1', '.', 't', 'x', 't', '\0'};

  pthread_create(&thread_1, NULL, createThread, &s1);
  pthread_create(&thread_2, NULL, createThread, &s2);

  // printf("thread_1: %ld, thread_2: %ld\n", thread_1, thread_2);

  pthread_join(thread_1, NULL);
  pthread_join(thread_2, NULL);

  printf("After Thread\n");

  pthread_mutex_destroy(&lock);
  exit(0);
}
