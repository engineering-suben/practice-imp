import React from "react";
import PropTypes from "prop-types";

class Products extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      text: "Products",
    };
  }
  render() {
    return <div>{this.state.text}</div>;
  }
}

// Products.propTypes = {
//   test: PropTypes.arrayOf(
//     PropTypes.exact({
//       a: PropTypes.string,
//       b: PropTypes.string,
//     })
//   ).isRequired,
//   test1: PropTypes.shape({
//     color: PropTypes.string,
//   }),
// };

export default Products;
