import React from "react";
import PropTypes from "prop-types";

import Products from "../products/products";

class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      text: "Home",
    };
  }
  render() {
    console.log(this.props.comp);
    return <div>{this.state.text}</div>;
  }
}

Home.propTypes = {
  test: PropTypes.arrayOf(
    PropTypes.exact({
      a: PropTypes.string,
      b: PropTypes.string,
      c: PropTypes.string,
    })
  ).isRequired,
  test1: PropTypes.shape({
    color: PropTypes.string.isRequired,
  }),
  comp: PropTypes.node,
  elem: PropTypes.instanceOf(HTMLElement),
  oneof: PropTypes.oneOf(["a", "b"]),
  oneoftype: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.object,
    PropTypes.string,
  ]),
  objectof: PropTypes.objectOf(PropTypes.number),
  customarrayof: PropTypes.arrayOf(function (val, key) {
    // console.log("val", val);
    // console.log("key", key);
    // throw "error";
    // return null;
    // return new Error("error");
  }),
  customobjectof: PropTypes.objectOf(function (val, key) {
    // console.log("val", val);
    // console.log("key", key);
    // return new Error("error");
  }),
  custom: function (props, propName, componentName) {
    // console.log(props);
    // console.log(propName);
    // console.log(componentName);
    // return new Error("error");
  },
};

export default Home;
