import React from "react";
import logo from "./logo.svg";
import "./App.css";

import Home from "./pages/home/home";
import Products from "./pages/products/products";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <Home
          test={[{ a: "a", b: "b" }]}
          test1={{ a: "asd", b: "vfe", color: "red" }}
          comp={<Products />}
          elem={document.getElementById("elem")}
          oneof={"a"}
          oneoftype={"c"}
          objectof={{ a: 1 }}
          customarrayof={[1, 2]}
          customobjectof={{ a: "a", b: "b" }}
          custom={{ a: "a" }}
        />
      </header>
    </div>
  );
}

export default App;
