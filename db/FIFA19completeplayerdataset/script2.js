const MongoClient = require("mongodb").MongoClient;
const fs = require("fs");
const readline = require("readline");

const url =
	"mongodb+srv://";

const dbName = "fifa19";

MongoClient.connect(url, { keepAlive: 1, connectTimeoutMS: 30000 }, function (
	err,
	client
) {
	if (err) {
		console.log(err);
	}

	console.log("***Connected successfully to server***");

	const db = client.db(dbName);

	onConnected(db, client);

	// client.close();
});

const cols = {
	ID: true,
	Age: true,
	Overall: true,
	Potential: true,

	//
	Value: true,
	//
	Wage: true,

	Special: true,

	"International Reputation": true,
	"Weak Foot": true,
	"Skill Moves": true,

	"Jersey Number": true,

	//
	Weight: true,

	Crossing: true,
	Finishing: true,
	HeadingAccuracy: true,
	ShortPassing: true,
	Volleys: true,
	Dribbling: true,
	Curve: true,
	FKAccuracy: true,
	LongPassing: true,
	BallControl: true,
	Acceleration: true,
	SprintSpeed: true,
	Agility: true,
	Reactions: true,
	Balance: true,
	ShotPower: true,
	Jumping: true,
	Stamina: true,
	Strength: true,
	LongShots: true,
	Aggression: true,
	Interceptions: true,
	Positioning: true,
	Vision: true,
	Penalties: true,
	Composure: true,
	Marking: true,
	StandingTackle: true,
	SlidingTackle: true,
	GKDiving: true,
	GKHandling: true,
	GKKicking: true,
	GKPositioning: true,
	GKReflexes: true,
	//
	"Release Clause": true,
};

const getData = function (db, client) {
	let columns;

	let iter = 0;

	const file = fs.readFileSync("csvjson.json");
	let fileStr = file.toString();
	fileStr = JSON.parse(fileStr);

	for (let item of fileStr) {
		delete item[""];

		for (key in item) {
			let val = item[key];

			if (cols[key]) {
				if (key === "Value" || key === "Wage" || key === "Release Clause") {
					val = parseFloat(item[key].substring(1, item[key].length - 1));
				} else if (key === "Weight") {
					val = parseFloat(item[key].substring(0, item[key].length - 3));
				} else {
					val = parseFloat(item[key]);
				}
			}

			if (val !== NaN) {
				item[key] = val;
			}
		}
	}

	const collection = db.collection("fifa19edit");
	collection.insertMany(fileStr, function (err, result) {
		if (err) {
			console.error(err);
		}
		console.log(result);
		client.close();
	});
};

const onConnected = function (db, client) {
	getData(db, client);
};
