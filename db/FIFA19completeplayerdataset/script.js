const MongoClient = require("mongodb").MongoClient;
const fs = require("fs");
const readline = require("readline");

const url =
	"mongodb+srv://";

const dbName = "fifa19";

MongoClient.connect(url, { keepAlive: 1, connectTimeoutMS: 30000 }, function (
	err,
	client
) {
	if (err) {
		console.log(err);
	}

	console.log("***Connected successfully to server***");

	const db = client.db(dbName);

	onConnected(db, client);

	// client.close();
});

const cols = {
	ID: true,
	Age: true,
	Overall: true,
	Potential: true,

	//
	Value: true,
	//
	Wage: true,

	Special: true,

	"International Reputation": true,
	"Weak Foot": true,
	"Skill Moves": true,

	"Jersey Number": true,

	//
	Weight: true,

	Crossing: true,
	Finishing: true,
	HeadingAccuracy: true,
	ShortPassing: true,
	Volleys: true,
	Dribbling: true,
	Curve: true,
	FKAccuracy: true,
	LongPassing: true,
	BallControl: true,
	Acceleration: true,
	SprintSpeed: true,
	Agility: true,
	Reactions: true,
	Balance: true,
	ShotPower: true,
	Jumping: true,
	Stamina: true,
	Strength: true,
	LongShots: true,
	Aggression: true,
	Interceptions: true,
	Positioning: true,
	Vision: true,
	Penalties: true,
	Composure: true,
	Marking: true,
	StandingTackle: true,
	SlidingTackle: true,
	GKDiving: true,
	GKHandling: true,
	GKKicking: true,
	GKPositioning: true,
	GKReflexes: true,
	//
	"Release Clause": true,
};

const rows = [];

const getData = function (db, client) {
	let columns;

	let iter = 0;

	const file = readline.createInterface({
		input: fs.createReadStream("data.csv"),
		output: process.stdout,
		terminal: false,
	});

	file.on("line", (line) => {
		if (iter === 0) {
			console.log(line);
			columns = line.split(",");
			columns.shift();
			console.log(columns);

			iter++;
		} else {
			const row = line.split(",");
			row.shift();
			let _row = {};

			for (let i = 0; i < columns.length; i++) {
				let val = row[i];
				if (cols[columns[i]]) {
					if (
						columns[i] === "Value" ||
						columns[i] === "Wage" ||
						columns[i] === "Release Clause"
					) {
						val = parseFloat(row[i].substring(1, row[i].length - 1));
					} else if (columns[i] === "Weight") {
						val = parseFloat(row[i].substring(0, row[i].length - 3));
					} else {
						val = parseFloat(row[i]);
					}
				}

				if (val === NaN) {
					val = row[i];
				}
				_row[columns[i]] = val;
			}

			rows.push(_row);

			file.close();
		}
	});

	file.on("close", (line) => {
		console.log("close", rows);

		// const collection = db.collection("fifa19");

		// collection.find({}, function (err, res) {
		// 	console.log(res);
		// });

		// collection.insertMany(rows, function (err, result) {
		// 	if (err) {
		// 		console.error(err);
		// 	}
		// 	console.log(result);
		// 	client.close();
		// });
	});
};

const onConnected = function (db, client) {
	getData(db, client);
};
