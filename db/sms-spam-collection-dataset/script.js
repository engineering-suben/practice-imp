const MongoClient = require("mongodb").MongoClient;

const url =
	"mongodb+srv://";

MongoClient.connect(url, function (err, client) {
	assert.equal(null, err);
	console.log("Connected successfully to server");

	const db = client.db();

	console.log(db.find({}));

	client.close();
});
