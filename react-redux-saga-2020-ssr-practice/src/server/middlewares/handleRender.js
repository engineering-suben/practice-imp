import React from "react";
import { renderToString, renderToNodeStream } from "react-dom/server";
import { StaticRouter } from "react-router-dom";
import { createStore } from "redux";
import { Provider } from "react-redux";

import reducers from "../../shared/configs/reducers";
import App from "../../shared/app";

// import { ChunkExtractor } from "@loadable/server";
// import path from "path";

// import render, { send } from "../processes/render/render";

// const statsFile = path.resolve(
// 	__dirname,
// 	"public/assets/web/loadable-stats.json"
// );

// const statsFile = path.resolve(__dirname, "./web/loadable-stats.json");
// console.log("statsFile", statsFile);

export const handleRender = (options) => {
	return function (req, res, next) {
		// ****
		// const store = createStore(reducers);
		// const webExtractor = new ChunkExtractor({
		// 	statsFile,
		// 	entrypoints: ["index.js"],
		// });
		// const jsx = webExtractor.collectChunks(
		// 	<Provider store={store}>
		// 		<StaticRouter location={req.url}>
		// 			<App />
		// 		</StaticRouter>
		// 	</Provider>
		// );
		// const html = renderToString(jsx);
		// const preloadedState = store.getState();
		// res.locals.html = renderCodeSplit(html, webExtractor, preloadedState);
		// next();
		// ****
		// ****
		const store = createStore(reducers);
		const html = renderToString(
			<Provider store={store}>
				<StaticRouter location={req.url}>
					<App />
				</StaticRouter>
			</Provider>
		);
		const preloadedState = store.getState();
		res.locals.html = renderFullPage(html, preloadedState);
		next();
		// ****
		// ****
		// render.send({ store, location: req.url });
		// render.on("message", (m) => {
		// 	const html = m.html;
		// 	const preloadedState = store.getState();
		// 	res.locals.html = renderFullPage(html, preloadedState);
		// 	next();
		// });
		// ****
	};
};

export const renderFullPage = (html, preloadedState) => {
	return `<!DOCTYPE html>
      <head>
      	<!--<meta http-equiv="X-Frame-Options" content="deny">--><!-- This doesn't work -->
        <title>React SSR</title>
        <link rel="stylesheet" href="/assets/index.css">
        <script defer src="/assets/index.js"></script>
      </head>
      <body>
        <div id="root">${html}</div>
        <script>
        	window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState)}
        </script>
      </body>
    </html>`;
};

// export const renderCodeSplit = (html, webExtractor, preloadedState) => {
// 	console.log("webExtractor.getLinkTags", webExtractor.getLinkTags());
// 	console.log("webExtractor.getScriptTags", webExtractor.getScriptTags());
// 	return `<!DOCTYPE html>
//       <head>
//         <title>React SSR</title>
//         ${webExtractor.getLinkTags()}
//         ${webExtractor.getScriptTags()}
//       </head>
//       <body>
//         <div id="root">${html}</div>
//         <script>
//         	window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState)}
//         </script>
//       </body>
//     </html>`;
// };
