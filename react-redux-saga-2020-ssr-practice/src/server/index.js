import https from "https";
import fs from "fs";
import express from "express";
import mongoose from "mongoose";
import Memcached from "memcached";
// import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import { handleRender } from "./middlewares/handleRender";

import userRouter from "./controllers/userController";

// mongoose.connect(
// 	"mongodb://localhost:27017/test1",
// 	{
// 		useNewUrlParser: true,
// 		useUnifiedTopology: true,
// 	},
// 	(err) => {
// 		if (err) {
// 			console.error("Mongo DB Connection Error!");
// 			console.error(err);
// 		} else {
// 			console.log("Mongo DB Connected");
// 		}
// 	}
// );

const memcached = new Memcached("127.0.0.1:11211");

const app = express();
app.use(express.static("public"));
app.use(cookieParser());

const publicRouter = express.Router();
const apiRouter = express.Router();

app.use("/", publicRouter);
app.use("/home", publicRouter);
app.use("/api", apiRouter);

publicRouter.use(handleRender());

publicRouter.get("/", (req, res) => {
	memcached.set("sth", "asd", 300, function (err, data) {
		console.log("/ err, data", err, data);
	});

	// res.cookie("access-token", process.env.PORT, {
	// 	expires: new Date(Date.now() + 12000),
	// });
	console.log("/");
	console.log("req.Referer", req.get("Referrer"));
	// console.log("req.cookies", req.headers.cookie);
	res.header("Access-Control-Allow-Credentials", true);
	// res.header("Access-Control-Allow-Origin", "https://ldev.limber.co.in:9000");
	res.cookie("access-token", process.env.PORT, { httpOnly: true });
	res.cookie("random", "random");
	res.send(res.locals.html);
});

publicRouter.get("/home", (req, res) => {
	memcached.get("sth", function (err, data) {
		console.log("/home err, data", err, data);
	});

	console.log("/home");
	console.log("req.Referer", req.get("Referrer"));
	console.log("req.Origin", req.get("Origin"));
	console.log("req.cookies", req.headers.cookie);
	console.log("req.cookies", req.cookies);
	// res.header(
	// 	"Content-Security-Policy",
	// 	"frame-ancestors 'self'; connect-src 'self';"
	// );
	// res.cookie("access-token", process.env.PORT);
	res.send(res.locals.html);
});

publicRouter.get("/products", (req, res) => {
	console.log("/products");
	console.log("req.Referer", req.get("Referrer"));
	console.log("req.cookies", req.headers.cookie);
	// res.header("X-Frame-Options", "deny");
	// res.header(
	// 	"Content-Security-Policy",
	// 	"frame-ancestors 'self' https://ldev.limber.co.in:9000 https://localhost:9000; connect-src 'self'"
	// );
	// res.header("Content-Security-Policy", "connect-src 'self';");
	// res.cookie("access-token", process.env.PORT);
	res.send(res.locals.html);
});

apiRouter.use((req, res, next) => {
	console.log("apiRouter", req.get("Origin"));
	// res.header("Access-Control-Allow-Origin", req.get("Origin"));
	next();
});

apiRouter.use("/users", userRouter);

apiRouter.options("/user", (req, res) => {
	console.log("****HERE****");
	// res.header("Access-Control-Allow-Credentials", true);
	res.header("Access-Control-Allow-Headers", "Content-Type");
	res.send();
});

apiRouter.get("/user", (req, res) => {
	// res.cookie("access-token", process.env.PORT, {
	// 	expires: new Date(Date.now() + 12000),
	// });
	console.log("/user");
	console.log("req.Referer", req.get("Referrer"));
	console.log("req.cookies", req.headers.cookie);
	// res.set("Cache-Control", "private, max-age=30");
	// res.set("Cache-Control", "private");
	res.header("Access-Control-Allow-Credentials", true);
	res
		.cookie("access-token", process.env.PORT, {
			expires: new Date(Date.now() + 2 * 60 * 1000),
			sameSite: "None",
			secure: true,
		})
		.cookie("somestring", "somerandomstring", {
			expires: new Date(Date.now() + 2 * 60 * 1000),
			sameSite: "None",
			secure: true,
		});
	res.send(["arsenal", "chelsea", "manu", 4]);
	console.log("sent");
});

apiRouter.post("/user", (req, res) => {
	// res.cookie("access-token", process.env.PORT, {
	// 	expires: new Date(Date.now() + 12000),
	// });
	console.log("/user");
	console.log("req.Referer", req.get("Referrer"));
	console.log("req.cookies", req.headers.cookie);
	res.header("Access-Control-Allow-Credentials", true);
	res.cookie("access-token", process.env.PORT);
	res.send(["arsenal", "chelsea"]);
});

app.get("*", (req, res, next) => {
	res.redirect("/");
});

// app.listen(process.env.PORT, () => {
// 	console.log("#############################");
// 	console.log("server running on port: ", process.env.PORT);
// 	console.log("#############################");
// });

https
	.createServer(
		{
			key: fs.readFileSync("localhost-privkey.pem"),
			cert: fs.readFileSync("localhost-cert.pem"),
		},
		app
	)
	.listen(process.env.PORT, () => {
		console.log("#############################");
		console.log("server running on port: ", process.env.PORT);
		console.log("#############################");
	});
