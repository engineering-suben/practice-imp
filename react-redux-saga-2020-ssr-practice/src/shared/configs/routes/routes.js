import React from "react";
import { Switch, Redirect } from "react-router-dom";

import { PrivateRoute } from "../../common/components/routes/PrivateRoute";
import { PublicRoute } from "../../common/components/routes/PublicRoute";

import Home from "../../pages/home/home";
import Products from "../../pages/products/products";

const Routes = (props) => {
	return (
		<Switch>
			<PublicRoute path="/home" component={Home} redirectPath="/home" />
			<PublicRoute
				path="/products"
				component={Products}
				redirectPath="/products"
			/>
			<Redirect to="/home" />
		</Switch>
	);
};

export default Routes;
