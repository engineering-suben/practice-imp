import React, { useEffect } from "react";
import { connect } from "react-redux";

import { getLoading, doSomething } from "./ducks";

const aTag = <a href="https://localhost:3000/home">link</a>;

const Home = ({ loading, doSomething }) => {
	useEffect(() => {
		// fetch("https://ldev.limber.co.in:9000/api/user", {
		// 	method: "post",
		// 	// withCredentials: true,
		// 	credentials: "include",
		// 	data: {
		// 		asd: "asd",
		// 	},
		// 	headers: {
		// 		"Content-Type": "application/json",
		// 	},
		// })
		// 	.then((body) => {
		// 		console.log("body", body.headers.get("set-cookie"));
		// 		return body.json();
		// 	})
		// 	.then((res) => console.log("res", res));
		// fetch("https://ldev.limber.in:9001/api/user", {
		// 	credentials: "include",
		// })
		// 	.then((body) => {
		// 		console.log("body", body.headers.get("set-cookie"));
		// 		return body.json();
		// 	})
		// 	.then((res) => console.log("res", res));
		// setTimeout(() => {
		// 	doSomething();
		// }, 1000);
	}, []);

	const onClick = (event) => {
		console.log("onClick", event);
		event.preventDefault();

		// fetch("https://localhost:3000/api/users")
		// 	.then((body) => body.json())
		// 	.then((res) => console.log("res", res));

		fetch("https://ldev.limber.co.in:9000/api/user", {
			credentials: "include",
			mode: "cors",
		})
			.then((body) => {
				console.log("body", body.headers.get("set-cookie"));
				return body.json();
			})
			.then((res) => console.log("res", res));
	};

	console.log("****Home****");

	return (
		<div>
			<a href="https://localhost:9000/home" onClick={onClick}>
				link
			</a>
			<br />
			<a href="https://ldev.limber.in:9001/api/user">lnk</a>
			<br />
			<iframe
				src="https://ldev.limber.in:9001/products"
				referrerPolicy="strict-origin-when-cross-origin"
				sandbox="allow-scripts allow-modals"
			/>
			{/*<iframe src="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe" />*/}
		</div>
	);
};

const enhance = connect(
	(state) => ({
		loading: getLoading(state),
	}),
	{
		doSomething,
	}
);

export default enhance(Home);
