var fs = require("fs");
var path = require("path");

var webpack = require("webpack");

var MiniCssExtractPlugin = require("mini-css-extract-plugin");
var miniCssExtractPlugin = new MiniCssExtractPlugin({ filename: "index.css" });
var WebpackNotifierPlugin = require("webpack-notifier");
var webpackNotifierPlugin = new WebpackNotifierPlugin({
	title: "Title",
	alwaysNotify: true,
});
var LoadablePlugin = require("@loadable/webpack-plugin");
var loadablePlugin = new LoadablePlugin();

var browserConfig = {
	mode: process.env.NODE_ENV === "development" ? "development" : "production",
	devtool:
		process.env.NODE_ENV === "development" ? "inline-source-map" : "source-map",
	entry: __dirname + "/src/browser/index.js",
	output: {
		path: __dirname + "/public/assets",
		// path: path.join(__dirname, "./public/assets/"),
		publicPath: "assets/",
		filename: "index.js",
	},
	// devServer: {
	//   inline: true,
	//   contentBase: "./public",
	//   port: process.env.PORT,
	//   historyApiFallback: true,
	// },
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: "babel-loader",
			},
			{
				test: /\.json$/,
				exclude: /node_modules/,
				loader: "json-loader",
			},
			{
				test: /\.(sc|c)ss$/,
				exclude: /node_modules/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
						options: {
							hmr: process.env.NODE_ENV === "development",
							reloadAll: true,
						},
					},
					"css-loader",
					"postcss-loader",
					"sass-loader",
				],
			},
			{
				test: [/\.svg$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
				loader: "file-loader",
				options: {
					name: "public/media/[name].[ext]",
					publicPath: (url) => url.replace(/public/, ""),
				},
			},
		],
	},
	plugins: [miniCssExtractPlugin, webpackNotifierPlugin],
	resolve: {
		alias: {
			src: path.resolve(__dirname, "src/"),
		},
	},
};

var serverConfig = {
	mode: process.env.NODE_ENV === "development" ? "development" : "production",
	devtool:
		process.env.NODE_ENV === "development" ? "inline-source-map" : "source-map",
	entry: __dirname + "/src/server/index.js",
	target: "node",
	output: {
		// path: path.resolve(__dirname, "dist"),
		path: __dirname,
		// path: __dirname + "/public/assets",
		// publicPath: "/public/assets/",
		filename: "dist/server.js",
		libraryTarget: "commonjs2",
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: "babel-loader",
			},
			{
				test: /\.json$/,
				exclude: /node_modules/,
				loader: "json-loader",
			},
			{
				test: /\.(sc|c)ss$/,
				exclude: /node_modules/,
				use: [
					{
						loader: "css-loader/locals",
					},
					"sass-loader",
				],
			},
			{
				test: [/\.svg$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
				loader: "file-loader",
				options: {
					name: "public/media/[name].[ext]",
					publicPath: (url) => url.replace(/public/, ""),
					emit: false,
				},
			},
		],
	},
	plugins: [miniCssExtractPlugin, webpackNotifierPlugin, loadablePlugin],
	resolve: {
		alias: {
			src: path.resolve(__dirname, "src/"),
		},
	},
};

module.exports = [browserConfig, serverConfig];
